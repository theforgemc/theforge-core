package neusfear.theforgecore;

import org.spongepowered.api.Sponge;

import java.util.List;
import java.util.Optional;

public class Reference {
    public static String id = Sponge.getPluginManager().getPlugin("theforgecore").get().getId();
    public static String name = Sponge.getPluginManager().getPlugin("theforgecore").get().getName();
    public static String version = Sponge.getPluginManager().getPlugin("theforgecore").get().getVersion().toString().replace("Optional", "");
    public static List<String> authors = Sponge.getPluginManager().getPlugin("theforgecore").get().getAuthors();
    public static Optional<String> description = Sponge.getPluginManager().getPlugin("theforgecore").get().getDescription();
    public static Optional<String> url = Sponge.getPluginManager().getPlugin("theforgecore").get().getUrl();
}
