package neusfear.theforgecore.modules.referrals;

import neusfear.theforgecore.helpers.util.Time;

public class Referral {

    private String uuid;
    private String date;

    public Referral(String uuid) {
        this.uuid = uuid;
        this.date = Time.getNow().toString();
    }

    public String getUuid() {
        return uuid;
    }

    public String getDate() {
        return date;
    }
}
