package neusfear.theforgecore.modules.referrals.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.referrals.Referral;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class ReferralsCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command shows the balance of a player"))

                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player"))))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        String argPlayer = args.<String>getOne("player").orElse(src.getName());
                        if (PlayerDataHandler.hasUserJoined(argPlayer)) {
                            PlayerData argPlayerdata = PlayerDataHandler.getPlayerdataFromUsername(argPlayer);
                            if (argPlayerdata.getReferrals().size() < 1) {
                                src.sendMessage(MsgHelper.error("You have no referrals."));
                                return CommandResult.success();
                            }
                            String players = "";
                            for (Referral referral : argPlayerdata.getReferrals()) {
                                players = players + ", " + PlayerDataHandler.getPlayerdata(referral.getUuid()).getUsername();
                            }
                            src.sendMessage(MsgHelper.info("&7Referrals: (&f" + argPlayerdata.getReferrals().size() + "&7)\n" + players.substring(1)));
                        } else {
                            src.sendMessage(MsgHelper.error("That player has not joined."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "referrals", "refs");
    }
}
