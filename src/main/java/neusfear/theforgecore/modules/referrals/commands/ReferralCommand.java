package neusfear.theforgecore.modules.referrals.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.referrals.Referral;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class ReferralCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command shows the balance of a player"))

                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player"))))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        Player player = (Player) src;
                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        String referrer = args.<String>getOne("player").orElse("help");
                        if (referrer.equals("help")) {
                            src.sendMessage(MsgHelper.info("&f/referral <player> &7 allows you to claim a referral for a player up to 1 hour after joining."));
                            return CommandResult.success();
                        }

                        if (PlayerDataHandler.getPlayerdataFromUsername(referrer).getUuid().equals(playerData.getUuid())) {
                            src.sendMessage(MsgHelper.error("You cannot claim yourself as a referrer."));
                            return CommandResult.success();
                        }

                        if (playerData.getPermissionLevel() > 0) {
                            src.sendMessage(MsgHelper.error("You've already claimed a referral"));
                            return CommandResult.success();
                        }

                        String firstJoin = PlayerDataHandler.getPlayerdata(((Player) src).getUniqueId().toString()).getFirstJoin();
                        if (Time.getNow().getTime() - Time.getTimestampFrom(firstJoin).getTime() > 3600000) {
                            src.sendMessage(MsgHelper.error("You cannot redeem a referral 1 hour after joining"));
                            return CommandResult.success();
                        }

                        if (PlayerDataHandler.hasUserJoined(referrer)) {
                            PlayerData referrerData = PlayerDataHandler.getPlayerdataFromUsername(referrer);
                            referrerData.addReferral(new Referral(((Player) src).getUniqueId().toString()));
                            src.sendMessage(MsgHelper.info("Referral sucessful!"));
                            if (PlayerDataHandler.getUserFromName(referrer).isPresent()) {
                                if (PlayerDataHandler.getUserFromName(referrer).get().isOnline()) {
                                    PlayerDataHandler.getUserFromName(referrer).get().getPlayer().get()
                                            .sendMessage(MsgHelper.info(src.getName() + " has been added to your referrals. $100 added to both accounts along with 10XP for you!"));
                                    referrerData.addMoney(100.0);
                                    referrerData.addExperience(10);
                                    playerData.addMoney(100);
                                    src.sendMessage(MsgHelper.info("Since you were referred we're going to give you an extra &f$100!"));
                                    playerData.setPermissionLevel(1);
                                }
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before, how could they have referred you???"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "referral", "ref");
    }
}
