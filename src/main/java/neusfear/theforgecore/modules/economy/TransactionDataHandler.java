package neusfear.theforgecore.modules.economy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.helpers.ServerDataHelper;
import org.spongepowered.api.scheduler.Task;

import java.io.*;

public class TransactionDataHandler {

    private static TransactionDataHandler transactionDataHandler;
    private static TransactionData transactionData;

    public TransactionDataHandler() {
        transactionDataHandler = this;

        if (!loadTransactionData()) {
            createNewTransactionData(true);
        }
    }

    public static void addTransaction(String itemName, int quantity, boolean buy) {
        TransactionTracker tracker = transactionData.getTrackerWithName(itemName);
        if (buy) {
            tracker.addBought(quantity);
        } else {
            tracker.addSold(quantity);
        }
    }

    public static int getBought(String itemname) {
        return transactionData.getTrackerWithName(itemname).getBought();
    }

    public static int getSold(String itemname) {
        return transactionData.getTrackerWithName(itemname).getSold();
    }

    public static TransactionData getTransactionData() {
        return transactionData;
    }

    private TransactionData createNewTransactionData(boolean save) {

        TransactionData data = new TransactionData();

        for (ItemPrice item : PriceReference.getItems()) {
            data.addTransactionTracker(new TransactionTracker(item.getAlias(), 0, 0));
        }

        if (save) {
            storeTransactionData(data);
            saveTransactionData();
        }

        return data;
    }

    private static void save() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(transactionData);
        try (FileWriter fileWriter = new FileWriter(ServerDataHelper.getTransactionDataFile())) {
            fileWriter.write(jsonString);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveTransactionData() {
        saveTransactionData(false);
    }

    public static void saveTransactionData(boolean useMainThread) {
        if (useMainThread) {
            save();
        } else {
            Task.builder().execute(TransactionDataHandler::save).async().submit(TheForgeCore.getInstance());
        }
    }

    private boolean loadTransactionData() {
        File file = ServerDataHelper.getTransactionDataFile();
        try {
            if (file.exists()) {
                Gson gson = new Gson();
                BufferedReader br = new BufferedReader(new FileReader(file));

                TransactionData data = gson.fromJson(br, TransactionData.class);

                LoggerHelper.getLogger().info("Checking out Transaction data for congruency with PriceReference");
                TransactionData newData = createNewTransactionData(false);
                TransactionTracker oldTracker;
                for (TransactionTracker tracker : newData.getTransactionTrackers()) {
                    oldTracker = data.getTrackerWithName(tracker.getItemType());
                    if (oldTracker.getBought() != -1) {
                        tracker.setBought(oldTracker.getBought());
                        tracker.setSold(oldTracker.getSold());
                    }
                }

                storeTransactionData(newData);
                saveTransactionData();

                return true;
            } else {
                return false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void storeTransactionData(TransactionData data) {
        transactionData = data;
    }
}
