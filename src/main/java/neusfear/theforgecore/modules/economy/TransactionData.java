package neusfear.theforgecore.modules.economy;

import java.util.ArrayList;

public class TransactionData {

    private ArrayList<TransactionTracker> transactionTrackers = new ArrayList<>();

    public ArrayList<TransactionTracker> getTransactionTrackers() {
        return transactionTrackers;
    }

    public void setTransactionTrackers(ArrayList<TransactionTracker> tr) {
        transactionTrackers = tr;
    }

    public void addTransactionTracker(TransactionTracker transactionTracker) {
        transactionTrackers.add(transactionTracker);
    }

    public TransactionTracker getTrackerWithName(String name) {
        for (TransactionTracker tracker : transactionTrackers) {
            if (tracker.getItemType().equalsIgnoreCase(name)) {
                return tracker;
            }
        }
        return new TransactionTracker(name, -1, -1);
    }
}
