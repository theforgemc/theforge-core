package neusfear.theforgecore.modules.economy.helpers;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;

import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

public class BalanceHelper {

    public static boolean hasSpaceFor(Player player, int ammount) {
        int[] gold = BalanceHelper.translateCurrencyToPhysical(ammount);
        int empty = 0;
        int sn = 0;
        for (Inventory inventory : player.getInventory().slots()) {
            //If the slot is empty
            if (inventory.size() == 0 && sn <= 35) {
                empty++;
            }
            sn++;
        }
        int needed = 0;
        if (gold[0] > 64) needed += Math.floorDiv(gold[0], 64);
        if (gold[0] % 64 > 0) needed++;
        if (gold[1] > 0) needed++;
        if (gold[2] > 0) needed++;
        return empty >= needed;
    }

    public static void givePhysicalCurrency(Player player, int ammount) {
        int[] gold = BalanceHelper.translateCurrencyToPhysical(ammount);
        int sn = 0;
        for (Inventory inventory : player.getInventory().slots()) {
            //If the slot is empty
            if (inventory.size() == 0 && sn <= 35) {
                if (gold[0] > 64) {
                    inventory.offer(ItemStack.of(ItemTypes.GOLD_BLOCK, 64));
                    gold[0] -= 64;
                    continue;
                }
                if (gold[0] > 0) {
                    inventory.offer(ItemStack.of(ItemTypes.GOLD_BLOCK, gold[0]));
                    gold[0] = 0;
                    continue;
                }
                if (gold[1] > 0) {
                    inventory.offer(ItemStack.of(ItemTypes.GOLD_INGOT, gold[1]));
                    gold[1] = 0;
                    continue;
                }
                if (gold[2] > 0) {
                    inventory.offer(ItemStack.of(ItemTypes.GOLD_NUGGET, gold[2]));
                    gold[2] = 0;
                    break;
                }
            }
            sn++;
        }
    }

    public static int getPhysicalCurrency(String uuid) {
        return getPhysicalCurrency(uuid, false);
    }

    public static int getPhysicalCurrency(String uuid, boolean takeFromInventory) {
        Optional<Player> userPlayer = Sponge.getServer().getPlayer(UUID.fromString(uuid));
        int quantity = 0;
        if (userPlayer.isPresent()) {
            Inventory nugQuery = userPlayer.get().getInventory().query(QueryOperationTypes.ITEM_TYPE.of(ItemTypes.GOLD_NUGGET));
            for (Iterator<Inventory> iterator = nugQuery.iterator(); iterator.hasNext(); ) {
                Inventory inventory = iterator.next();
                quantity += inventory.peek().get().getQuantity();
                if (takeFromInventory) inventory.clear();
            }

            Inventory ingotQuery = userPlayer.orElseGet(null).getInventory().query(QueryOperationTypes.ITEM_TYPE.of(ItemTypes.GOLD_INGOT));
            for (Iterator<Inventory> iterator = ingotQuery.iterator(); iterator.hasNext(); ) {
                Inventory inventory = iterator.next();
                quantity += inventory.peek().get().getQuantity()*9;
                if (takeFromInventory) inventory.clear();
            }

            Inventory blockQuery = userPlayer.orElseGet(null).getInventory().query(QueryOperationTypes.ITEM_TYPE.of(ItemTypes.GOLD_BLOCK));
            for (Iterator<Inventory> iterator = blockQuery.iterator(); iterator.hasNext(); ) {
                Inventory inventory = iterator.next();
                quantity += inventory.peek().get().getQuantity()*81;
                if (takeFromInventory) inventory.clear();
            }
        }
        return quantity;
    }

    public static int[] translateCurrencyToPhysical(int amount) {
        //gold[0] = blocks
        //gold[1] = ingots
        //gold[2] = nugs
        int[] gold = {0, 0, 0};

        /*
        for (int b = ammount; b > 0; b -= 81) {
            gold[0]++;
        }
        for (int i = (ammount-(gold[0]*81)); i > 0; i -= 81) {
            gold[1]++;
        }
        for (int n = (ammount-(gold[0]*81)-(gold[1]*9)); n > 0; n -= 81) {
            gold[2]++;
        }
         */

        //if it can't fit as a single stack of nugs
        if (amount > 64) {
            //if it cant fit as a 2 stacks of only nugs and ingots
            if (amount > 640 ) {
                gold[0] = Math.floorDiv(amount, 81);
                gold[1] = Math.floorDiv(amount - gold[0] * 81, 9);
                gold[2] = Math.floorMod(amount, 9);
                return gold;
            }
            gold[1] = Math.floorDiv(amount, 9);
            gold[2] = Math.floorMod(amount, 9);
            return gold;
        }
        gold[2] = amount;
        return gold;
    }
}
