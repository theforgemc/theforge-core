package neusfear.theforgecore.modules.economy.helpers;

import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.MathHelper;
import neusfear.theforgecore.modules.economy.PriceReference;
import neusfear.theforgecore.modules.economy.TransactionDataHandler;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.apache.commons.lang3.text.WordUtils;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;

public class SignHelper {

    public static ArrayList<Location<World>> signLocs = new ArrayList<>();
    public static final double SELLCONVERSION = 0.6;

    public static void createSign(Location<World> location, Player player, boolean buy, boolean sell) {
        if (location.getTileEntity().isPresent()) {
            if (player.getItemInHand(HandTypes.MAIN_HAND).isPresent()) {
                if (!PriceReference.getPrettyNameOf(player.getItemInHand(HandTypes.MAIN_HAND).get()).equals("undefined")) {
                    ItemStack items = player.getItemInHand(HandTypes.MAIN_HAND).get();
                    setLine(1, location.getTileEntity().get(), MsgHelper.formatted("&6[Shop&6]"));
                    setLine(2, location.getTileEntity().get(), MsgHelper.formatted(WordUtils.capitalizeFully(PriceReference.getPrettyNameOf(items))));
                    if (buy) {
                        setLine(3, location.getTileEntity().get(), MsgHelper.formatted("Buy " + items.getQuantity() + " : $" + MathHelper.roundAvoid(PriceReference.getFor(PriceReference.getPrettyNameOf(items))*items.getQuantity(), 2)));
                    }
                    if (sell) {
                        setLine(4, location.getTileEntity().get(), MsgHelper.formatted("Sell " + items.getQuantity() + " : $" + MathHelper.roundAvoid(PriceReference.getFor(PriceReference.getPrettyNameOf(items)) * items.getQuantity() * SELLCONVERSION , 2)));
                    }
                    updateSign((Sign) location.getTileEntity().get());
                } else {
                    player.sendMessage(MsgHelper.error("that item is not supported in the shop."));
                }
            }
        }
        player.sendMessage(MsgHelper.info("Sign created."));
    }

    public static boolean setLine(int line, TileEntity entity, Text text) {
        if (entity.supports(SignData.class)) {
            SignData sign = entity.getOrCreate(SignData.class).get();
            sign.set(sign.lines().set(line - 1, text));
            entity.offer(sign);
            return true;
        }
        return false;
    }

    private static void updateSign(Sign sign) {
        String itemname = sign.getSignData().lines().get(1).toPlain();
        double price = MathHelper.roundAvoid(PriceReference.getFor(itemname), 2);
        int quantity = Integer.valueOf(sign.getSignData().lines().get(2).toPlain().split(" ")[1]);
        if (!sign.getSignData().lines().get(2).toPlain().split("\\$")[0].equalsIgnoreCase("")) {
            setLine(3, sign, MsgHelper.formatted(sign.getSignData().lines().get(2).toPlain().split("\\$")[0] + "$" + MathHelper.roundAvoid(price * (double) quantity, 2)));
        }
        if (!sign.getSignData().lines().get(3).toPlain().split("\\$")[0].equalsIgnoreCase("")) {
            setLine(4, sign, MsgHelper.formatted(sign.getSignData().lines().get(3).toPlain().split("\\$")[0] + "$" + MathHelper.roundAvoid(price * SELLCONVERSION * (double) quantity, 2)));
        }
    }

    public static void process(Player player, Sign sign, boolean buy) {
        String itemname = sign.getSignData().lines().get(1).toPlain();
        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

        if (buy) {
            if (sign.getSignData().lines().get(2).isEmpty()) {
                player.sendMessage(MsgHelper.error("You can't buy at this shop"));
                return;
            }
        } else {
            if (sign.getSignData().lines().get(3).isEmpty()) {
                player.sendMessage(MsgHelper.error("You can't sell at this shop"));
                return;
            }
        }
        int quantity = Integer.valueOf(sign.getSignData().lines().get(2).toPlain().split(" ")[1]);
        double price;
        if (player.get(Keys.IS_SNEAKING).orElse(false)) {
            price = MathHelper.roundAvoid(PriceReference.getFor(itemname) * 64, 2);
        } else {
            price = MathHelper.roundAvoid(PriceReference.getFor(itemname) * quantity, 2);
        }
        if (buy) {
            ItemStack stack = PriceReference.createStackFrom(itemname, quantity);
            if (!stack.isEmpty() && player.getInventory().canFit(stack)) {
                if (playerData.getMoney() >= price) {
                    playerData.removeMoney(price);
                    player.getInventory().offer(stack);
                    player.sendMessage(MsgHelper.info("You bought " + quantity + " " + itemname + " for: $" + price + " your remaining balance is " + playerData.getMoney()));
                    TransactionDataHandler.addTransaction(itemname, quantity, true);
                    updateSign(sign);
                } else {
                    player.sendMessage(MsgHelper.error("You don't have the required funds to buy " + quantity + " " + itemname));
                }
                return;
            }
            if (!player.getInventory().contains(stack)) {
                player.sendMessage(MsgHelper.error("You don't have enough " + itemname + " to be sold."));
                return;
            }
            player.sendMessage(MsgHelper.error("You don'd have enough space to hold that."));
        } else {
            ItemStack stack = PriceReference.createStackFrom(itemname, quantity);
            if (!stack.isEmpty() && player.getInventory().contains(stack)) {
                player.getInventory().query(QueryOperationTypes.ITEM_STACK_IGNORE_QUANTITY.of(stack)).poll(stack.getQuantity());
                playerData.addMoney(price);
                player.sendMessage(MsgHelper.info("You sold " + quantity + " " + itemname + " for: $" + price + " your balance is now: $" + playerData.getMoney()));
                TransactionDataHandler.addTransaction(itemname, quantity, false);
                updateSign(sign);
                return;
            }
            if (!player.getInventory().contains(stack)) {
                player.sendMessage(MsgHelper.error("You don't have enough " + itemname + " to be sold."));
                return;
            }
            player.sendMessage(MsgHelper.error("There was an error processing your request, please report this to a staff member"));
        }
    }

    /*
    public static void changeSigns(ChangeDataHolderEvent event) {
        Player player = (Player) event.getTargetHolder();
        if (!event.getTargetHolder().get(Keys.IS_SNEAKING).orElse(false)) {
            player.sendMessage(MsgHelper.info("Start Sneaking"));
            BlockRay<World> blockRay = BlockRay.from(player)
                    .skipFilter(BlockRay.onlyAirFilter()).distanceLimit(10).build();
            Optional<TileEntity> tileEntity = blockRay.end().map(BlockRayHit::getLocation).flatMap(Location::getTileEntity);
                if (tileEntity.isPresent() && tileEntity.get().getType().equals(TileEntityTypes.SIGN)) {
                    LoggerHelper.getLogger().info("Sign");
                    Sign sign = (Sign) tileEntity.get();
                    player.sendMessage(MsgHelper.formatted(sign.getSignData().lines().get(0).toPlain()));
                    player.sendMessage(MsgHelper.formatted(sign.getSignData().lines().get(1).toPlain()));
                    player.sendMessage(MsgHelper.formatted(sign.getSignData().lines().get(2).toPlain()));
                    player.sendMessage(MsgHelper.formatted(sign.getSignData().lines().get(3).toPlain()));
                }
        } else {
            player.sendMessage(MsgHelper.info("Stop Sneaking"));
        }
    }
     */
}
