package neusfear.theforgecore.modules.economy;

import org.spongepowered.api.item.inventory.ItemStack;

public class ItemPrice {

    private String alias;
    private ItemStack item;
    private double price;
    private int stock;

    public ItemPrice(String alias, ItemStack itemStack, double price, int startStock) {
        setAlias(alias);
        setItem(itemStack);
        setPrice(price);
        setStock(startStock);
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
