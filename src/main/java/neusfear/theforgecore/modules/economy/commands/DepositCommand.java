package neusfear.theforgecore.modules.economy.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.economy.helpers.BalanceHelper;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class DepositCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command deposits the physical currency of the player into their bank."))
                .arguments(GenericArguments.optionalWeak(GenericArguments.integer(Text.of("amount"))))
                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        Player player = (Player) src;
                        String uuid = player.getUniqueId().toString();
                        int physMoney = BalanceHelper.getPhysicalCurrency(uuid);


                        if (args.getOne("amount").isPresent()) {

                            int depoAmount = args.<Integer>getOne("amount").get();

                            if (physMoney >= depoAmount) {

                                if (BalanceHelper.hasSpaceFor(player, physMoney - depoAmount)) {
                                    //Deposit ALL they have
                                    PlayerDataHandler.getPlayerdata(uuid).addMoney(BalanceHelper.getPhysicalCurrency(uuid, true));
                                    //Give back what they wanted to keep in an optimized storage space.
                                    BalanceHelper.givePhysicalCurrency(player, physMoney - depoAmount);
                                    src.sendMessage(MsgHelper.info("$" + depoAmount + " deposited."));
                                } else {
                                    src.sendMessage(MsgHelper.error("If you were to deposit that amount your inventory would not have enough space to hold the split stacks' configuration."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("You have $" + physMoney + " in your inventory. Which is not enough to deposit $" + depoAmount));
                            }
                        } else {
                            //Deposit it ALL
                            PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).addMoney(BalanceHelper.getPhysicalCurrency(uuid, true));
                            src.sendMessage(MsgHelper.info("$" + physMoney + " deposited."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "deposit", "depo", "bank");
    }
}
