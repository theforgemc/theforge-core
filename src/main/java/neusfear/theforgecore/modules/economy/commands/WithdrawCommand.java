package neusfear.theforgecore.modules.economy.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.economy.helpers.BalanceHelper;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class WithdrawCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command deposits the physical currency of the player into their bank."))

                .arguments(
                        GenericArguments.onlyOne(GenericArguments.integer(Text.of("amount")))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        Player player = (Player) src;
                        String uuid = player.getUniqueId().toString();
                        int ammount = args.<Integer>getOne("amount").get();

                        if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getMoney() >= ammount) {

                            //Deposit what they have to make withdraw more space efficient and save it to add to their desired withdraw
                            int invMoney = BalanceHelper.getPhysicalCurrency(uuid);
                            PlayerDataHandler.getPlayerdata(uuid).addMoney(BalanceHelper.getPhysicalCurrency(uuid, true));

                            if (BalanceHelper.hasSpaceFor(player, ammount)) {
                                BalanceHelper.givePhysicalCurrency(player, ammount + invMoney);
                                src.sendMessage(MsgHelper.info("$" + ammount + " withdrawn from your account."));
                            } else {
                                //Give them back the amount of money they had.
                                BalanceHelper.givePhysicalCurrency(player, invMoney);
                                src.sendMessage(MsgHelper.error("You don't have enough space to hold that much money. We sorted your current cash for you though."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("You don't have enough money in your bank to withdraw that much."));
                        }

                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "withdraw", "wd");
    }
}
