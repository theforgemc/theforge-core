package neusfear.theforgecore.modules.economy.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.economy.helpers.BalanceHelper;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class BalanceCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command shows the balance of a player"))

                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.player(Text.of("player"))))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        Player srcPlayer = (Player) src;
                        Player player = args.<Player>getOne("player").orElse(srcPlayer);

                        String uuid = player.getUniqueId().toString();
                        if (PlayerDataHandler.hasJoined(player.getUniqueId().toString())) {
                            sendBalanceMessage(src, uuid);
                        } else {
                            src.sendMessage(MsgHelper.error("That player doesn't exist"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "balance", "bal");
    }

    public static void sendBalanceMessage(CommandSource src, String uuid) {
        src.sendMessage(MsgHelper.info("Balance of <e>" + PlayerDataHandler.getPlayerdata(uuid).getUsername() + "'s<e> Bank: <e>$" + PlayerDataHandler.getPlayerdata(uuid).getMoney() + "<e> Inventory: <e>$" + BalanceHelper.getPhysicalCurrency(uuid) + ".00"));
    }
}
