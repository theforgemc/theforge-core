package neusfear.theforgecore.modules.economy.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.MathHelper;
import neusfear.theforgecore.modules.economy.helpers.BalanceHelper;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class PayCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command transfers money from the sender to the specified player"))

                .arguments(
                        GenericArguments.onlyOne(GenericArguments.player(Text.of("player"))),
                        GenericArguments.onlyOne(GenericArguments.doubleNum(Text.of("amount")))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if (src instanceof Player) {

                        Player player = (Player) src;
                        Player playerArg = args.<Player>getOne("player").get();

                        if (PlayerDataHandler.hasJoined(playerArg.getUniqueId().toString())) {

                            if (MathHelper.countDecimalDigits(args.<Double>getOne("amount").get()) < 3) {

                                String uuid = player.getUniqueId().toString();
                                double amount = args.<Double>getOne("amount").get();
                                if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getMoney() >= amount) {
                                    PlayerDataHandler.getPlayerdata(uuid).removeMoney(amount);
                                    PlayerDataHandler.getPlayerdata(playerArg.getUniqueId().toString()).addMoney(amount);
                                    src.sendMessage(MsgHelper.info("You transfered $" + amount + " to " + playerArg.getName()));
                                    playerArg.sendMessage(MsgHelper.info("$" + amount + " was transfered to you by " + src.getName()));
                                } else {
                                    if ((PlayerDataHandler.getPlayerdata(uuid).getMoney() + BalanceHelper.getPhysicalCurrency(uuid)) >= amount) {
                                        src.sendMessage(MsgHelper.error("You don't have enough money in your bank, but if you deposit your inventory with <e>/deposit<e> you will."));
                                    } else {
                                        src.sendMessage(MsgHelper.error("You don't have enough money to pay that amount."));
                                    }
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("That is not a valid dollar amount. Make sure there are 2 or less decimal places."));
                            }

                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "pay");
    }
}
