package neusfear.theforgecore.modules.economy;

public class TransactionTracker {

    private String itemType;
    private int bought;
    private int sold;

    public TransactionTracker(String type, int b, int s) {
        setItemType(type);
        setBought(b);
        setSold(s);
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public void addBought(int n) {
        bought += n;
    }

    public int getBought() {
        return bought;
    }

    public void setBought(int bought) {
        this.bought = bought;
    }

    public void addSold(int n) {
        sold += n;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }
}
