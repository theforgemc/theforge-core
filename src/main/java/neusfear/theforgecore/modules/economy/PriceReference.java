package neusfear.theforgecore.modules.economy;

import neusfear.theforgecore.helpers.LoggerHelper;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.*;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.potion.PotionTypes;

import java.util.ArrayList;

public class PriceReference {

    private static PriceReference priceReference;
    private static ArrayList<ItemPrice> items = new ArrayList<>();

    public PriceReference() {
        priceReference = this;

        /*                   */
        /* UNCRAFTABLE ITEMS */
        /*                   */

        //Stone blocks
        items.add(new ItemPrice("stone", ItemTypes.STONE.getTemplate().createStack(), 0.1, 1000000));
        items.add(new ItemPrice("granite", ItemTypes.STONE.getTemplate().with(Keys.STONE_TYPE, StoneTypes.GRANITE).get().createStack(), 0.1, 1000000));
        items.add(new ItemPrice("polished granite", ItemTypes.STONE.getTemplate().with(Keys.STONE_TYPE, StoneTypes.SMOOTH_GRANITE).get().createStack(), 0.1, 1000000));
        items.add(new ItemPrice("diorite", ItemTypes.STONE.getTemplate().with(Keys.STONE_TYPE, StoneTypes.DIORITE).get().createStack(), 0.1, 1000000));
        items.add(new ItemPrice("polished diorite", ItemTypes.STONE.getTemplate().with(Keys.STONE_TYPE, StoneTypes.SMOOTH_DIORITE).get().createStack(), 0.1, 1000000));
        items.add(new ItemPrice("andesite", ItemTypes.STONE.getTemplate().with(Keys.STONE_TYPE, StoneTypes.ANDESITE).get().createStack(), 0.1, 1000000));
        items.add(new ItemPrice("polished andesite", ItemTypes.STONE.getTemplate().with(Keys.STONE_TYPE, StoneTypes.SMOOTH_ANDESITE).get().createStack(), 0.1, 1000000));
        items.add(new ItemPrice("cobblestone", ItemTypes.COBBLESTONE.getTemplate().createStack(), 0.1, 10000));
        items.add(new ItemPrice("bedrock", ItemTypes.BEDROCK.getTemplate().createStack(), 1000000000.0, 0));
        items.add(new ItemPrice("obsidian", ItemTypes.OBSIDIAN.getTemplate().createStack(), 40.0, 1000));

        //Dirt blocks
        items.add(new ItemPrice("grass block", ItemTypes.GRASS.getTemplate().createStack(), 0.2, 100000));
        items.add(new ItemPrice("dirt", ItemTypes.DIRT.getTemplate().createStack(), 0.1, 100000));
        items.add(new ItemPrice("coarse dirt", ItemTypes.DIRT.getTemplate().with(Keys.DIRT_TYPE, DirtTypes.COARSE_DIRT).get().createStack(), 0.1, 100000));
        items.add(new ItemPrice("podzol", ItemTypes.DIRT.getTemplate().with(Keys.DIRT_TYPE, DirtTypes.PODZOL).get().createStack(), 0.1, 100000));
        items.add(new ItemPrice("mycelium", ItemTypes.MYCELIUM.getTemplate().createStack(), 2.0, 100000));

        //Sand and Gravel
        items.add(new ItemPrice("sand", ItemTypes.SAND.getTemplate().createStack(), 0.1, 100000));
        items.add(new ItemPrice("red sand", ItemTypes.SAND.getTemplate().with(Keys.SAND_TYPE, SandTypes.RED).get().createStack(), 0.2, 100000));
        items.add(new ItemPrice("gravel", ItemTypes.GRAVEL.getTemplate().createStack(), 8.0, 10000));

        //Farming
        items.add(new ItemPrice("melon", ItemTypes.MELON.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("pumpkin", ItemTypes.PUMPKIN.getTemplate().createStack(), 3.0, 10000));

        //Nether / End
        items.add(new ItemPrice("netherrack", ItemTypes.NETHERRACK.getTemplate().createStack(), 0.1, 100000));
        items.add(new ItemPrice("soul sand", ItemTypes.SOUL_SAND.getTemplate().createStack(), 2.0, 1000));
        items.add(new ItemPrice("magma block", ItemTypes.MAGMA.getTemplate().createStack(), 24.0, 100000));
        items.add(new ItemPrice("end stone", ItemTypes.END_STONE.getTemplate().createStack(), 1.0, 10000));


        //Misc
        items.add(new ItemPrice("sponge", ItemTypes.SPONGE.getTemplate().createStack(), 400.0, 100));
        items.add(new ItemPrice("wet sponge", ItemTypes.SPONGE.getTemplate().with(Keys.IS_WET, true).get().createStack(), 400.0, 100));
        items.add(new ItemPrice("ice", ItemTypes.ICE.getTemplate().createStack(), 8.0, 10000));

        //Wood
        items.add(new ItemPrice("oak log", ItemTypes.LOG.getTemplate().with(Keys.TREE_TYPE, TreeTypes.OAK).get().createStack(), 2.0, 10000));
        items.add(new ItemPrice("spruce log", ItemTypes.LOG.getTemplate().with(Keys.TREE_TYPE, TreeTypes.SPRUCE).get().createStack(), 2.0, 10000));
        items.add(new ItemPrice("birch log", ItemTypes.LOG.getTemplate().with(Keys.TREE_TYPE, TreeTypes.BIRCH).get().createStack(), 2.0, 10000));
        items.add(new ItemPrice("jungle log", ItemTypes.LOG.getTemplate().with(Keys.TREE_TYPE, TreeTypes.JUNGLE).get().createStack(), 2.0, 10000));
        items.add(new ItemPrice("acacia log", ItemTypes.LOG2.getTemplate().with(Keys.TREE_TYPE, TreeTypes.ACACIA).get().createStack(), 2.0, 10000));
        items.add(new ItemPrice("dark oak log", ItemTypes.LOG2.getTemplate().with(Keys.TREE_TYPE, TreeTypes.DARK_OAK).get().createStack(), 2.0, 10000));

        //Wool
        items.add(new ItemPrice("white wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("orange wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("magenta wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightBlue wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("yellow wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lime wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("pink wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("gray wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightGray wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("cyan wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("purple wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("blue wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("brown wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("green wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("red wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("black wool", ItemTypes.WOOL.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), 8.0, 100000));

        //Terracotta
        items.add(new ItemPrice("white clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("orange clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("magenta clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightBlue clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("yellow clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lime clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("pink clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("gray clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightGray clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("cyan clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("purple clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("blue clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("brown clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("green clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("red clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("black clay", ItemTypes.STAINED_HARDENED_CLAY.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("terracotta", ItemTypes.HARDENED_CLAY.getTemplate().createStack(), 8.0, 100000));

        //GLAZED TERRACOTTA HERE
        items.add(new ItemPrice("white glazed", ItemTypes.WHITE_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("orange glazed", ItemTypes.ORANGE_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("magenta glazed", ItemTypes.MAGENTA_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightBlue glazed", ItemTypes.LIGHT_BLUE_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("yellow glazed", ItemTypes.YELLOW_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lime glazed", ItemTypes.LIME_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("pink glazed", ItemTypes.PINK_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("gray glazed", ItemTypes.GRAY_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightGray glazed", ItemTypes.SILVER_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("cyan glazed", ItemTypes.CYAN_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("purple glazed", ItemTypes.PURPLE_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("blue glazed", ItemTypes.BLUE_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("brown glazed", ItemTypes.BROWN_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("green glazed", ItemTypes.GREEN_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("red glazed", ItemTypes.RED_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));
        items.add(new ItemPrice("black glazed", ItemTypes.BLACK_GLAZED_TERRACOTTA.getTemplate().createStack(), 8.0, 100000));

        //Concrete
        items.add(new ItemPrice("white concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("orange concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("magenta concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("L Blue concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("yellow concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lime concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("pink concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("gray concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("L Gray concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("cyan concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("purple concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("blue concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("brown concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("green concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("red concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("black concrete", ItemTypes.CONCRETE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), 8.0, 100000));

       //Concrete Powder
        items.add(new ItemPrice("white powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("orange powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("magenta powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightBlue powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("yellow powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lime powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("pink powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("gray powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("lightGray powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("cyan powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("purple powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("blue powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("brown powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("green powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("red powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), 8.0, 100000));
        items.add(new ItemPrice("black powder", ItemTypes.CONCRETE_POWDER.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), 8.0, 100000));

        //Raw materials
        items.add(new ItemPrice("coal", ItemTypes.COAL.getTemplate().with(Keys.COAL_TYPE, CoalTypes.COAL).get().createStack(), 5.0, 1000));
        items.add(new ItemPrice("diamond", ItemTypes.DIAMOND.getTemplate().createStack(), 200.0, 300));
        items.add(new ItemPrice("emerald", ItemTypes.EMERALD.getTemplate().createStack(), 100.0, 1000));
        items.add(new ItemPrice("flint", ItemTypes.FLINT.getTemplate().createStack(), 1.0, 1000));
        items.add(new ItemPrice("gold ingot", ItemTypes.GOLD_INGOT.getTemplate().createStack(), 9.0, 1000000));
        items.add(new ItemPrice("iron ingot", ItemTypes.IRON_INGOT.getTemplate().createStack(), 25.0, 1000000));
        items.add(new ItemPrice("lapis", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), 15.00, 1500));
        items.add(new ItemPrice("quartz", ItemTypes.QUARTZ.getTemplate().createStack(), 8.0, 1000));
        items.add(new ItemPrice("redstone", ItemTypes.REDSTONE.getTemplate().createStack(), 8.0, 1500));

        //Music Discs
        items.add(new ItemPrice("record 11", ItemTypes.RECORD_11.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record 13", ItemTypes.RECORD_13.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record blocks", ItemTypes.RECORD_BLOCKS.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record cat", ItemTypes.RECORD_CAT.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record chirp", ItemTypes.RECORD_CHIRP.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record far", ItemTypes.RECORD_FAR.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record mall", ItemTypes.RECORD_MALL.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record mellohi", ItemTypes.RECORD_MELLOHI.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record stall", ItemTypes.RECORD_STAL.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record strad", ItemTypes.RECORD_STRAD.getTemplate().createStack(), 100.0,250));
        items.add(new ItemPrice("record wait", ItemTypes.RECORD_WAIT.getTemplate().createStack(), 100.0, 250));
        items.add(new ItemPrice("record ward", ItemTypes.RECORD_WARD.getTemplate().createStack(), 100.0, 250));

        //Mob Drops
        items.add(new ItemPrice("blaze rod", ItemTypes.BLAZE_ROD.getTemplate().createStack(), 15.0, 500));
        items.add(new ItemPrice("bone", ItemTypes.BONE.getTemplate().createStack(), 0.25, 10000));
        items.add(new ItemPrice("creeper skull", ItemTypes.SKULL.getTemplate().with(Keys.SKULL_TYPE, SkullTypes.CREEPER).get().createStack(), 1000.0, 100));
        items.add(new ItemPrice("player skull", ItemTypes.SKULL.getTemplate().with(Keys.SKULL_TYPE, SkullTypes.PLAYER).get().createStack(), 1000.0, 100));
        items.add(new ItemPrice("skeleton skull", ItemTypes.SKULL.getTemplate().with(Keys.SKULL_TYPE, SkullTypes.SKELETON).get().createStack(), 1000.0, 100));
        items.add(new ItemPrice("wither skull", ItemTypes.SKULL.getTemplate().with(Keys.SKULL_TYPE, SkullTypes.WITHER_SKELETON).get().createStack(), 10000.0, 100));
        items.add(new ItemPrice("zombie skull", ItemTypes.SKULL.getTemplate().with(Keys.SKULL_TYPE, SkullTypes.ZOMBIE).get().createStack(), 1000.0, 100));

        //Chain Armor
        items.add(new ItemPrice("chain helmet", ItemTypes.CHAINMAIL_HELMET.getTemplate().createStack(), 500.0, 100));
        items.add(new ItemPrice("chain chestplate", ItemTypes.CHAINMAIL_CHESTPLATE.getTemplate().createStack(), 800.0, 100));
        items.add(new ItemPrice("chain leggings", ItemTypes.CHAINMAIL_LEGGINGS.getTemplate().createStack(), 700.0, 100));
        items.add(new ItemPrice("chain boots", ItemTypes.CHAINMAIL_BOOTS.getTemplate().createStack(), 400.0, 100));

        //assorted stuff
        items.add(new ItemPrice("clay", ItemTypes.CLAY_BALL.getTemplate().createStack(), 0.12, 10000));
        items.add(new ItemPrice("diamond horse", ItemTypes.DIAMOND_HORSE_ARMOR.getTemplate().createStack(), 250.0, 100));
        items.add(new ItemPrice("dragon egg", ItemTypes.DRAGON_EGG.getTemplate().createStack(), 100000.0, 25));
        items.add(new ItemPrice("dragon head", ItemTypes.SKULL.getTemplate().with(Keys.SKULL_TYPE, SkullTypes.ENDER_DRAGON).get().createStack(), 100000.0, 50));
        items.add(new ItemPrice("egg", ItemTypes.EGG.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("elytra", ItemTypes.ELYTRA.getTemplate().createStack(), 1500.0, 50));
        items.add(new ItemPrice("ender pearl", ItemTypes.ENDER_PEARL.getTemplate().createStack(), 40.00, 1000));
        items.add(new ItemPrice("feather", ItemTypes.FEATHER.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("ghast tear", ItemTypes.GHAST_TEAR.getTemplate().createStack(), 40.0, 300));
        items.add(new ItemPrice("glowstone dust", ItemTypes.GLOWSTONE_DUST.getTemplate().createStack(), 3.0, 10000));
        items.add(new ItemPrice("golden horse", ItemTypes.GOLDEN_HORSE_ARMOR.getTemplate().createStack(), 200.0, 100));
        items.add(new ItemPrice("gunpowder", ItemTypes.GUNPOWDER.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("ink sac", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("iron horse", ItemTypes.IRON_HORSE_ARMOR.getTemplate().createStack(), 150.0, 100));
        items.add(new ItemPrice("name tag", ItemTypes.NAME_TAG.getTemplate().createStack(), 350.0, 100));
        items.add(new ItemPrice("nether star", ItemTypes.NETHER_STAR.getTemplate().createStack(), 30000.0, 30));
        items.add(new ItemPrice("prismarine crystals", ItemTypes.PRISMARINE_CRYSTALS.getTemplate().createStack(), 2.0, 10000));
        items.add(new ItemPrice("prismarine shard", ItemTypes.PRISMARINE_SHARD.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("rabbit hide", ItemTypes.RABBIT_HIDE.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("rabbit foot", ItemTypes.RABBIT_FOOT.getTemplate().createStack(), 4.0, 1000));
        items.add(new ItemPrice("raw beef", ItemTypes.BEEF.getTemplate().createStack(), 4.0, 10000));
        items.add(new ItemPrice("raw chicken", ItemTypes.CHICKEN.getTemplate().createStack(), 3.0, 10000));
        items.add(new ItemPrice("raw fish", ItemTypes.FISH.getTemplate().createStack(), 2.0, 10000));
        items.add(new ItemPrice("raw mutton", ItemTypes.MUTTON.getTemplate().createStack(), 3.0, 10000));
        items.add(new ItemPrice("raw porkchop", ItemTypes.PORKCHOP.getTemplate().createStack(), 3.0, 10000));
        items.add(new ItemPrice("raw rabbit", ItemTypes.RABBIT.getTemplate().createStack(), 2.0, 10000));
        items.add(new ItemPrice("raw salmon", ItemTypes.FISH.getTemplate().with(Keys.FISH_TYPE, Fishes.SALMON).get().createStack(), 3.0, 10000));
        items.add(new ItemPrice("name", ItemTypes.FISH.getTemplate().with(Keys.FISH_TYPE, Fishes.CLOWNFISH).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("name", ItemTypes.FISH.getTemplate().with(Keys.FISH_TYPE, Fishes.PUFFERFISH).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("rotten flesh", ItemTypes.ROTTEN_FLESH.getTemplate().createStack(), 0.1, 10000));
        items.add(new ItemPrice("saddle", ItemTypes.SADDLE.getTemplate().createStack(), 350.0, 400));
        items.add(new ItemPrice("shulker shell", ItemTypes.SHULKER_SHELL.getTemplate().createStack(), 50.0, 100));
        items.add(new ItemPrice("slimeball", ItemTypes.SLIME_BALL.getTemplate().createStack(), 10.0, 1000));
        items.add(new ItemPrice("snowball", ItemTypes.SNOWBALL.getTemplate().createStack(), 0.1, 100000));
        items.add(new ItemPrice("spider eye", ItemTypes.SPIDER_EYE.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("string", ItemTypes.STRING.getTemplate().createStack(), 0.1, 10000));
        items.add(new ItemPrice("totem of undying", ItemTypes.TOTEM_OF_UNDYING.getTemplate().createStack(), 100000.0, 10));
        items.add(new ItemPrice("leather", ItemTypes.LEATHER.getTemplate().createStack(), 5.0, 100000));
        items.add(new ItemPrice("cobweb", ItemTypes.WEB.getTemplate().createStack(), 3.0, 1000));
        items.add(new ItemPrice("enchanting bottle", ItemTypes.EXPERIENCE_BOTTLE.getTemplate().createStack(), 10.0, 1000));

        //Leaves
        items.add(new ItemPrice("acacia leaves", ItemTypes.LEAVES2.getTemplate().with(Keys.TREE_TYPE, TreeTypes.ACACIA).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("birch leaves", ItemTypes.LEAVES.getTemplate().with(Keys.TREE_TYPE, TreeTypes.BIRCH).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("dark oak leaves", ItemTypes.LEAVES2.getTemplate().with(Keys.TREE_TYPE, TreeTypes.DARK_OAK).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("jungle leaves", ItemTypes.LEAVES.getTemplate().with(Keys.TREE_TYPE, TreeTypes.JUNGLE).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("oak leaves", ItemTypes.LEAVES.getTemplate().with(Keys.TREE_TYPE, TreeTypes.OAK).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("spruce leaves", ItemTypes.LEAVES.getTemplate().with(Keys.TREE_TYPE, TreeTypes.SPRUCE).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("acacia leaves", ItemTypes.LEAVES2.getTemplate().with(Keys.TREE_TYPE, TreeTypes.ACACIA).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("dark oak leaves", ItemTypes.LEAVES2.getTemplate().with(Keys.TREE_TYPE, TreeTypes.DARK_OAK).get().createStack(), 1.0, 10000));
        
        //Saplings
        items.add(new ItemPrice("acacia sapling", ItemTypes.SAPLING.getTemplate().with(Keys.TREE_TYPE, TreeTypes.ACACIA).get().createStack(), 8.0, 1000));
        items.add(new ItemPrice("birch sapling", ItemTypes.SAPLING.getTemplate().with(Keys.TREE_TYPE, TreeTypes.BIRCH).get().createStack(), 8.0, 1000));
        items.add(new ItemPrice("dark oak sapling", ItemTypes.SAPLING.getTemplate().with(Keys.TREE_TYPE, TreeTypes.DARK_OAK).get().createStack(), 8.0, 1000));
        items.add(new ItemPrice("jungle sapling", ItemTypes.SAPLING.getTemplate().with(Keys.TREE_TYPE, TreeTypes.JUNGLE).get().createStack(), 8.0, 1000));
        items.add(new ItemPrice("oak sapling", ItemTypes.SAPLING.getTemplate().with(Keys.TREE_TYPE, TreeTypes.OAK).get().createStack(), 8.0, 1000));
        items.add(new ItemPrice("spruce sapling", ItemTypes.SAPLING.getTemplate().with(Keys.TREE_TYPE, TreeTypes.SPRUCE).get().createStack(), 8.0, 1000));

        //Flowers
        items.add(new ItemPrice("allium", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.ALLIUM).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("azure bluet", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.HOUSTONIA).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("blue orchid", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.BLUE_ORCHID).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("brown mushroom", ItemTypes.BROWN_MUSHROOM.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("dandelion", ItemTypes.YELLOW_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.DANDELION).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("dead bush", ItemTypes.DEADBUSH.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("fern", ItemTypes.TALLGRASS.getTemplate().with(Keys.SHRUB_TYPE, ShrubTypes.FERN).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("lilac", ItemTypes.DOUBLE_PLANT.getTemplate().with(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.SYRINGA).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("lily pad", ItemTypes.WATERLILY.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("orange tulip", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.ORANGE_TULIP).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("oxeye daisy", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.OXEYE_DAISY).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("peony", ItemTypes.DOUBLE_PLANT.getTemplate().with(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.PAEONIA).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("large fern", ItemTypes.DOUBLE_PLANT.getTemplate().with(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.FERN).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("tall grass", ItemTypes.DOUBLE_PLANT.getTemplate().with(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.GRASS).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("pink tulip", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.PINK_TULIP).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("poppy", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.POPPY).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("red mushroom", ItemTypes.RED_MUSHROOM.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("red tulip", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.RED_TULIP).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("rose bush", ItemTypes.DOUBLE_PLANT.getTemplate().with(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.ROSE).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("sunflower", ItemTypes.DOUBLE_PLANT.getTemplate().with(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.SUNFLOWER).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("tall grass", ItemTypes.TALLGRASS.getTemplate().with(Keys.SHRUB_TYPE, ShrubTypes.TALL_GRASS).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("vines", ItemTypes.VINE.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("white tulip", ItemTypes.RED_FLOWER.getTemplate().with(Keys.PLANT_TYPE, PlantTypes.WHITE_TULIP).get().createStack(), 1.0, 10000));
        /*double tall grass and ferns are not included as unsure how to*/

        //Crops
        items.add(new ItemPrice("apple", ItemTypes.APPLE.getTemplate().createStack(), 5.0, 10000));
        items.add(new ItemPrice("beetroot", ItemTypes.BEETROOT.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("cactus", ItemTypes.CACTUS.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("carrot", ItemTypes.CARROT.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("chorus flower", ItemTypes.CHORUS_FLOWER.getTemplate().createStack(), 2.0, 10000));
        items.add(new ItemPrice("chorus fruit", ItemTypes.CHORUS_FRUIT.getTemplate().createStack(), 2.0, 10000));
        items.add(new ItemPrice("cocoa beans", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), 1.0, 10000));
        items.add(new ItemPrice("nether wart", ItemTypes.NETHER_WART.getTemplate().createStack(), 2.0, 10000));
        items.add(new ItemPrice("poisonous potato", ItemTypes.POISONOUS_POTATO.getTemplate().createStack(), 100.0, 10000));
        items.add(new ItemPrice("potato", ItemTypes.POTATO.getTemplate().createStack(), 0.5, 10000));
        items.add(new ItemPrice("sugar cane", ItemTypes.REEDS.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("wheat", ItemTypes.WHEAT.getTemplate().createStack(), 1.0, 10000));

        //Seeds
        items.add(new ItemPrice("beetroot seeds", ItemTypes.BEETROOT_SEEDS.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("seeds", ItemTypes.WHEAT_SEEDS.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("melon seeds", ItemTypes.MELON_SEEDS.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("pumpkin seeds", ItemTypes.PUMPKIN_SEEDS.getTemplate().createStack(), 1.0, 10000));

        //Mushroom Blocks
        items.add(new ItemPrice("brown mushroom block", ItemTypes.BROWN_MUSHROOM_BLOCK.getTemplate().createStack(), 1.0, 10000));
        items.add(new ItemPrice("red mushroom block", ItemTypes.RED_MUSHROOM_BLOCK.getTemplate().createStack(), 1.0,10000));

        //Raw ores
        items.add(new ItemPrice("emerald ore", ItemTypes.EMERALD_ORE.getTemplate().createStack(), 1.0,10000));
        items.add(new ItemPrice("quartz ore", ItemTypes.QUARTZ_ORE.getTemplate().createStack(), 1.0, 10000));



        /*                 */
        /* Craftable Items */
        /*                 */
        //Food & similar
        items.add(new ItemPrice("sugar", ItemTypes.SUGAR.getTemplate().createStack(), getFor("sugar cane"), 10000));
        items.add(new ItemPrice("bread", ItemTypes.BREAD.getTemplate().createStack(), getFor("wheat") * 3, 10000));
        items.add(new ItemPrice("melon block", ItemTypes.MELON_BLOCK.getTemplate().createStack(), getFor("melon") * 9, 10000));
        items.add(new ItemPrice("hay bale", ItemTypes.HAY_BLOCK.getTemplate().createStack(), getFor("wheat") * 9, 10000));
        items.add(new ItemPrice("cookie", ItemTypes.COOKIE.getTemplate().createStack(), ((getFor("wheat") * 2)+getFor("cocoa beans"))/8, 10000));
        items.add(new ItemPrice("baked potato", ItemTypes.BAKED_POTATO.getTemplate().createStack(), getFor("potato"), 10000));
        items.add(new ItemPrice("pumpkin pie", ItemTypes.PUMPKIN_PIE.getTemplate().createStack(), getFor("pumpkin")+getFor("egg")+getFor("sugar"), 10000));


            //Cooked Meat & Fish
        items.add(new ItemPrice("steak", ItemTypes.COOKED_BEEF.getTemplate().createStack(), getFor("raw beef"), 10000));
        items.add(new ItemPrice("cooked porkchop", ItemTypes.COOKED_PORKCHOP.getTemplate().createStack(), getFor("raw porkchop"), 10000));
        items.add(new ItemPrice("cooked chicken", ItemTypes.COOKED_CHICKEN.getTemplate().createStack(), getFor("raw chicken"), 10000));
        items.add(new ItemPrice("cooked mutton", ItemTypes.COOKED_MUTTON.getTemplate().createStack(), getFor("raw mutton"), 10000));
        items.add(new ItemPrice("cooked rabbit", ItemTypes.COOKED_RABBIT.getTemplate().createStack(), getFor("raw rabbit"), 10000));
        // Changed to cod in 1.13
        items.add(new ItemPrice("cooked fish", ItemTypes.COOKED_FISH.getTemplate().createStack(), getFor("raw fish"), 10000));
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^
        items.add(new ItemPrice("cooked salmon", ItemTypes.COOKED_FISH.getTemplate().with(Keys.COOKED_FISH, CookedFishes.SALMON).get().createStack(), getFor("raw salmon"), 10000));

        //Materials
        items.add(new ItemPrice("paper", ItemTypes.PAPER.getTemplate().createStack(), getFor("sugar cane"), 10000));
        items.add(new ItemPrice("bonemeal", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), getFor("bone") / 3, 10000));
        items.add(new ItemPrice("brick", ItemTypes.BRICK.getTemplate().createStack(), getFor("clay"), 10000));
        items.add(new ItemPrice("book", ItemTypes.BOOK.getTemplate().createStack(), (getFor("paper")*3)+getFor("leather"), 10000));
        items.add(new ItemPrice("clay block", ItemTypes.CLAY.getTemplate().createStack(), getFor("clay")*4, 10000));
        items.add(new ItemPrice("blaze powder", ItemTypes.BLAZE_POWDER.getTemplate().createStack(), getFor("blaze rod")/2,10000));
        items.add(new ItemPrice("eye of ender", ItemTypes.ENDER_EYE.getTemplate().createStack(), getFor("ender pearl")+getFor("blaze powder"),10000));

        //Crafted Dyes
        items.add(new ItemPrice("rose red", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), (getFor("red tulip")+(getFor("rose bush")/2)+getFor("poppy")+getFor("beetroot"))/4, 100000));
        items.add(new ItemPrice("cactus green", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), getFor("cactus"), 100000));
        items.add(new ItemPrice("purple dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), getFor("lapis")+getFor("rose red"), 100000));
        items.add(new ItemPrice("cyan dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), getFor("lapis")+getFor("cactus green"), 100000));
        items.add(new ItemPrice("lightGray dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), (getFor("white tulip")+getFor("oxeye daisy") + ((getFor("ink sac")+(getFor("bonemeal")*2))/3) + getFor("azure bluet"))/4, 100000));
        items.add(new ItemPrice("gray dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), (((getFor("ink sac")+ getFor("bonemeal"))/2)+((getFor("lightGray dye")+getFor("bonemeal"))/2))/2, 100000));
        items.add(new ItemPrice("pink dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), (((getFor("rose red")+getFor("bonemeal"))/2) + (getFor("peony")/2) + getFor("pink tulip")), 100000));
        //LIME DYE IS MADE WITH PICKLES IN 1.13
        items.add(new ItemPrice("lime dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), (((getFor("cactus green")+getFor("bonemeal"))/2) + 0)/1, 100000));
        items.add(new ItemPrice("dandelion yellow", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), ((getFor("sunflower")/2)+getFor("dandelion"))/2, 100000));
        items.add(new ItemPrice("lightBlue dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), (((getFor("lapis")+getFor("bonemeal"))/2)+getFor("blue orchid"))/2, 100000));
        items.add(new ItemPrice("magenta dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), (((getFor("purple dye")+getFor("pink dye"))/2)+(getFor("lilac")/2)+((getFor("lapis")+getFor("rose red")+getFor("pink dye"))/3)+((getFor("lapis")+getFor("bonemeal")+(getFor("rose red")*2))/4)+getFor("allium"))/5, 100000));
        items.add(new ItemPrice("orange dye", ItemTypes.DYE.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), (getFor("rose red")+getFor("dandelion yellow"))/2, 100000));

        //Buckets
        items.add(new ItemPrice("bucket", ItemTypes.BUCKET.getTemplate().createStack(), getFor("iron ingot") * 3, 500));
        items.add(new ItemPrice("water bucket", ItemTypes.WATER_BUCKET.getTemplate().createStack(), getFor("bucket"), 500));
        items.add(new ItemPrice("milk bucket", ItemTypes.MILK_BUCKET.getTemplate().createStack(), getFor("bucket"), 500));
        items.add(new ItemPrice("lava bucket", ItemTypes.LAVA_BUCKET.getTemplate().createStack(), getFor("bucket") * 1.2, 500));
        items.add(new ItemPrice("cake", ItemTypes.CAKE.getTemplate().createStack(), (getFor("milk bucket")*0.2*3)+getFor("egg")+(getFor("sugar")*2)+(getFor("wheat")*3), 10000));

        //Nuggets
        items.add(new ItemPrice("gold nugget", ItemTypes.GOLD_NUGGET.getTemplate().createStack(), getFor("gold ingot")/9, 500));
        items.add(new ItemPrice("iron nugget", ItemTypes.IRON_NUGGET.getTemplate().createStack(), getFor("iron ingot")/9, 500));

        //Mineral Blocks
        items.add(new ItemPrice("coal block", ItemTypes.COAL_BLOCK.getTemplate().createStack(), getFor("coal") * 9, 10000));
        items.add(new ItemPrice("diamond block", ItemTypes.DIAMOND_BLOCK.getTemplate().createStack(), getFor("diamond") * 9, 10000));
        items.add(new ItemPrice("emerald block", ItemTypes.EMERALD_BLOCK.getTemplate().createStack(), getFor("emerald") * 9, 10000));
        items.add(new ItemPrice("gold block", ItemTypes.GOLD_BLOCK.getTemplate().createStack(), getFor("gold ingot") * 9, 10000));
        items.add(new ItemPrice("iron block", ItemTypes.IRON_BLOCK.getTemplate().createStack(), getFor("iron ingot") * 9, 10000));
        items.add(new ItemPrice("lapis block", ItemTypes.LAPIS_BLOCK.getTemplate().createStack(), getFor("lapis") * 9, 10000));
        items.add(new ItemPrice("quartz block", ItemTypes.QUARTZ_BLOCK.getTemplate().with(Keys.QUARTZ_TYPE, QuartzTypes.DEFAULT).get().createStack(), getFor("quartz") * 9, 10000));
        items.add(new ItemPrice("redstone block", ItemTypes.REDSTONE_BLOCK.getTemplate().createStack(), getFor("redstone") * 9, 10000));

        //golden food
        items.add(new ItemPrice("golden carrot", ItemTypes.GOLDEN_CARROT.getTemplate().createStack(), getFor("carrot")+(getFor("gold nugget")*8), 10000));
        items.add(new ItemPrice("golden apple", ItemTypes.GOLDEN_APPLE.getTemplate().with(Keys.GOLDEN_APPLE_TYPE, GoldenApples.GOLDEN_APPLE).get().createStack(), getFor("apple")+(getFor("gold ingot")*8), 10000));
        items.add(new ItemPrice("god apple", ItemTypes.GOLDEN_APPLE.getTemplate().with(Keys.GOLDEN_APPLE_TYPE, GoldenApples.ENCHANTED_GOLDEN_APPLE).get().createStack(), getFor("apple")+(getFor("gold block")*8), 10000));

        //Quartz
        //items.add(new ItemPrice("quartz slab", ItemTypes.STONE_SLAB.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.QUARTZ).get().createStack(), getFor("quartz block") /2, 10000));
        items.add(new ItemPrice("chiseled quartz block", ItemTypes.QUARTZ_BLOCK.getTemplate().with(Keys.QUARTZ_TYPE, QuartzTypes.CHISELED).get().createStack(), getFor("quartz slab") * 2, 10000));
        items.add(new ItemPrice("quartz pillar", ItemTypes.QUARTZ_BLOCK.getTemplate().with(Keys.QUARTZ_TYPE, QuartzTypes.LINES_Y).get().createStack(), getFor("quartz block")*1, 10000));

        //Planks
        items.add(new ItemPrice("oak planks", ItemTypes.PLANKS.getTemplate().with(Keys.TREE_TYPE, TreeTypes.OAK).get().createStack(), getFor("oak log") / 4, 10000));
        items.add(new ItemPrice("spruce planks", ItemTypes.PLANKS.getTemplate().with(Keys.TREE_TYPE, TreeTypes.SPRUCE).get().createStack(), getFor("spruce log") / 4, 10000));
        items.add(new ItemPrice("birch planks", ItemTypes.PLANKS.getTemplate().with(Keys.TREE_TYPE, TreeTypes.BIRCH).get().createStack(), getFor("birch log") / 4, 10000));
        items.add(new ItemPrice("jungle planks", ItemTypes.PLANKS.getTemplate().with(Keys.TREE_TYPE, TreeTypes.JUNGLE).get().createStack(), getFor("jungle log") / 4, 10000));
        items.add(new ItemPrice("acacia planks", ItemTypes.PLANKS.getTemplate().with(Keys.TREE_TYPE, TreeTypes.ACACIA).get().createStack(), getFor("acacia log") / 4, 10000));
        items.add(new ItemPrice("dark oak planks", ItemTypes.PLANKS.getTemplate().with(Keys.TREE_TYPE, TreeTypes.DARK_OAK).get().createStack(), getFor("dark oak log") / 4, 10000));

        //stick
        items.add(new ItemPrice("stick", ItemTypes.STICK.getTemplate().createStack(), ((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)/2, 10000));

        //Bricks
        items.add(new ItemPrice("bricks", ItemTypes.BRICK_BLOCK.getTemplate().createStack(), getFor("brick")*4, 10000));
        //items.add(new ItemPrice("brick slab", ItemTypes.STONE_SLAB.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.BRICK).get().createStack(), getFor("bricks") /2, 10000));
            //Nether Bricks
        items.add(new ItemPrice("nether brick", ItemTypes.NETHER_BRICK.getTemplate().createStack(), getFor("netherrack")*1, 10000));
        items.add(new ItemPrice("nether bricks", ItemTypes.NETHER_BRICK.getTemplate().createStack(), getFor("nether brick")*4, 10000));
        //items.add(new ItemPrice("nether brick slab", ItemTypes.STONE_SLAB.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.NETHERBRICK).get().createStack(), getFor("nether bricks") /2, 10000));
        items.add(new ItemPrice("nether brick fence", ItemTypes.NETHER_BRICK_FENCE.getTemplate().createStack(), getFor("nether bricks")*1, 10000));
        items.add(new ItemPrice("red nether brick", ItemTypes.RED_NETHER_BRICK.getTemplate().createStack(), (getFor("nether brick")*2)+(getFor("nether wart")*2), 10000));
            //Stone Bricks
        items.add(new ItemPrice("stone bricks", ItemTypes.STONEBRICK.getTemplate().createStack(), getFor("stone")*1, 10000));
        items.add(new ItemPrice("mossy bricks", ItemTypes.STONEBRICK.getTemplate().with(Keys.BRICK_TYPE, BrickTypes.MOSSY).get().createStack(), getFor("stone bricks")+getFor("vines"), 10000));
        items.add(new ItemPrice("cracked stone", ItemTypes.STONEBRICK.getTemplate().with(Keys.BRICK_TYPE, BrickTypes.CRACKED).get().createStack(), getFor("stone bricks")*1, 10000));
        //items.add(new ItemPrice("stone brick slab", ItemTypes.STONE_SLAB.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.SMOOTH_BRICK).get().createStack(), getFor("stone bricks") /2, 10000));
        items.add(new ItemPrice("chiseled stone", ItemTypes.STONEBRICK.getTemplate().with(Keys.BRICK_TYPE, BrickTypes.CHISELED).get().createStack(), getFor("stone brick slab")*2, 10000));

        //Purpur
        items.add(new ItemPrice("popped chorus", ItemTypes.CHORUS_FRUIT_POPPED.getTemplate().createStack(), getFor("chorus fruit"), 10000));
        items.add(new ItemPrice("purpur block", ItemTypes.PURPUR_BLOCK.getTemplate().createStack(), getFor("popped chorus"), 10000));
        items.add(new ItemPrice("purpur slab", ItemTypes.PURPUR_SLAB.getTemplate().createStack(), getFor("purpur block") /2, 10000));
        items.add(new ItemPrice("purpur pillar", ItemTypes.PURPUR_PILLAR.getTemplate().createStack(), getFor("purpur slab") *2, 10000));
        items.add(new ItemPrice("purpur stairs", ItemTypes.PURPUR_STAIRS.getTemplate().createStack(), (getFor("purpur block") * 6)/4, 10000));

        //Sandstone
        items.add(new ItemPrice("sandstone", ItemTypes.SANDSTONE.getTemplate().createStack(), getFor("sand") * 4, 10000));
        //items.add(new ItemPrice("sandstone slab", ItemTypes.STONE_SLAB.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.SAND).get().createStack(), getFor("sandstone") /2, 10000));
        items.add(new ItemPrice("sandstone stairs", ItemTypes.SANDSTONE_STAIRS.getTemplate().createStack(), (getFor("sandstone")*6)/4, 10000));
        items.add(new ItemPrice("chiseled sandstone", ItemTypes.SANDSTONE.getTemplate().with(Keys.SANDSTONE_TYPE, SandstoneTypes.CHISELED).get().createStack(), getFor("sandstone slab") * 2, 10000));
        items.add(new ItemPrice("cut sandstone", ItemTypes.SANDSTONE.getTemplate().with(Keys.SANDSTONE_TYPE, SandstoneTypes.SMOOTH).get().createStack(), getFor("sandstone")*1, 10000));
        //smooth sandstone is something different in 1.13 so it was renamed to cut sandstone

        //Red Sandstone
        items.add(new ItemPrice("red sandstone", ItemTypes.RED_SANDSTONE.getTemplate().createStack(), getFor("red sand") * 4, 10000));
        //TODO
        //items.add(new ItemPrice("red sandstone slab", ItemTypes.STONE_SLAB2.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.RED_SAND).get().createStack(), getFor("red sandstone") /2, 10000));
        items.add(new ItemPrice("red sand stairs", ItemTypes.RED_SANDSTONE_STAIRS.getTemplate().createStack(), (getFor("red sandstone")*6)/4, 10000));
        items.add(new ItemPrice("chisel'd red sand", ItemTypes.RED_SANDSTONE.getTemplate().with(Keys.SANDSTONE_TYPE, SandstoneTypes.CHISELED).get().createStack(), getFor("red sandstone slab") * 2, 10000));
        items.add(new ItemPrice("Red Cut Sand", ItemTypes.RED_SANDSTONE.getTemplate().with(Keys.SANDSTONE_TYPE, SandstoneTypes.SMOOTH).get().createStack(), getFor("red sandstone")*1, 10000));

        //Wood Slabs
        items.add(new ItemPrice("oak slab", ItemTypes.WOODEN_SLAB.getTemplate().with(Keys.TREE_TYPE, TreeTypes.OAK).get().createStack(), getFor("oak planks") /2, 10000));
        items.add(new ItemPrice("spruce slab", ItemTypes.WOODEN_SLAB.getTemplate().with(Keys.TREE_TYPE, TreeTypes.SPRUCE).get().createStack(), getFor("spruce planks") /2, 10000));
        items.add(new ItemPrice("birch slab", ItemTypes.WOODEN_SLAB.getTemplate().with(Keys.TREE_TYPE, TreeTypes.BIRCH).get().createStack(), getFor("birch planks") /2, 10000));
        items.add(new ItemPrice("jungle slab", ItemTypes.WOODEN_SLAB.getTemplate().with(Keys.TREE_TYPE, TreeTypes.JUNGLE).get().createStack(), getFor("jungle planks") /2, 10000));
        items.add(new ItemPrice("acacia slab", ItemTypes.WOODEN_SLAB.getTemplate().with(Keys.TREE_TYPE, TreeTypes.ACACIA).get().createStack(), getFor("acacia planks") /2, 10000));
        items.add(new ItemPrice("dark oak slab", ItemTypes.WOODEN_SLAB.getTemplate().with(Keys.TREE_TYPE, TreeTypes.DARK_OAK).get().createStack(), getFor("dark oak planks") /2, 10000));

        //other slabs
        //items.add(new ItemPrice("stone slab", ItemTypes.STONE_SLAB.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.STONE).get().createStack(), getFor("stone") /2, 10000));
        //items.add(new ItemPrice("cobblestone slab", ItemTypes.STONE_SLAB.getTemplate().with(Keys.SLAB_TYPE, SlabTypes.COBBLESTONE).get().createStack(), getFor("cobblestone") /2, 10000));


        //Wood Stairs
        items.add(new ItemPrice("oak stairs", ItemTypes.OAK_STAIRS.getTemplate().createStack(), (getFor("oak planks")*6)/4, 10000));
        items.add(new ItemPrice("spruce stairs", ItemTypes.SPRUCE_STAIRS.getTemplate().createStack(), (getFor("spruce planks")*6)/4, 10000));
        items.add(new ItemPrice("birch stairs", ItemTypes.BIRCH_STAIRS.getTemplate().createStack(), (getFor("birch planks")*6)/4, 10000));
        items.add(new ItemPrice("jungle stairs", ItemTypes.JUNGLE_STAIRS.getTemplate().createStack(), (getFor("jungle planks")*6)/4, 10000));
        items.add(new ItemPrice("acacia stairs", ItemTypes.ACACIA_STAIRS.getTemplate().createStack(), (getFor("acacia planks")*6)/4, 10000));
        items.add(new ItemPrice("dark oak stairs", ItemTypes.DARK_OAK_STAIRS.getTemplate().createStack(), (getFor("dark oak planks")*6)/4, 10000));

        //other stairs
        items.add(new ItemPrice("cobblestone stairs", ItemTypes.STONE_STAIRS.getTemplate().createStack(), (getFor("cobblestone")*6)/4, 10000));
        items.add(new ItemPrice("brick stairs", ItemTypes.BRICK_STAIRS.getTemplate().createStack(), (getFor("bricks")*6)/4, 10000));
        items.add(new ItemPrice("stone brick stairs", ItemTypes.STONE_BRICK_STAIRS.getTemplate().createStack(), (getFor("stone bricks")*6)/4, 10000));
        items.add(new ItemPrice("nether brick stairs", ItemTypes.NETHER_BRICK_STAIRS.getTemplate().createStack(), (getFor("nether bricks")*6)/4, 10000));
        items.add(new ItemPrice("quartz stairs", ItemTypes.QUARTZ_STAIRS.getTemplate().createStack(), (getFor("quartz block")*6)/4, 10000));

        //Wood Fences
        items.add(new ItemPrice("oak fence", ItemTypes.FENCE.getTemplate().createStack(), (getFor("oak planks")*4)+(getFor("stick")*2), 10000));
        items.add(new ItemPrice("spruce fence", ItemTypes.SPRUCE_FENCE.getTemplate().createStack(), (getFor("spruce planks")*4)+(getFor("stick")*2), 10000));
        items.add(new ItemPrice("birch fence", ItemTypes.BIRCH_FENCE.getTemplate().createStack(), (getFor("birch planks")*4)+(getFor("stick")*2), 10000));
        items.add(new ItemPrice("jungle fence", ItemTypes.JUNGLE_FENCE.getTemplate().createStack(), (getFor("jungle planks")*4)+(getFor("stick")*2), 10000));
        items.add(new ItemPrice("acacia fence", ItemTypes.ACACIA_FENCE.getTemplate().createStack(), (getFor("acacia planks")*4)+(getFor("stick")*2), 10000));
        items.add(new ItemPrice("dark oak fence", ItemTypes.DARK_OAK_FENCE.getTemplate().createStack(), (getFor("dark oak planks")*4)+(getFor("stick")*2), 10000));

        //Wood Gates
        items.add(new ItemPrice("oak gate", ItemTypes.FENCE_GATE.getTemplate().createStack(), (getFor("oak planks")*2)+(getFor("stick")*4), 10000));
        items.add(new ItemPrice("spruce gate", ItemTypes.SPRUCE_FENCE_GATE.getTemplate().createStack(), (getFor("spruce planks")*2)+(getFor("stick")*4), 10000));
        items.add(new ItemPrice("birch gate", ItemTypes.BIRCH_FENCE_GATE.getTemplate().createStack(), (getFor("birch planks")*2)+(getFor("stick")*4), 10000));
        items.add(new ItemPrice("jungle gate", ItemTypes.JUNGLE_FENCE_GATE.getTemplate().createStack(), (getFor("jungle planks")*2)+(getFor("stick")*4), 10000));
        items.add(new ItemPrice("acacia gate", ItemTypes.ACACIA_FENCE_GATE.getTemplate().createStack(), (getFor("acacia planks")*2)+(getFor("stick")*4), 10000));
        items.add(new ItemPrice("dark oak gate", ItemTypes.DARK_OAK_FENCE_GATE.getTemplate().createStack(), (getFor("dark oak planks")*2)+(getFor("stick")*4), 10000));

        //Buttons (wooden ones have all types in 1.13)
        items.add(new ItemPrice("wooden button", ItemTypes.WOODEN_BUTTON.getTemplate().createStack(), getFor("oak planks")*1, 10000));
        items.add(new ItemPrice("stone button", ItemTypes.STONE_BUTTON.getTemplate().createStack(), getFor("stone")*1, 10000));

        //Pressure Plates (wooden ones have all types in 1.13)
        items.add(new ItemPrice("wooden plate", ItemTypes.WOODEN_PRESSURE_PLATE.getTemplate().createStack(), ((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*2, 10000));
        items.add(new ItemPrice("stone plate", ItemTypes.STONE_PRESSURE_PLATE.getTemplate().createStack(), getFor("stone")*2, 10000));
        items.add(new ItemPrice("gold plate", ItemTypes.LIGHT_WEIGHTED_PRESSURE_PLATE.getTemplate().createStack(), getFor("gold ingot")*2, 10000));
        items.add(new ItemPrice("iron plate", ItemTypes.HEAVY_WEIGHTED_PRESSURE_PLATE.getTemplate().createStack(), getFor("iron ingot")*2, 10000));

        //Doors
        items.add(new ItemPrice("iron door", ItemTypes.IRON_DOOR.getTemplate().createStack(), getFor("iron ingot")*3, 10000));
        items.add(new ItemPrice("oak door", ItemTypes.WOODEN_DOOR.getTemplate().createStack(), getFor("oak planks")*3, 10000));
        items.add(new ItemPrice("spruce door", ItemTypes.SPRUCE_DOOR.getTemplate().createStack(), getFor("spruce planks")*3, 10000));
        items.add(new ItemPrice("birch door", ItemTypes.BIRCH_DOOR.getTemplate().createStack(), getFor("birch planks")*3, 10000));
        items.add(new ItemPrice("jungle door", ItemTypes.JUNGLE_DOOR.getTemplate().createStack(), getFor("jungle planks")*3, 10000));
        items.add(new ItemPrice("acacia door", ItemTypes.ACACIA_DOOR.getTemplate().createStack(), getFor("acacia planks")*3, 10000));
        items.add(new ItemPrice("dark oak door", ItemTypes.DARK_OAK_DOOR.getTemplate().createStack(), getFor("dark oak planks")*3, 10000));

        //Stews and bowls
        items.add(new ItemPrice("bowl", ItemTypes.BOWL.getTemplate().createStack(), (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*3)/4, 10000));
        items.add(new ItemPrice("mushroom stew", ItemTypes.MUSHROOM_STEW.getTemplate().createStack(), getFor("bowl")+getFor("red mushroom")+getFor("brown mushroom"), 10000));
        items.add(new ItemPrice("rabbit stew", ItemTypes.RABBIT_STEW.getTemplate().createStack(), getFor("bowl")+((getFor("red mushroom")+getFor("brown mushroom"))/2)+getFor("baked potato")+getFor("cooked rabbit"), 10000));
        items.add(new ItemPrice("beetroot soup", ItemTypes.BEETROOT_SOUP.getTemplate().createStack(), getFor("bowl")+(getFor("beetroot")*6), 10000));

        //Carpets
        items.add(new ItemPrice("white carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), (getFor("white wool")*2)/3, 10000));
        items.add(new ItemPrice("orange carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), (getFor("orange wool")*2)/3, 10000));
        items.add(new ItemPrice("magenta carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), (getFor("magenta wool")*2)/3, 10000));
        items.add(new ItemPrice("lightBlue carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), (getFor("lightBlue wool")*2)/3, 10000));
        items.add(new ItemPrice("yellow carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), (getFor("yellow wool")*2)/3, 10000));
        items.add(new ItemPrice("lime carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), (getFor("lime wool")*2)/3, 10000));
        items.add(new ItemPrice("pink carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), (getFor("pink wool")*2)/3, 10000));
        items.add(new ItemPrice("gray carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), (getFor("gray wool")*2)/3, 10000));
        items.add(new ItemPrice("lightGray carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), (getFor("lightGray wool")*2)/3, 10000));
        items.add(new ItemPrice("cyan carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), (getFor("cyan wool")*2)/3, 10000));
        items.add(new ItemPrice("purple carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), (getFor("purple wool")*2)/3, 10000));
        items.add(new ItemPrice("blue carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), (getFor("blue wool")*2)/3, 10000));
        items.add(new ItemPrice("brown carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), (getFor("brown wool")*2)/3, 10000));
        items.add(new ItemPrice("green carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), (getFor("green wool")*2)/3, 10000));
        items.add(new ItemPrice("red carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), (getFor("red wool")*2)/3, 10000));
        items.add(new ItemPrice("black carpet", ItemTypes.CARPET.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), (getFor("black wool")*2)/3, 10000));

        //Glass/glass
        items.add(new ItemPrice("glass", ItemTypes.GLASS.getTemplate().createStack(), getFor("sand"), 100000));
        items.add(new ItemPrice("glass pane", ItemTypes.GLASS_PANE.getTemplate().createStack(), (getFor("glass")*6)/16, 10000));

        items.add(new ItemPrice("white glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("orange glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("magenta glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("lightBlue glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("yellow glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("lime glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("pink glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("gray glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("lightGray glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("cyan glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("purple glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("blue glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("brown glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("green glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("red glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), getFor("glass")*1, 10000));
        items.add(new ItemPrice("black glass", ItemTypes.STAINED_GLASS.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), getFor("glass")*1, 10000));

        items.add(new ItemPrice("white pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), (getFor("white glass")*6)/16, 10000));
        items.add(new ItemPrice("orange pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), (getFor("orange glass")*6)/16, 10000));
        items.add(new ItemPrice("magenta pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), (getFor("magenta glass")*6)/16, 10000));
        items.add(new ItemPrice("lightBlue pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), (getFor("lightBlue glass")*6)/16, 10000));
        items.add(new ItemPrice("yellow pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), (getFor("yellow glass")*6)/16, 10000));
        items.add(new ItemPrice("lime pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), (getFor("lime glass")*6)/16, 10000));
        items.add(new ItemPrice("pink pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), (getFor("pink glass")*6)/16, 10000));
        items.add(new ItemPrice("gray pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), (getFor("gray glass")*6)/16, 10000));
        items.add(new ItemPrice("lightGray pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), (getFor("lightGray glass")*6)/16, 10000));
        items.add(new ItemPrice("cyan pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), (getFor("cyan glass")*6)/16, 10000));
        items.add(new ItemPrice("purple pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), (getFor("purple glass")*6)/16, 10000));
        items.add(new ItemPrice("blue pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), (getFor("blue glass")*6)/16, 10000));
        items.add(new ItemPrice("brown pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), (getFor("brown glass")*6)/16, 10000));
        items.add(new ItemPrice("green pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), (getFor("green glass")*6)/16, 10000));
        items.add(new ItemPrice("red pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), (getFor("red glass")*6)/16, 10000));
        items.add(new ItemPrice("black pane", ItemTypes.STAINED_GLASS_PANE.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), (getFor("black glass")*6)/16, 10000));

        //Beds
        items.add(new ItemPrice("white bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.WHITE).get().createStack(), (getFor("white wool")*3)+(((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*3), 100000));
        items.add(new ItemPrice("orange bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.ORANGE).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("magenta bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.MAGENTA).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("lightBlue bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIGHT_BLUE).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("yellow bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.YELLOW).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("lime bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.LIME).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("pink bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.PINK).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("gray bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.GRAY).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("lightGray bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.SILVER).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("cyan bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.CYAN).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("purple bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.PURPLE).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("blue bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLUE).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("brown bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.BROWN).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("green bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.GREEN).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("red bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.RED).get().createStack(), getFor("white bed")*1, 100000));
        items.add(new ItemPrice("black bed", ItemTypes.BED.getTemplate().with(Keys.DYE_COLOR, DyeColors.BLACK).get().createStack(), getFor("white bed")*1, 100000));
        //Shulkers
        //??? no normal ?????????????????????????????????????????
        items.add(new ItemPrice("white shulker", ItemTypes.WHITE_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("bonemeal"), 1000));
        items.add(new ItemPrice("orange shulker", ItemTypes.ORANGE_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("orange dye"), 1000));
        items.add(new ItemPrice("magenta shulker", ItemTypes.MAGENTA_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("magenta dye"), 1000));
        items.add(new ItemPrice("lightBlue shulker", ItemTypes.LIGHT_BLUE_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("lightBlue dye"), 1000));
        items.add(new ItemPrice("yellow shulker", ItemTypes.YELLOW_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("dandelion yellow"), 1000));
        items.add(new ItemPrice("lime shulker", ItemTypes.LIME_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("lime dye"), 1000));
        items.add(new ItemPrice("pink shulker", ItemTypes.PINK_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("pink dye"), 1000));
        items.add(new ItemPrice("gray shulker", ItemTypes.GRAY_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("gray dye"), 1000));
        items.add(new ItemPrice("lightGray shulker", ItemTypes.SILVER_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("lightGray dye"), 1000));
        items.add(new ItemPrice("cyan shulker", ItemTypes.CYAN_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("cyan dye"), 1000));
        items.add(new ItemPrice("purple shulker", ItemTypes.PURPLE_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("purple dye"), 1000));
        items.add(new ItemPrice("blue shulker", ItemTypes.BLUE_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("lapis"), 1000));
        items.add(new ItemPrice("brown shulker", ItemTypes.BROWN_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("cocoa beans"), 1000));
        items.add(new ItemPrice("green shulker", ItemTypes.GREEN_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("cactus green"), 1000));
        items.add(new ItemPrice("red shulker", ItemTypes.RED_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("rose red"), 1000));
        items.add(new ItemPrice("black shulker", ItemTypes.BLACK_SHULKER_BOX.getTemplate().createStack(), (getFor("shulker shell")*2)+getFor("chest")+getFor("ink sac"), 1000));


        //Prismarine & Similar
        items.add(new ItemPrice("prismarine", ItemTypes.PRISMARINE.getTemplate().with(Keys.PRISMARINE_TYPE, PrismarineTypes.ROUGH).get().createStack(), getFor("prismarine shard") * 4, 10000));
        items.add(new ItemPrice("prismarine bricks", ItemTypes.PRISMARINE.getTemplate().with(Keys.PRISMARINE_TYPE, PrismarineTypes.BRICKS).get().createStack(), getFor("prismarine shard") * 9, 10000));
        items.add(new ItemPrice("dark prismarine", ItemTypes.PRISMARINE.getTemplate().with(Keys.PRISMARINE_TYPE, PrismarineTypes.DARK).get().createStack(), (getFor("prismarine shard") * 8)+getFor("ink sac"), 10000));
        items.add(new ItemPrice("sea lantern", ItemTypes.SEA_LANTERN.getTemplate().createStack(), (getFor("prismarine crystals") * 5)+(getFor("prismarine shard")*4), 10000));


        //Assorted stuff
        items.add(new ItemPrice("tnt", ItemTypes.TNT.getTemplate().createStack(), (getFor("sand") * 4)+(getFor("gunpowder") * 5), 10000));
        items.add(new ItemPrice("mossy cobble", ItemTypes.MOSSY_COBBLESTONE.getTemplate().createStack(), getFor("cobblestone")+getFor("vines"), 10000));
        items.add(new ItemPrice("bookshelf", ItemTypes.BOOKSHELF.getTemplate().createStack(), (getFor("book")*3)+(((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*6), 10000));
        items.add(new ItemPrice("torch", ItemTypes.TORCH.getTemplate().createStack(), getFor("coal")+getFor("stick"), 10000));
        items.add(new ItemPrice("chest", ItemTypes.CHEST.getTemplate().createStack(), ((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*8, 10000));
        items.add(new ItemPrice("crafting table", ItemTypes.CRAFTING_TABLE.getTemplate().createStack(), ((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*4, 10000));
        items.add(new ItemPrice("furnace", ItemTypes.FURNACE.getTemplate().createStack(), getFor("cobblestone")*8, 10000));
        items.add(new ItemPrice("glowstone", ItemTypes.GLOWSTONE.getTemplate().createStack(), getFor("glowstone dust")*4, 10000));
        items.add(new ItemPrice("jack o lantern", ItemTypes.LIT_PUMPKIN.getTemplate().createStack(), getFor("torch")+getFor("pumpkin"), 10000));
        items.add(new ItemPrice("snow block", ItemTypes.SNOW.getTemplate().createStack(), getFor("snowball")*4, 10000));
        items.add(new ItemPrice("snow layer", ItemTypes.SNOW_LAYER.getTemplate().createStack(), (getFor("snow block")*3)/6, 10000));
        items.add(new ItemPrice("jukebox", ItemTypes.JUKEBOX.getTemplate().createStack(), getFor("diamond")+(((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*8), 10000));
        //TRAPDOORS CHANGED IN 1.13 SO THIS WILL BE THE WOOD TYPE LATER ON
        items.add(new ItemPrice("trapdoor", ItemTypes.TRAPDOOR.getTemplate().createStack(), ((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*6, 10000));
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        items.add(new ItemPrice("iron trapdoor", ItemTypes.IRON_TRAPDOOR.getTemplate().createStack(), getFor("iron ingot")*4, 10000));
        items.add(new ItemPrice("iron bars", ItemTypes.IRON_BARS.getTemplate().createStack(), (getFor("iron ingot")*6)/16, 10000));
        items.add(new ItemPrice("enchanting table", ItemTypes.ENCHANTING_TABLE.getTemplate().createStack(), (getFor("obsidian")*4)+(getFor("diamond")*2)+getFor("book"), 10000));
        items.add(new ItemPrice("end stone bricks", ItemTypes.END_BRICKS.getTemplate().createStack(), getFor("end stone")*1, 10000));
        items.add(new ItemPrice("redstone lamp", ItemTypes.REDSTONE_LAMP.getTemplate().createStack(), (getFor("redstone")*4)+getFor("glowstone"), 10000));
        items.add(new ItemPrice("enderchest", ItemTypes.ENDER_CHEST.getTemplate().createStack(), (getFor("obsidian")*8)+getFor("eye of ender"), 10000));
        items.add(new ItemPrice("tripwire hook", ItemTypes.TRIPWIRE_HOOK.getTemplate().createStack(), (getFor("iron ingot") + getFor("stick")+((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6))/2, 10000));
        items.add(new ItemPrice("beacon", ItemTypes.BEACON.getTemplate().createStack(), (getFor("obsidian")*3) + (getFor("glass")*5) + getFor("nether star"), 10000));
        items.add(new ItemPrice("cobblestone wall", ItemTypes.COBBLESTONE_WALL.getTemplate().with(Keys.WALL_TYPE, WallTypes.NORMAL).get().createStack(), getFor("cobblestone"), 10000));
        items.add(new ItemPrice("mossy cobble wall", ItemTypes.COBBLESTONE_WALL.getTemplate().with(Keys.WALL_TYPE, WallTypes.MOSSY).get().createStack(), getFor("mossy cobble"), 10000));
        items.add(new ItemPrice("anvil", ItemTypes.ANVIL.getTemplate().createStack(), (getFor("iron ingot")*4)+(getFor("iron block")*3), 10000));
        items.add(new ItemPrice("packed ice", ItemTypes.PACKED_ICE.getTemplate().createStack(), getFor("ice")*9, 10000));
        items.add(new ItemPrice("slime block", ItemTypes.SLIME.getTemplate().createStack(), getFor("slimeball")*9, 10000));
        items.add(new ItemPrice("nether wart block", ItemTypes.NETHER_WART_BLOCK.getTemplate().createStack(), getFor("nether wart")*9, 10000));
        items.add(new ItemPrice("bone block", ItemTypes.BONE_BLOCK.getTemplate().createStack(), getFor("bonemeal")*9, 10000));
        // In 1.13 stripped logs affect charcoal
        items.add(new ItemPrice("charcoal", ItemTypes.COAL.getTemplate().with(Keys.COAL_TYPE, CoalTypes.CHARCOAL).get().createStack(), (getFor("oak log")+getFor("spruce log")+getFor("birch log")+getFor("jungle log")+getFor("acacia log")+getFor("dark oak log"))/6, 10000));
        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        items.add(new ItemPrice("sign", ItemTypes.SIGN.getTemplate().createStack(), getFor("stick")+ (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*6), 10000));
        items.add(new ItemPrice("compass", ItemTypes.COMPASS.getTemplate().createStack(), (getFor("iron ingot")*4) + getFor("redstone"), 10000));
        items.add(new ItemPrice("clock", ItemTypes.CLOCK.getTemplate().createStack(), (getFor("gold ingot")*4) + getFor("redstone"), 10000));
        items.add(new ItemPrice("fire charge", ItemTypes.FIRE_CHARGE.getTemplate().createStack(), (getFor("gunpowder")+getFor("blaze powder")+getFor("coal"))/3, 10000));
        items.add(new ItemPrice("book and quill", ItemTypes.WRITABLE_BOOK.getTemplate().createStack(), getFor("book") + getFor("feather")+getFor("ink sac"), 10000));
        items.add(new ItemPrice("item frame", ItemTypes.ITEM_FRAME.getTemplate().createStack(), (getFor("stick")*8) + getFor("leather"), 10000));
        items.add(new ItemPrice("flower pot", ItemTypes.FLOWER_POT.getTemplate().createStack(), getFor("brick")*3, 10000));
        items.add(new ItemPrice("armor stand", ItemTypes.ARMOR_STAND.getTemplate().createStack(), (getFor("stick")*6) + getFor("stone slab"), 10000));
        items.add(new ItemPrice("lead", ItemTypes.LEAD.getTemplate().createStack(), ((getFor("string")*4) + getFor("slimeball"))/2, 10000));
        items.add(new ItemPrice("end crystal", ItemTypes.END_CRYSTAL.getTemplate().createStack(), (getFor("glass")*7)+getFor("eye of ender")+getFor("ghast tear"), 10000));
        items.add(new ItemPrice("end rod", ItemTypes.END_ROD.getTemplate().createStack(), (getFor("popped chorus") + getFor("blaze rod"))/4, 10000));
        items.add(new ItemPrice("empty map", ItemTypes.MAP.getTemplate().createStack(), (getFor("paper")*8)+getFor("compass"), 10000));
        
        //Brewing
        items.add(new ItemPrice("brewing stand", ItemTypes.BREWING_STAND.getTemplate().createStack(), (getFor("cobblestone")*3)+getFor("blaze rod"), 10000));
        items.add(new ItemPrice("cauldron", ItemTypes.CAULDRON.getTemplate().createStack(), getFor("iron ingot")*7, 10000));
        items.add(new ItemPrice("glass bottle", ItemTypes.GLASS_BOTTLE.getTemplate().createStack(), (getFor("glass")*3)/3, 10000));
        //strange water bottle shit face
        items.add(new ItemPrice("water bottle", ItemTypes.POTION.getTemplate().with(Keys.POTION_TYPE, PotionTypes.WATER).get().createStack(), getFor("glass bottle"), 10000));
        items.add(new ItemPrice("dragon breath", ItemTypes.DRAGON_BREATH.getTemplate().createStack(), getFor("glass bottle")*1.2, 10000));
        items.add(new ItemPrice("fermented eye", ItemTypes.FERMENTED_SPIDER_EYE.getTemplate().createStack(), getFor("spider eye")+getFor("brown mushroom")+getFor("sugar"), 10000));
        items.add(new ItemPrice("magma cream", ItemTypes.MAGMA_CREAM.getTemplate().createStack(), getFor("slimeball")+getFor("blaze powder"), 10000));
        items.add(new ItemPrice("glistering melon", ItemTypes.SPECKLED_MELON.getTemplate().createStack(), getFor("melon")+(getFor("gold nugget")*8), 10000));

        //Weapons
            //Other
                //Bow
        items.add(new ItemPrice("bow", ItemTypes.BOW.getTemplate().createStack(), (getFor("string")*3) + (getFor("stick")*3), 10000));
        items.add(new ItemPrice("arrow", ItemTypes.ARROW.getTemplate().createStack(), (getFor("feather") + getFor("stick") + getFor("flint"))/4, 10000));
                //Flint and Steel
        items.add(new ItemPrice("flint and steel", ItemTypes.FLINT_AND_STEEL.getTemplate().createStack(), getFor("flint") + getFor("iron ingot"), 10000));
                //Shield
        items.add(new ItemPrice("shield", ItemTypes.SHIELD.getTemplate().createStack(), getFor("iron ingot") + (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*6), 10000));
            //Sword
        items.add(new ItemPrice("wooden sword", ItemTypes.WOODEN_SWORD.getTemplate().createStack(), getFor("stick") + (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*2), 10000));
        items.add(new ItemPrice("stone sword", ItemTypes.STONE_SWORD.getTemplate().createStack(), getFor("stick") + (getFor("cobblestone")*2), 10000));
        items.add(new ItemPrice("golden sword", ItemTypes.GOLDEN_SWORD.getTemplate().createStack(), getFor("stick") + (getFor("gold ingot")*2), 10000));
        items.add(new ItemPrice("iron sword", ItemTypes.IRON_SWORD.getTemplate().createStack(), getFor("stick") + (getFor("iron ingot")*2), 10000));
        items.add(new ItemPrice("diamond sword", ItemTypes.DIAMOND_SWORD.getTemplate().createStack(), getFor("stick") + (getFor("diamond")*2), 10000));

        //Tools
            //Fishing rods
        items.add(new ItemPrice("fishing rod", ItemTypes.FISHING_ROD.getTemplate().createStack(), (getFor("stick")*3) + (getFor("string")*2), 10000));
        items.add(new ItemPrice("carrot on a stick", ItemTypes.CARROT_ON_A_STICK.getTemplate().createStack(), getFor("fishing rod") + getFor("carrot"), 10000));
            //Shears
        items.add(new ItemPrice("shears", ItemTypes.SHEARS.getTemplate().createStack(), getFor("iron ingot")*2, 10000));
            //Pickaxe
        items.add(new ItemPrice("wooden pickaxe", ItemTypes.WOODEN_PICKAXE.getTemplate().createStack(), (getFor("stick")*2) + (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*3), 10000));
        items.add(new ItemPrice("stone pickaxe", ItemTypes.STONE_PICKAXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("cobblestone")*3), 10000));
        items.add(new ItemPrice("golden pickaxe", ItemTypes.GOLDEN_PICKAXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("gold ingot")*3), 10000));
        items.add(new ItemPrice("iron pickaxe", ItemTypes.IRON_PICKAXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("iron ingot")*3), 10000));
        items.add(new ItemPrice("diamond pickaxe", ItemTypes.DIAMOND_PICKAXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("diamond")*3), 10000));
            //Axe
        items.add(new ItemPrice("wooden axe", ItemTypes.WOODEN_AXE.getTemplate().createStack(), (getFor("stick")*2) + (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*3), 10000));
        items.add(new ItemPrice("stone axe", ItemTypes.STONE_AXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("cobblestone")*3), 10000));
        items.add(new ItemPrice("golden axe", ItemTypes.GOLDEN_AXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("gold ingot")*3), 10000));
        items.add(new ItemPrice("iron axe", ItemTypes.IRON_AXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("iron ingot")*3), 10000));
        items.add(new ItemPrice("diamond axe", ItemTypes.DIAMOND_AXE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("diamond")*3), 10000));
            //Shovel
        items.add(new ItemPrice("wooden shovel", ItemTypes.WOODEN_SHOVEL.getTemplate().createStack(), (getFor("stick")*2) + (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*1), 10000));
        items.add(new ItemPrice("stone shovel", ItemTypes.STONE_SHOVEL.getTemplate().createStack(), (getFor("stick")*2) + (getFor("cobblestone")*1), 10000));
        items.add(new ItemPrice("golden shovel", ItemTypes.GOLDEN_SHOVEL.getTemplate().createStack(), (getFor("stick")*2) + (getFor("gold ingot")*1), 10000));
        items.add(new ItemPrice("iron shovel", ItemTypes.IRON_SHOVEL.getTemplate().createStack(), (getFor("stick")*2) + (getFor("iron ingot")*1), 10000));
        items.add(new ItemPrice("diamond shovel", ItemTypes.DIAMOND_SHOVEL.getTemplate().createStack(), (getFor("stick")*2) + (getFor("diamond")*1), 10000));
            //Hoe
        items.add(new ItemPrice("wooden hoe", ItemTypes.WOODEN_HOE.getTemplate().createStack(), (getFor("stick")*2) + (((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*2), 10000));
        items.add(new ItemPrice("stone hoe", ItemTypes.STONE_HOE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("cobblestone")*3), 10000));
        items.add(new ItemPrice("golden hoe", ItemTypes.GOLDEN_HOE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("gold ingot")*3), 10000));
        items.add(new ItemPrice("iron hoe", ItemTypes.IRON_HOE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("iron ingot")*3), 10000));
        items.add(new ItemPrice("diamond hoe", ItemTypes.DIAMOND_HOE.getTemplate().createStack(), (getFor("stick")*2) + (getFor("diamond")*3), 10000)); //don't craft a diamond hoe, it's useless

        //Armor
            //Diamond Armor
        items.add(new ItemPrice("diamond helmet", ItemTypes.DIAMOND_HELMET.getTemplate().createStack(), getFor("diamond") * 5, 250));
        items.add(new ItemPrice("diamond chest", ItemTypes.DIAMOND_CHESTPLATE.getTemplate().createStack(), getFor("diamond") * 8, 250));
        items.add(new ItemPrice("diamond leggings", ItemTypes.DIAMOND_LEGGINGS.getTemplate().createStack(), getFor("diamond") * 7, 250));
        items.add(new ItemPrice("diamond boots", ItemTypes.DIAMOND_BOOTS.getTemplate().createStack(), getFor("diamond") * 4, 250));
            //Gold Armor
        items.add(new ItemPrice("gold helmet", ItemTypes.GOLDEN_HELMET.getTemplate().createStack(), getFor("gold ingot") * 5, 250));
        items.add(new ItemPrice("gold chestplate", ItemTypes.GOLDEN_CHESTPLATE.getTemplate().createStack(), getFor("gold ingot") * 8, 250));
        items.add(new ItemPrice("gold leggings", ItemTypes.GOLDEN_LEGGINGS.getTemplate().createStack(), getFor("gold ingot") * 7, 250));
        items.add(new ItemPrice("gold boots", ItemTypes.GOLDEN_BOOTS.getTemplate().createStack(), getFor("gold ingot") * 4, 250));
            //Iron Armor
        items.add(new ItemPrice("iron helmet", ItemTypes.IRON_HELMET.getTemplate().createStack(), getFor("iron ingot") * 5, 250));
        items.add(new ItemPrice("iron chestplate", ItemTypes.IRON_CHESTPLATE.getTemplate().createStack(), getFor("iron ingot") * 8, 250));
        items.add(new ItemPrice("iron leggings", ItemTypes.IRON_LEGGINGS.getTemplate().createStack(), getFor("iron ingot") * 7, 250));
        items.add(new ItemPrice("iron boots", ItemTypes.IRON_BOOTS.getTemplate().createStack(), getFor("iron ingot") * 4, 250));
            //Leather Armor
        items.add(new ItemPrice("leather helmet", ItemTypes.LEATHER_HELMET.getTemplate().createStack(), getFor("leather") * 5, 250));
        items.add(new ItemPrice("leather chestplate", ItemTypes.LEATHER_CHESTPLATE.getTemplate().createStack(), getFor("leather") * 8, 250));
        items.add(new ItemPrice("leather leggings", ItemTypes.LEATHER_LEGGINGS.getTemplate().createStack(), getFor("leather") * 7, 250));
        items.add(new ItemPrice("leather boots", ItemTypes.LEATHER_BOOTS.getTemplate().createStack(), getFor("leather") * 4, 250));

        //Redstone
        items.add(new ItemPrice("dispenser", ItemTypes.DISPENSER.getTemplate().createStack(), getFor("redstone") + getFor("bow") + getFor("cobblestone")*7, 10000));
        items.add(new ItemPrice("noteblock", ItemTypes.NOTEBLOCK.getTemplate().createStack(), getFor("redstone") + ((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*8, 10000));
        items.add(new ItemPrice("piston", ItemTypes.PISTON.getTemplate().createStack(), getFor("redstone") + getFor("iron ingot") + (getFor("cobblestone")*4) + ((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks")/6)*3), 10000));
        items.add(new ItemPrice("sticky piston", ItemTypes.STICKY_PISTON.getTemplate().createStack(), getFor("piston") + getFor("slimeball"), 10000));
        items.add(new ItemPrice("lever", ItemTypes.LEVER.getTemplate().createStack(), getFor("stick")+getFor("cobblestone"), 10000));
        items.add(new ItemPrice("redstone torch", ItemTypes.REDSTONE_TORCH.getTemplate().createStack(), getFor("stick")+getFor("redstone"), 10000));
        items.add(new ItemPrice("button", ItemTypes.STONE_BUTTON.getTemplate().createStack(), getFor("stone"), 10000));
        items.add(new ItemPrice("trapped chest", ItemTypes.TRAPPED_CHEST.getTemplate().createStack(), getFor("chest")+getFor("tripwire hook"), 10000));
        items.add(new ItemPrice("daylight detector", ItemTypes.DAYLIGHT_DETECTOR.getTemplate().createStack(), (getFor("glass")*3)+(getFor("quartz")*3)+((getFor("oak planks")+getFor("spruce planks")+getFor("birch planks")+getFor("jungle planks")+getFor("acacia planks")+getFor("dark oak planks"))/6)*3, 10000));
        items.add(new ItemPrice("hopper", ItemTypes.HOPPER.getTemplate().createStack(), getFor("chest")+(getFor("iron ingot")*5), 10000));
        items.add(new ItemPrice("dropper", ItemTypes.DROPPER.getTemplate().createStack(), (getFor("cobblestone")*7)+getFor("redstone"), 10000));
        items.add(new ItemPrice("observer", ItemTypes.OBSERVER.getTemplate().createStack(), (getFor("cobblestone")*6)+(getFor("redstone")*2)+getFor("quartz"), 10000));
        items.add(new ItemPrice("repeater", ItemTypes.REPEATER.getTemplate().createStack(), (getFor("stone")*3)+(getFor("redstone torch")*2)+getFor("redstone"), 10000));
        items.add(new ItemPrice("comparator", ItemTypes.COMPARATOR.getTemplate().createStack(), (getFor("stone")*3)+(getFor("redstone torch")*3)+getFor("quartz"), 10000));

        //Boats
        items.add(new ItemPrice("oak boat", ItemTypes.BOAT.getTemplate().createStack(), getFor("oak planks") * 5, 10000));
        items.add(new ItemPrice("spruce boat", ItemTypes.SPRUCE_BOAT.getTemplate().createStack(), getFor("spruce planks") * 5, 10000));
        items.add(new ItemPrice("birch boat", ItemTypes.BIRCH_BOAT.getTemplate().createStack(), getFor("birch planks") * 5, 10000));
        items.add(new ItemPrice("jungle boat", ItemTypes.JUNGLE_BOAT.getTemplate().createStack(), getFor("jungle planks") * 5, 10000));
        items.add(new ItemPrice("acacia boat", ItemTypes.ACACIA_BOAT.getTemplate().createStack(), getFor("acacia planks") * 5, 10000));
        items.add(new ItemPrice("dark oak boat", ItemTypes.DARK_OAK_BOAT.getTemplate().createStack(), getFor("dark oak planks") * 5, 10000));

        //Minecarts
        items.add(new ItemPrice("minecart", ItemTypes.MINECART.getTemplate().createStack(), getFor("iron ingot") * 5, 10000));
        items.add(new ItemPrice("chest minecart", ItemTypes.CHEST_MINECART.getTemplate().createStack(), getFor("minecart")+getFor("chest"), 10000));
        items.add(new ItemPrice("furnace minecart", ItemTypes.FURNACE_MINECART.getTemplate().createStack(), getFor("minecart")+getFor("furnace"), 10000));
        items.add(new ItemPrice("tnt minecart", ItemTypes.TNT_MINECART.getTemplate().createStack(), getFor("minecart")+getFor("tnt"), 10000));
        items.add(new ItemPrice("hopper minecart", ItemTypes.HOPPER_MINECART.getTemplate().createStack(), getFor("minecart")+getFor("hopper"), 10000));
        items.add(new ItemPrice("rail", ItemTypes.RAIL.getTemplate().createStack(), ((getFor("iron ingot")*6)+getFor("stick"))/16, 10000));
        items.add(new ItemPrice("powered rail", ItemTypes.GOLDEN_RAIL.getTemplate().createStack(), ((getFor("gold ingot") * 6)+getFor("redstone")+getFor("stick"))/6, 10000));
        items.add(new ItemPrice("detector rail", ItemTypes.DETECTOR_RAIL.getTemplate().createStack(), ((getFor("iron ingot") * 6)+getFor("redstone")+getFor("stone plate"))/6, 10000));
        items.add(new ItemPrice("activator rail", ItemTypes.ACTIVATOR_RAIL.getTemplate().createStack(), ((getFor("iron ingot")*6)+(getFor("stick")*2)+getFor("redstone torch"))/6, 10000));

        for (ItemPrice itemPrice : items) {
            if (itemPrice.getPrice() < 0) {
                LoggerHelper.getLogger().warn("Negative item price: " + itemPrice.getAlias());
            }
        }
    }

    public static ArrayList<ItemPrice> getItems() {
        return items;
    }

    public static ItemStack createStackFrom(String prettyName, int quantity) {
        for (ItemPrice itemPrice : items) {
            if (itemPrice.getAlias().equalsIgnoreCase(prettyName)) {
                ItemStack stack = itemPrice.getItem();
                stack.setQuantity(quantity);
                return stack;
            }
        }
        return ItemStack.empty();
    }

    public static ItemPrice get(String itemName) {
        for (ItemPrice itemPrice : items) {
            if (itemPrice.getAlias().equalsIgnoreCase(itemName)) {
                return itemPrice;
            }
        }
        return new ItemPrice("undefined", ItemStack.empty(), -1.0, 0);
    }

    public static double getFor(String item) {
        int bought = TransactionDataHandler.getBought(item);
        int sold = TransactionDataHandler.getSold(item);
        int stock = PriceReference.get(item).getStock();
        return get(item).getPrice() + ((double) (bought - sold) / (double) stock);
    }

    public static String getPrettyNameOf(ItemStack stack) {
        for (ItemPrice item : items) {
            ItemStack itemStack = item.getItem();
            itemStack.setQuantity(stack.getQuantity());
            if (stack.equalTo(itemStack)) {
                return item.getAlias();
            }
        }
        return "undefined";
    }

}
