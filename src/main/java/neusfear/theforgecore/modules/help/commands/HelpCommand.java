package neusfear.theforgecore.modules.help.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class HelpCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command gives a useful ping message."))
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("module")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if(src instanceof Player) {
                        Player player = (Player) src;
                        Optional<String> module = args.<String>getOne(Text.of("module"));
                        if (!module.isPresent()) {
                            src.sendMessage(MsgHelper.error("Spefify a module /help <module>"));
                            src.sendMessage(MsgHelper.info("Help Modules: &7chat, economy, essential, homes, infractions, islands, permissions, playerdata, referrals, reports, spawn, votes, warps"));
                        } else {
                            if (module.get().equals("staff")) {
                                if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getPermissionLevel() > 8) {
                                    PaginationList.builder()
                                            .title(Text.of("Help"))
                                            .contents(Text.of("/staff help"), Text.of("/staff setwarp"),
                                                    Text.of("/staff delwarp"), Text.of("/staff featurewarp"),
                                                    Text.of("/staff unfeaturewarp"), Text.of("/staff addinfraction"),
                                                    Text.of("/staff setspawn"), Text.of("/staff setfirstspawn"),
                                                    Text.of("/staff setrank"), Text.of("/staff homes"),
                                                    Text.of("/staff home"), Text.of("/staff delhome"),
                                                    Text.of("/staff islandteleport"), Text.of("/staff socialspy"),
                                                    Text.of("/staff comandspy"), Text.of("/staff broadcast"),
                                                    Text.of("/staff invsee"), Text.of("/staff +1"),
                                                    Text.of("/staff addxp"))
                                            .header(Text.of("TheForge Help Menu"))
                                            .padding(Text.of("-"))
                                            .sendTo(src);
                                }
                                return CommandResult.success();
                            }
                            if (module.get().equals("chat")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/message <player>"), Text.of("/reply <player>"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("economy")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/balance [<player>]"), Text.of("/deposit"),
                                                Text.of("/pay <player>"), Text.of("/withdraw"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("essential")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/ping"), Text.of("/rules"),
                                                Text.of("/seen <player>"), Text.of("/tpa <player>"),
                                                Text.of("/tpcancel"), Text.of("/tpdeny"), Text.of("/tpaccept"),
                                                Text.of("/afk"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("homes")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/home [<home>]"), Text.of("/sethome <name>"),
                                                Text.of("/homes"), Text.of("/delhome <home>"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("infractions")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/infractions [<player>]"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("islands")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/island info [<player>]"), Text.of("/island trust [<player>]"),
                                                Text.of("/island untrust [<player>]"), Text.of("/island home [<player>]"),
                                                Text.of("/island promote [<player>]"), Text.of("/island demote [<player>]"),
                                                Text.of("/island setname [<name>]"), Text.of("/island setdesc [<description>]"),
                                                Text.of("/island flag [<flag>] [<modification>]"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("permissions")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/rank list"), Text.of("/rank <rank>"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("playerdata")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/playerdata [<player>]"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("referrals")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/referral [<player>]"), Text.of("/referrals [<player>]"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("report")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/report <player> <reason>"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("spawn")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/spawn"), Text.of("/firstspawn"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("votes")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("No Commands yet."))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            if (module.get().equals("warps")) {
                                PaginationList.builder()
                                        .title(Text.of("Help"))
                                        .contents(Text.of("/advertisedwarps"), Text.of("/advertisewarp <warp>"),
                                                Text.of("/delwarp <warp>"), Text.of("/featuredwarps"),
                                                Text.of("/setwarp <warpname>"), Text.of("/warp <warp>"),
                                                Text.of("/warps"))
                                        .padding(Text.of("-"))
                                        .sendTo(src);
                                return CommandResult.success();
                            }
                            src.sendMessage(MsgHelper.error("You need to specify a valid module type. /help"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only command."));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "help");
    }
}
