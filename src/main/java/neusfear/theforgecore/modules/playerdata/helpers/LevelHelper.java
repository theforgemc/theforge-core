package neusfear.theforgecore.modules.playerdata.helpers;

import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.effect.sound.SoundCategories;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.title.Title;

public class LevelHelper {

    public static int getLevelForXP(int experience) {
        if (experience < getXPForLevel(2)) {
            return 1;
        }
        for (int i = 2; i < 100; i++) {
            if (experience > getXPForLevel(i) && experience < getXPForLevel(i + 1)) {
                return i;
            }
        }
        return 99;
    }

    public static int getXPForLevel(int level) {
        return (int) (((double)level+2000)*Math.pow(((double)level/5), 2.0));
    }

    public static void levelUp(PlayerData playerData) {
        for (Player player : Sponge.getServer().getOnlinePlayers()) {
            if (player.getName().equals(playerData.getUsername())) {
                player.sendTitle(Title.builder().title(MsgHelper.formatted("&6You Leveled up"))
                        .subtitle(MsgHelper.formatted("&fYou are now Level: &7" + LevelHelper.getLevelForXP(playerData.getExperience()))).build());
                player.playSound(SoundTypes.UI_TOAST_CHALLENGE_COMPLETE, SoundCategories.PLAYER, player.getLocation().getPosition(), 1);
            }
        }
    }
}
