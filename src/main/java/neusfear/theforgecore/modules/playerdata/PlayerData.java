package neusfear.theforgecore.modules.playerdata;

import neusfear.theforgecore.helpers.util.MathHelper;
import neusfear.theforgecore.modules.homes.Home;
import neusfear.theforgecore.modules.infractions.Infraction;
import neusfear.theforgecore.modules.islands.IslandInfo;
import neusfear.theforgecore.modules.playerdata.helpers.LevelHelper;
import neusfear.theforgecore.modules.referrals.Referral;
import neusfear.theforgecore.modules.serverdata.IslandDataHandler;
import neusfear.theforgecore.modules.warps.Warp;

import java.util.ArrayList;

public class PlayerData {

    private int version;
    private String uuid;
    private String username;
    private int index;
    private int permissionLevel;
    private String firstJoin;
    private String seen;
    private String lastJoinReward;
    private int consecJoins;
    private int ontime;
    private int afkLevel;
    private boolean online;
    private int experience;
    private int reputation;
    private double money;
    private ArrayList<Warp> warps = new ArrayList<>();
    private ArrayList<Infraction> infractions = new ArrayList<>();
    private String muted;
    private String banStatus;
    private ArrayList<Home> homes = new ArrayList<>();
    private ArrayList<IslandInfo> islands = new ArrayList<>();
    private ArrayList<Referral> referrals = new ArrayList<>();
    private int votes;
    private int consecutiveVotes;
    private String lastVote;


    public PlayerData() {}

    public PlayerData(int ver, String id, String name, int indexn, int permLevel, String fJoin, String see,
                      String ljw, int cJ, int pTime, int xp, int rep, int afk, boolean status, ArrayList<Warp> pwarps, double mon,
                      ArrayList<Infraction> inf, String mut, String bS, ArrayList<Home> home, ArrayList<IslandInfo> iInf,
                      ArrayList<Referral> ref, int vq, int vir, String lv) {
        version = ver;
        uuid = id;
        username = name;
        index = indexn;
        permissionLevel = permLevel;
        firstJoin = fJoin;
        seen = see;
        lastJoinReward = ljw;
        consecJoins = cJ;
        ontime = pTime;
        afkLevel = afk;
        online = status;
        experience = xp;
        reputation = rep;
        money = mon;
        warps = pwarps;
        infractions = inf;
        muted = mut;
        banStatus = bS;
        homes = home;
        islands = iInf;
        referrals = ref;
        votes = vq;
        consecutiveVotes = vir;
        lastVote = lv;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String id) {
        uuid = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        username = name;
    }

    public void setIslands(ArrayList<IslandInfo> islands) {
        this.islands = islands;
    }

    public ArrayList<IslandInfo> getIslands() {
        return islands;
    }

    public IslandInfo addIsland(String biome) {
        IslandInfo islandInfo = IslandDataHandler.createNewIslandInfo(biome);
        islands.add(islandInfo);
        IslandDataHandler.getIslandData().addNewIsland(getUuid());
        return islandInfo;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getFirstJoin() {
        return firstJoin;
    }

    public void setFirstJoin(String fJoin) {
        firstJoin = fJoin;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public int getConsecJoins() {
        return consecJoins;
    }

    public void addConsecJoin() {
        consecJoins++;
    }

    public void resetConsecJoins() {
        consecJoins = 1;
    }

    public void setConsecJoins(int cJ) {
        consecJoins = cJ;
    }

    public int getPermissionLevel() { return permissionLevel; }

    public void setPermissionLevel(int permLevel) {
        permissionLevel = permLevel;
    }

    public int getOntime() { return ontime; }

    public void setOntime(int pTime) { ontime = pTime; }

    public void addOntime(int min) { ontime += min; }

    public int getAfkLevel() { return afkLevel; }

    public void setAfkLevel(int afk) { afkLevel = afk; }

    public void addAfkLevel(int afk) { afkLevel += afk; }

    public boolean isOnline() { return online; }

    public void setOnline(boolean status) { online = status; }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void addExperience(int experience) {
        this.experience += experience;
        if (LevelHelper.getLevelForXP(this.experience) > LevelHelper.getLevelForXP(this.experience - experience)) {
            LevelHelper.levelUp(this);
        }
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    public void addReputation() {
        reputation++;
    }

    public ArrayList<Warp> getWarps() { return warps; }

    public void setWarps(ArrayList<Warp> pwarps) { warps = pwarps; }

    public void addWarp(Warp warp) { warps.add(warp); }

    public void removeWarp(Warp warp) { warps.remove(warp); }

    public double getMoney() { return MathHelper.roundAvoid(money, 2); }

    public void setMoney(double mon) { money = mon; }

    public void addMoney(double mon) { money += mon; }

    public void removeMoney(double mon) { money -= mon; }

    public ArrayList<Infraction> getInfractions() { return infractions; }

    public void setInfractions(ArrayList<Infraction> infractions) { this.infractions = infractions; }

    public void addInfraction(Infraction infraction) { this.infractions.add(infraction); }

    public String getMuted() {
        return muted;
    }

    public void setMuted(String muted) {
        this.muted = muted;
    }

    public String getBanStatus() {
        return banStatus;
    }

    public void setBanStatus(String banStatus) {
        this.banStatus = banStatus;
    }

    public ArrayList<Home> getHomes() { return homes; }

    public void setHomes(ArrayList<Home> homes) { this.homes = homes; }

    public void addHome(Home home) {
        for (Home oldHome : homes) {
            if (oldHome.getName().equalsIgnoreCase(home.getName())) {
                removeHome(oldHome);
            }
        }
        this.homes.add(home);
    }

    public void removeHome(Home home) { this.homes.remove(home); }

    public ArrayList<Referral> getReferrals() {
        return referrals;
    }

    public void setReferrals(ArrayList<Referral> referrals1) {
        this.referrals = referrals1;
    }

    public void addReferral(Referral referral) {
        this.referrals.add(referral);
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public void addVote() {
        this.votes++;
    }

    public int getConsecutiveVotes() {
        return consecutiveVotes;
    }

    public void setConsecutiveVotes(int consecutiveVotes) {
        this.consecutiveVotes = consecutiveVotes;
    }

    public void addConsecVote() {
        this.consecutiveVotes++;
    }

    public String getLastVote() {
        return lastVote;
    }

    public void setLastVote(String lastVote) {
        this.lastVote = lastVote;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getLastJoinReward() {
        return lastJoinReward;
    }

    public void setLastJoinReward(String lastJoinReward) {
        this.lastJoinReward = lastJoinReward;
    }
}
