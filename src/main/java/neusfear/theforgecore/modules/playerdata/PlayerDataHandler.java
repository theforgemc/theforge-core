package neusfear.theforgecore.modules.playerdata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.PlayerDataHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.permissions.Permissions;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.votes.VoteHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.user.UserStorageService;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


//This class is used to create a new playerdata file for a player when they join.
public class PlayerDataHandler {

    private static PlayerDataHandler playerDataHandler;
    private static ArrayList<PlayerData> loadedPlayers = new ArrayList<>();
    private static final int PLAYERDATA_VERSION = 3;

    public PlayerDataHandler() {
        playerDataHandler = this;

        //Incriment player OnlineTime every 1 min
        Task playTimeTask = Task.builder().execute(() -> updateAllOntime())
                .delay(1, TimeUnit.MINUTES).interval(1, TimeUnit.MINUTES)
                .name("TheForgeCore - Ontime updates.").submit(TheForgeCore.getInstance());

        //Creating the playerdata folder if it doesn't exist
        File pdDir = new File(String.valueOf(PlayerDataHelper.getPlayerdataDirectory()) + File.separator);
        if (!pdDir.exists()) {
            createPlayerdataDir(pdDir);
        }
        LoggerHelper.getLogger().info("Registered PlayerData Handler.");

        loadAllPlayerdata();
        LoggerHelper.getLogger().info("Loaded all PlayerData");
    }

    public static Optional<User> getUserFromName(String name) {
        Optional<UserStorageService> userStorage = Sponge.getServiceManager().provide(UserStorageService.class);
        return userStorage.get().get(name);
    }

    public static Optional<User> getUserFromID(String uuid) {
        Optional<UserStorageService> userStorage = Sponge.getServiceManager().provide(UserStorageService.class);
        return userStorage.get().get(UUID.fromString(uuid));
    }

    public static String getUserFromUUID(String uuid) {
        PlayerData playerData = getPlayerdata(uuid);
        return playerData.getUsername();
    }

    public void createPlayerdataDir(File pdDir) {
        LoggerHelper.getLogger().info("Could not find playerdata directory, we're making you one.");

        try{
            pdDir.mkdir();
            LoggerHelper.getLogger().info("playerdata file created.");
        } catch(SecurityException se) {
            LoggerHelper.getLogger().warn("Security Exception:" + se);
        }
    }

    public static boolean hasJoined(String uuid) {
        return getPlayerdata(uuid) != null && !getPlayerdata(uuid).getUsername().equals("null");
    }

    public static boolean hasUserJoined(String username) {
        return getPlayerdataFromUsername(username) != null && !getPlayerdataFromUsername(username).getUsername().equals("null");
    }

    //Creates a new playerdata file and adds the playerdata to the loaded playerdata list
    public static void createNewPlayerdata(String uuid) {
        PlayerData playerData = new PlayerData();

        playerData.setUuid(uuid);
        playerData.setUsername("null");
        playerData.setIndex(ServerDataHandler.getServerData().incrementUniquePlayers());
        playerData.setPermissionLevel(0);
        playerData.setFirstJoin(Time.getNow().toString());
        playerData.setSeen(Time.getNow().toString());
        playerData.setLastJoinReward(Time.getNow().toString());
        playerData.setOntime(0);
        playerData.setAfkLevel(0);
        playerData.setExperience(0);
        playerData.setReputation(0);
        playerData.setMoney(100.0);
        playerData.setWarps(playerData.getWarps());
        playerData.setInfractions(playerData.getInfractions());
        playerData.setMuted("false");
        playerData.setBanStatus("false");
        playerData.setHomes(playerData.getHomes());
        playerData.setIslands(playerData.getIslands());
        playerData.setReferrals(playerData.getReferrals());
        playerData.setConsecJoins(1);
        playerData.setVotes(0);
        playerData.setConsecutiveVotes(0);
        playerData.setLastVote(Time.getFutureDate(-24));

        addPlayerdata(playerData);

        savePlayerdata(playerData.getUuid());
    }

    public static void save(String uuid) {
        PlayerData playerData = getPlayerdata(uuid);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(playerData);
        try (FileWriter fileWriter = new FileWriter(PlayerDataHelper.getPlayerdataFile(playerData.getUuid()))) {
            fileWriter.write(jsonString);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void savePlayerdata(String uuid) {
        savePlayerdata(uuid, false);
    }

    //Asynchronously saves the specified PlayerData instance to the respective file.
    public static void savePlayerdata(String uuid, boolean useMainThread) {
        if (useMainThread) {
            save(uuid);
        } else {
            Task.builder().execute(() -> {
                save(uuid);
            }).async().submit(TheForgeCore.getInstance());
        }
    }

    public static void saveAllPlayerdata() {
        saveAllPlayerdata(false);
    }

    //Saves all loaded PlayerData objects to their file instances.
    public static void saveAllPlayerdata(boolean useMainThread) {
        if (useMainThread) {
            for (PlayerData playerData: PlayerDataHandler.getLoadedPlayers()) {
                savePlayerdata(playerData.getUuid(), true);
            }
            LoggerHelper.getLogger().info("Saved " + PlayerDataHandler.getLoadedPlayers().size() + " PlayerData files on the main thread.");
        } else {
            Task.builder().execute(() -> {
                for (PlayerData playerData: PlayerDataHandler.getLoadedPlayers()) {
                    savePlayerdata(playerData.getUuid());
                }
                LoggerHelper.getLogger().info("Saved " + PlayerDataHandler.getLoadedPlayers().size() + " PlayerData files asynchronously.");
            }).async().submit(TheForgeCore.getInstance());
        }
    }

    //Returns the playerdata of the player with the given uuid, if it's not present in the list of loaded Players
    public static PlayerData getPlayerdata(String uuid) {
        for (PlayerData playerData: loadedPlayers) {
            if (playerData.getUuid().equals(uuid)) {
                return playerData;
            }
        }
        return null;
    }

    //Returns the playerdata of the player with the given Username
    public static PlayerData getPlayerdataFromUsername(String username) {
        for (PlayerData playerData : loadedPlayers) {
            if (playerData.getUsername().equalsIgnoreCase(username)) {
                return playerData;
            }
        }
        return null;
    }

    //Check for playerdata update
    public static void testAndUpdatePlayerdata(PlayerData playerData) {
        if (playerData.getVersion() < PLAYERDATA_VERSION) {
            if (playerData.getVersion() == 0) {
                playerData.setVersion(1);
            }
            if (playerData.getVersion() == 1) {
                playerData.setReferrals(playerData.getReferrals());
                playerData.setConsecJoins(playerData.getConsecJoins());
                playerData.setVotes(playerData.getVotes());
                playerData.setConsecutiveVotes(playerData.getConsecutiveVotes());
                if (playerData.getLastVote() == null) {
                    playerData.setLastVote(Time.getFutureDate(-24));
                } else {
                    playerData.setLastVote(playerData.getLastVote());
                }
                playerData.setVersion(2);
            }
            if (playerData.getVersion() == 2) {
                playerData.setLastJoinReward(playerData.getSeen());
                playerData.setVersion(3);
            }
            if (playerData.getVersion() == PLAYERDATA_VERSION) {
                return;
            }
            LoggerHelper.getLogger().error("There was an error updating playerdata for: " + playerData.getUuid());
        }
    }

    //Reads the specified playerdata file specified and loads the PlayerData object to the list of loaded Players
    public static void loadPlayerdata(String uuid) {
        File file = PlayerDataHelper.getPlayerdataFile(uuid);
        try {
            if (file.exists()) {
                Gson gson = new Gson();
                BufferedReader br = new BufferedReader(new FileReader(file));

                PlayerData playerData = gson.fromJson(br, PlayerData.class);
                //Checkout old playerdata and add new values to prevent nullpointers
                testAndUpdatePlayerdata(playerData);

                //Make sure vote consecutive is up-to-date on server start (or try)
                VoteHandler.tryResetConsec(playerData);

                //Add to list
                addPlayerdata(playerData);
            } else {
                return;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void loadAllPlayerdata() {
        File[] pdDir = new File(PlayerDataHelper.getPlayerdataDirectory() + File.separator).listFiles();
        for (int i = 0; i < pdDir.length; i++) {
            if (pdDir[i].isFile()) {
                loadPlayerdata(pdDir[i].getName().replace(".json", ""));
            }
        }
    }

    //Adds the specified PlayerData object to the list of all the loaded PlayerData objects
    public static void addPlayerdata(PlayerData playerData) {
        loadedPlayers.add(playerData);
    }

    //Returns the list of loaded PlayerData objects
    public static ArrayList<PlayerData> getLoadedPlayers() {
        return loadedPlayers;
    }

    public static void updateAllOntime() {
        for (PlayerData playerData: getLoadedPlayers()) {
            if(playerData.isOnline()) {
                //If the player is not AFK
                if (!(playerData.getAfkLevel() > 5)) {
                    playerData.addOntime(1);
                }
                playerData.addAfkLevel(1);
                if (playerData.getAfkLevel() == 5) {
                    Sponge.getServer().getBroadcastChannel().send(MsgHelper.formatted("&7* &f" +
                            Permissions.getRankPrefix(playerData.getPermissionLevel()) + " " + playerData.getUsername() + " &7is now AFK"));
                }
                if (playerData.getAfkLevel() > 10) {
                    if (playerData.getPermissionLevel() >= 8) {
                        playerData.setAfkLevel(6);
                    } else {
                        Optional<Player> player = Sponge.getServer().getPlayer(UUID.fromString(playerData.getUuid()));
                        if (player.isPresent()) {
                            player.get().kick(MsgHelper.formatted("You we're afk for longer than 10 minutes."));
                        }
                    }
                }
            }
        }
    }
}
