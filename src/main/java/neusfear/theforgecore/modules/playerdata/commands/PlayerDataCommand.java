package neusfear.theforgecore.modules.playerdata.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.permissions.Permissions;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.playerdata.helpers.LevelHelper;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class PlayerDataCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command shows the playerdata of the player"))

                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player"))))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        String player = args.<String>getOne("player").orElse(src.getName());

                        Optional<User> user = PlayerDataHandler.getUserFromName(player);
                        if (user.isPresent()) {
                            String uuid = user.get().getUniqueId().toString();
                            sendPlayerDataMessage(src, uuid);
                        } else {
                            String uuid = ((Player) src).getUniqueId().toString();
                            if (player.equals(PlayerDataHandler.getPlayerdata(uuid).getUsername())) {
                                sendPlayerDataMessage(src, uuid);
                            } else {
                                src.sendMessage(MsgHelper.error("That player does not exist."));
                            }
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only command."));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "playerdata", "pdata", "pd", "playerinfo", "pinfo", "pinf", "playerinf");
    }

    private static void sendPlayerDataMessage(CommandSource src, String uuid) {
        PlayerData playerData = PlayerDataHandler.getPlayerdata(uuid);
        src.sendMessage(MsgHelper.info("&fPlayerData"));
        src.sendMessage(MsgHelper.interactablePiece("&fUser: &7" + playerData.getUsername(), playerData.getUuid(), "/msg " + playerData.getUsername()));
        src.sendMessage(MsgHelper.interactablePiece("&fLevel: &7" + LevelHelper.getLevelForXP(playerData.getExperience()), "&fUntil Next Level: &7" + playerData.getExperience() + "&f/&7" + LevelHelper.getXPForLevel(LevelHelper.getLevelForXP(playerData.getExperience())+1)));
        src.sendMessage(MsgHelper.interactablePiece("&fReputation: &7" + playerData.getReputation(), "&7The number of times staff has noticed this player stand out in their good deeds."));
        src.sendMessage(MsgHelper.interactablePiece("&fJoined: &7" + playerData.getFirstJoin(), "&fIndex: &7" + playerData.getIndex() + "\n&fConsecutive Joins: &7" + playerData.getConsecJoins()));
        src.sendMessage(MsgHelper.interactablePiece("&fPlaytime: &7" + Time.getTimeFromMin(playerData.getOntime()), "This number does not count time spent AFK."));
        src.sendMessage(MsgHelper.interactablePiece("&fRank: &7" + Permissions.getRankFromPermissionLevel(playerData.getPermissionLevel()), "Permission Level: &7" + playerData.getPermissionLevel()));
        src.sendMessage(MsgHelper.interactablePiece("&fInfractions: &7" + playerData.getInfractions().size(), "click for more info.", "/infractions " + playerData.getUsername()));
    }
}
