package neusfear.theforgecore.modules.chat;

import neusfear.theforgecore.helpers.ConfigHelper;
import neusfear.theforgecore.modules.permissions.Permissions;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

public class ConnectionMessages {

    public static void playerJoin(ClientConnectionEvent.Join event){
        Player player = (Player) event.getSource();
        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
        String playerRank = Permissions.getRankPrefix(playerData.getPermissionLevel());
        event.setMessage(Text.of(TextSerializers.FORMATTING_CODE.deserialize(ConfigHelper.getConfig().getNode("messages", "join").getString().replace("{PLAYER}",
                "" + playerRank + " " + event.getTargetEntity().getName() + "&f" ))));
    }

    public static void playerLeave(ClientConnectionEvent.Disconnect event) {
        Player player = (Player) event.getSource();
        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
        String playerRank = Permissions.getRankPrefix(playerData.getPermissionLevel());
        event.setMessage(Text.of(TextSerializers.FORMATTING_CODE.deserialize(ConfigHelper.getConfig().getNode("messages", "leave").getString().replace("{PLAYER}",
                "" + playerRank + " " + event.getTargetEntity().getName() + "&f" ))));
    }

    public static void welcomePlayerBack(Player player){
        player.sendMessage(Text.of(TextSerializers.FORMATTING_CODE
                .deserialize(ConfigHelper.getConfig().getNode("messages", "welcomeback").getString()
                .replace("{PLAYER}", Permissions.getRankPrefix(PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getPermissionLevel()) + " " + player.getName() + "&f" ))));
    }

    public static void welcomePlayer(ClientConnectionEvent.Join event) {
        Sponge.getServer().getBroadcastChannel().send(Text.of(TextSerializers.FORMATTING_CODE
                .deserialize(ConfigHelper.getConfig().getNode("messages", "welcome").getString()
                .replace("{PLAYER}", event.getTargetEntity().getName()))));
    }
}