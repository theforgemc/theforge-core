package neusfear.theforgecore.modules.chat.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class MessageCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command allows private messages to be sent between players."))

                .arguments(
                        GenericArguments.onlyOne(GenericArguments.player(Text.of("player"))),
                        GenericArguments.optionalWeak(GenericArguments.remainingJoinedStrings(Text.of("message")))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if (src instanceof Player) {

                        Player playerArg = args.<Player>getOne("player").get();

                        if (PlayerDataHandler.hasJoined(playerArg.getUniqueId().toString())) {
                            if (playerArg.isOnline()) {
                                sendMessage(src, playerArg, args.<String>getOne("message").get());
                            } else {
                                src.sendMessage(MsgHelper.error("The player you are trying to message is not online."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "message", "msg", "m");
    }

    public static void sendMessage(CommandSource sender, Player target, String message) {
        sender.sendMessage(MsgHelper.formatted("&6[&7You &8-> &f" + target.getName() + "&6]&7: &f" + message));
        target.sendMessage(MsgHelper.formatted("&6[&f" + sender.getName() + " &8->&7 You&6]&7: &f" + message));
        for (Player player : EssentialDataHander.getEssentialDataHander().getSocialSpies()) {
            player.sendMessage(MsgHelper.formatted("&8[&5SS&8]: [&7" + sender.getName() + " &8-> &7" + target.getName() + "&8] " + message));
        }
        EssentialDataHander.getEssentialDataHander().setRecentReply(target, (Player)sender);
    }
}
