package neusfear.theforgecore.modules.chat.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class ReplyCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command allows private messages to be sent between players."))

                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.remainingJoinedStrings(Text.of("message")))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if (src instanceof Player) {
                        String message = args.<String>getOne("message").get();
                        Player player = (Player) src;

                        if (EssentialDataHander.getEssentialDataHander().getRecipientForSender(player) != null) {
                            if (EssentialDataHander.getEssentialDataHander().getRecipientForSender(player).isOnline()) {
                                sendMessage(src, EssentialDataHander.getEssentialDataHander().getRecipientForSender(player), args.<String>getOne("message").get());
                            } else {
                                src.sendMessage(MsgHelper.error("The player you are replying to is no longer online."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("You have no such player to reply to."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command"));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "reply", "r");
    }

    public static void sendMessage(CommandSource sender, Player target, String message) {
        sender.sendMessage(MsgHelper.formatted("&6[&7You &8-> &f" + target.getName() + "&6]&7: &f" + message));
        target.sendMessage(MsgHelper.formatted("&6[&f" + sender.getName() + " &8->&7 You&6]&7: &f" + message));
        EssentialDataHander.getEssentialDataHander().setRecentReply(target, (Player)sender);
    }
}
