package neusfear.theforgecore.modules.dailyrewards;

public class DailyReward {

    private double money;
    private int XP;

    public DailyReward(double money, int exp) {
        this.money = money;
        this.XP = exp;
    }

    public double getMoney() {
        return money;
    }

    public int getXP() {
        return XP;
    }
}
