package neusfear.theforgecore.modules.dailyrewards;

import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.playerdata.PlayerData;

public class DailyRewards {

    public static void issueReward(PlayerData playerData) {
        playerData.addMoney(getReward(playerData.getConsecJoins()).getMoney());
        playerData.addExperience(getReward(playerData.getConsecJoins()).getXP());
        playerData.setLastJoinReward(Time.getNow().toString());
    }

    public static DailyReward getReward(int joins) {
        if (joins > 1 && joins < 3) {
            return new DailyReward(10, 2);
        } else if (joins > 2 && joins < 5) {
            return new DailyReward(50, 5);
        } else if (joins > 4 && joins < 10) {
            return new DailyReward(100, 15);
        } else if (joins > 9 && joins < 20) {
            return new DailyReward(250, 20);
        } else if (joins > 19 && joins < 30) {
            return new DailyReward(500, 25);
        } else if (joins > 29 && joins < 40) {
            return new DailyReward(750, 30);
        } else if (joins > 39 && joins < 50) {
            return new DailyReward(1000, 35);
        } else if (joins > 49 && joins < 60) {
            return new DailyReward(1500, 40);
        } else if (joins > 59 && joins < 70) {
            return new DailyReward(2000, 50);
        } else if (joins > 69 && joins < 80) {
            return new DailyReward(3000, 60);
        } else if (joins > 79 && joins < 90) {
            return new DailyReward(5000, 75);
        } else if (joins > 89 && joins < 100) {
            return new DailyReward(7500, 90);
        } else if (joins > 99 && joins < 365 ) {
            return new DailyReward(10000, 100);
        } else if (joins > 364) {
            return new DailyReward(25000, 250);
        } else {
            return new DailyReward(0, 0);
        }
    }
}
