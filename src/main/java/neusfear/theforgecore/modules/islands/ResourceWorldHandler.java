package neusfear.theforgecore.modules.islands;

import neusfear.theforgecore.helpers.LoggerHelper;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.DimensionTypes;
import org.spongepowered.api.world.GeneratorTypes;
import org.spongepowered.api.world.WorldArchetype;
import org.spongepowered.api.world.difficulty.Difficulties;
import org.spongepowered.api.world.storage.WorldProperties;

import java.io.IOException;

public class ResourceWorldHandler {

    public static final String worldName = "resource";
    private static WorldArchetype.Builder builder = WorldArchetype.builder();

    public static void createResourceWorld() {
        if (Sponge.getGame().getServer().getWorld(worldName).isPresent()) {
            LoggerHelper.getLogger().warn("Tried to create resource world when it already exists.");
            return;
        }
        builder.loadsOnStartup(true)
                .commandsAllowed(true)
                .generator(GeneratorTypes.OVERWORLD) //Use custom generator here later
                .dimension(DimensionTypes.OVERWORLD)
                .difficulty(Difficulties.NORMAL)
                .pvp(true)
                .usesMapFeatures(true);
        WorldArchetype settings = builder.enabled(true).build(worldName, worldName);
        WorldProperties properties = null;
        try {
            properties = Sponge.getServer().createWorldProperties(worldName, settings);
            //properties.setGeneratorModifiers(modifiers);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Sponge.getServer().saveWorldProperties(properties);
    }
}
