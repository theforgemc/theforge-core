package neusfear.theforgecore.modules.islands.generators;

import com.flowpowered.math.vector.Vector3i;
import neusfear.theforgecore.modules.islands.managers.IslandWorldManager;
import org.spongepowered.api.world.biome.BiomeTypes;
import org.spongepowered.api.world.extent.MutableBiomeVolume;
import org.spongepowered.api.world.gen.BiomeGenerator;

public class IslandBiomeGenerator implements BiomeGenerator{

    private final BiomeGenerator original;


    public IslandBiomeGenerator(BiomeGenerator original) {
        this.original = original;
    }


    @Override
    public void generateBiomes(MutableBiomeVolume buffer) {
        this.original.generateBiomes(buffer);

        Vector3i min = buffer.getBiomeMin();
        Vector3i max = buffer.getBiomeMax();

        for (int x = min.getX(); x <= max.getX(); x++) {
            for (int z = min.getZ(); z <= max.getZ(); z++) {
                if (IslandWorldManager.getOwnerOf(x, z) != null && !IslandWorldManager.getOwnerOf(x, z).equals("server")) {
                    buffer.setBiome(x, 0, z, IslandWorldManager.getBiomeTypeFrom(IslandWorldManager.getIslandInfoOf(x, z).getBiome()));
                } else {
                    buffer.setBiome(x, 0, z, BiomeTypes.VOID);
                }
            }
        }
    }
}
