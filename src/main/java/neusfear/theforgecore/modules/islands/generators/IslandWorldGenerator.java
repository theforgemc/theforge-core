package neusfear.theforgecore.modules.islands.generators;

import neusfear.theforgecore.helpers.ConfigHelper;
import neusfear.theforgecore.modules.islands.helpers.Perlin;
import neusfear.theforgecore.modules.islands.managers.IslandWorldManager;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.ImmutableBiomeVolume;
import org.spongepowered.api.world.extent.MutableBlockVolume;
import org.spongepowered.api.world.gen.GenerationPopulator;

public class IslandWorldGenerator implements GenerationPopulator {

    private static final int SIZE = ConfigHelper.getConfig().getNode("islands", "size").getInt();
    private static final int DIST = ConfigHelper.getConfig().getNode("islands", "distance").getInt(); //Must be an odd number > 3
    private static final double RADIUS = (SIZE / 2f);

    private static final Perlin SHAPE = new Perlin(9863, 30);
    private static final Perlin TEXTURE = new Perlin(2345, 4);
    private static final Perlin FEATURES = new Perlin(2360, 15);

    private double noise;
    private double height;

    @Override
    public void populate(World world, MutableBlockVolume buffer, ImmutableBiomeVolume biomes) {

        for (int x = buffer.getBlockMin().getX(); x <= buffer.getBlockMax().getX(); x++) {
            for (int z = buffer.getBlockMin().getZ(); z <= buffer.getBlockMax().getZ(); z++) {
                if(draw(x) && draw(z)) {
                    for(int y = getDefaultDepth(x, z); y < getDefaultHeight(x, z); y++) {
                        if (genYet(x, z)) {
                            buffer.setBlockType(x, y, z, BlockTypes.STONE);
                        }
                    }
                }
            }
        }
    }

    private static boolean genYet(int x, int z) {
        if (IslandWorldManager.getOwnerOf(x, z) != null) {
            return true;
        }
        return false;
    }

    private static boolean draw(int x) {
        return Math.abs(Math.floorMod(x,SIZE+DIST)) < SIZE;
    }

    private int getDefaultHeight(int x, int z) {
        noise = -10*SHAPE.getNoiseLevelAtPosition(x, z) + TEXTURE.getNoiseLevelAtPosition(x,z);
        //Makes the island have a hill in the middle (add to the height a little bit and multiply the noise a little bit)
        height = Math.pow(((SIZE / 2f) - ((distanceFromCenter(x, z))) * 1.5), 1.2) + (noise * (((SIZE / 2f) - distanceFromCenter(x, z)) / 5));
        return (int) (Math.rint((height/16))+64);
    }

    private int getDefaultDepth(int x, int z) {
        noise = -10*SHAPE.getNoiseLevelAtPosition(x, z) - 8*FEATURES.getNoiseLevelAtPosition(x, z);
        height = (Math.pow(((SIZE / 2f) - ((distanceFromCenter(x, z))) * 1.5), 1.2) + ((noise / 1) * (((SIZE / 2f) - distanceFromCenter(x, z)) / 5))) / 7;
        if ((-height)+64 < 0) {
            return 0;
        } else {
            return (int) (Math.rint(-height+64));
        }

    }

    private double distanceFromCenter(int x, int z) {
        return Math.sqrt(Math.pow((RADIUS - (Math.floorMod(x, SIZE + DIST))), 2.0) + Math.pow((RADIUS - (Math.floorMod(z, SIZE + DIST))), 2.0));
    }
}
