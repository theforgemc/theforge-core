package neusfear.theforgecore.modules.islands;

public class IslandFlag {

    private String name;
    private String description;
    private String value;

    public IslandFlag(String name, String description, String value) {
        setName(name);
        setDescription(description);
        setValue(String.valueOf(value));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public IslandFlag setValue(String value) {
        this.value = value;
        return this;
    }
}
