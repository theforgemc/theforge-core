package neusfear.theforgecore.modules.islands;

import neusfear.theforgecore.modules.islands.generators.IslandBiomeGenerator;
import neusfear.theforgecore.modules.islands.generators.IslandWorldGenerator;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.world.biome.BiomeGenerationSettings;
import org.spongepowered.api.world.biome.BiomeType;
import org.spongepowered.api.world.gen.WorldGenerator;
import org.spongepowered.api.world.gen.WorldGeneratorModifier;
import org.spongepowered.api.world.gen.populator.Fossil;
import org.spongepowered.api.world.storage.WorldProperties;

import java.util.Arrays;
import java.util.List;

public class IslandWorldModifier implements WorldGeneratorModifier{

    @Override
    public void modifyWorldGenerator(WorldProperties world, DataContainer settings, WorldGenerator worldGenerator) {
        /** This generates a completley empty world
        worldGenerator.getGenerationPopulators().clear();
        worldGenerator.getPopulators().clear();
        for (BiomeType biome: Sponge.getRegistry().getAllOf(BiomeType.class)) {
            BiomeGenerationSettings biomeSettings = worldGenerator.getBiomeSettings(biome);
            biomeSettings.getGenerationPopulators().clear();
            biomeSettings.getPopulators().clear();
            biomeSettings.getGroundCoverLayers().clear();
        }
        worldGenerator.setBaseGenerationPopulator((world1, buffer, biomes) -> { });
        worldGenerator.setBiomeGenerator(buffer -> buffer.getBiomeWorker().fill((x, y, z) -> BiomeTypes.VOID));
         **/
        world.setGenerateSpawnOnLoad(false);

        String[] disabledBiomeNames = new String[]{"Ocean", "Deep Ocean", "Beach", "FrozenRiver", "River", "Stone Beach", "FrozenOcean", "MushroomIslandShore", "Cold Beach", "Mesa", "Mesa Plateau F", "Mesa Plateau", "Mesa (Bryce)", "Bryce", "Badlands", "Wooded Badlands Plateau", "Modified Badlands Plateau", "Badlands Plateau", "Modified Wooded Badlands Plateau", "Eroded Badlands"};
        List<String> biomeList = Arrays.asList(disabledBiomeNames);

        for (BiomeType biome: Sponge.getRegistry().getAllOf(BiomeType.class)) {
            BiomeGenerationSettings biomeSettings = worldGenerator.getBiomeSettings(biome);
            if (biomeList.contains(biome.getName())) {
                biomeSettings.getGenerationPopulators().clear();
                biomeSettings.getPopulators().clear();
                biomeSettings.getGroundCoverLayers().clear();
            }
            biomeSettings.getPopulators(Fossil.class).clear();
        }
        worldGenerator.setBaseGenerationPopulator(new IslandWorldGenerator());
        worldGenerator.setBiomeGenerator(new IslandBiomeGenerator(worldGenerator.getBiomeGenerator()));
    }

    @Override
    public String getId() {
        return "theforge:islands";
    }

    @Override
    public String getName() {
        return "Islands Generator";
    }
}
