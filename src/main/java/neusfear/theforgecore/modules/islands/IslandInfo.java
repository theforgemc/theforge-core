package neusfear.theforgecore.modules.islands;

import neusfear.theforgecore.modules.warps.Warp;

import java.util.ArrayList;

public class IslandInfo {

    private String id;
    private int index;
    private String name;
    private String description;
    private ArrayList<String> members = new ArrayList<>();
    private ArrayList<String> coOwners = new ArrayList<>();
    private ArrayList<Warp> islandWarps = new ArrayList<>();
    private ArrayList<IslandFlag> flags = new ArrayList<>();
    private int experience;
    private String biome;

    public IslandInfo() {
        super();
    }

    public IslandInfo(String iid, int indx, String nam, String desc, ArrayList<String> mem, ArrayList<String> cOs, ArrayList<Warp> wrps, ArrayList<IslandFlag> flgs, int exp, String biome) {
        super();
        setId(iid);
        setIndex(index);
        setName(nam);
        setDescription(desc);
        setMembers(mem);
        setCoOwners(cOs);
        setIslandWarps(wrps);
        setFlags(flgs);
        setExperience(exp);
        setBiome(biome);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<String> members) {
        this.members = members;
    }

    public void addMember(String member) {
        this.members.add(member);
    }

    public void removeMember(String member) {
        this.members.remove(member);
    }

    public ArrayList<String> getCoOwners() {
        return coOwners;
    }

    public void setCoOwners(ArrayList<String> coOwners) {
        this.coOwners = coOwners;
    }

    public void addCoOwner(String coowner) {
        this.coOwners.add(coowner);
    }

    public void removeCoOwner(String coowner) {
        this.coOwners.remove(coowner);
    }

    public ArrayList<Warp> getIslandWarps() {
        return islandWarps;
    }

    public void setIslandWarps(ArrayList<Warp> islandWarps) {
        this.islandWarps = islandWarps;
    }

    public ArrayList<IslandFlag> getFlags() {
        return flags;
    }

    public void setFlags(ArrayList<IslandFlag> flags) {
        this.flags = flags;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getBiome() {
        return biome;
    }

    public void setBiome(String biome) {
        this.biome = biome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
