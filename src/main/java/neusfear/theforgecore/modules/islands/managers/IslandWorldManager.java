package neusfear.theforgecore.modules.islands.managers;

import com.flowpowered.math.vector.Vector3i;
import neusfear.theforgecore.helpers.ConfigHelper;
import neusfear.theforgecore.modules.islands.IslandInfo;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.IslandDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.biome.BiomeType;
import org.spongepowered.api.world.biome.BiomeTypes;

import java.util.ArrayList;

public class IslandWorldManager {


    private static final int SIZE = ConfigHelper.getConfig().getNode("islands", "size").getInt();
    private static final int DIST = ConfigHelper.getConfig().getNode("islands", "distance").getInt();

    public static Location<World> getIslandSafeStart(String id) {
        World world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);

        int x = IslandWorldManager.getIslandXCenterCoord(id);
        int z = IslandWorldManager.getIslandZCenterCoord(id);

        Location<World> location = new Location<World>(world, x, 100, z);
        world.loadChunk(location.getChunkPosition(), true);

        //Just in case they dig a hole straight down the middle of their island into the void.
        if (world.getHighestYAt(x, z) < 2) {
            world.setBlock(new Vector3i(x, 60, z), BlockState.builder().blockType(BlockTypes.GLASS).build());
        }

        return new Location<World>(world, x, world.getHighestYAt(x, z) + 2, z);
    }

    public static String getCoordForIslandNumber(int pn) {
        // (di, dj) is a vector - direction in which we move right now
        int di = 1;
        int dj = 0;
        // length of current segment
        int segment_length = 1;

        // current position (i, j) and how much of current segment we passed
        int i = 0;
        int j = 0;
        int segment_passed = 0;
        if (pn == 0){
            return ("(" + i + "," + j + ")");
        }
        for (int k = 0; k < pn; ++k) {
            // make a step, add 'direction' vector (di, dj) to current position (i, j)
            i += di;
            j += dj;
            ++segment_passed;

            if (segment_passed == segment_length) {
                // done with current segment
                segment_passed = 0;

                // 'rotate' directions
                int buffer = di;
                di = -dj;
                dj = buffer;

                // increase segment length if necessary
                if (dj == 0) {
                    ++segment_length;
                }
            }
        }
        return "(" + i + "," + j + ")";
    }
    public static String getIslandID(int x, int z) {
        return "(" + getIslandIDCoord(x) + "," + getIslandIDCoord(z) + ")";
    }

    public static int getIslandIDCoord(int i) {
        int center = -(DIST/2);
        int ti = (i - center) / (DIST+SIZE);
        if(i < 0) {ti--;}
        return ti;
    }

    public static Vector3i getIslandSmallCorner(String id) {
        String[] list = id.replace("(", "").replace(")", "").split(",");
        Integer x = Integer.valueOf(list[0]);
        Integer z = Integer.valueOf(list[1]);
        return new Vector3i((x * (SIZE+DIST)), 0, (z* (SIZE+DIST)));
    }

    public static Vector3i getIslandBigCorner(String id) {
        String[] list = id.replace("(", "").replace(")", "").split(",");
        Integer x = Integer.valueOf(list[0]);
        Integer z = Integer.valueOf(list[1]);
        return new Vector3i((x * (SIZE+DIST) + SIZE), 0, (z* (SIZE+DIST) + SIZE));
    }

    public static int getIslandXCenterCoord(String id) {
        String[] list = id.replace("(", "").replace(")", "").split(",");
        Integer x = Integer.valueOf(list[0]);
        return  (x * (SIZE+DIST)) + (SIZE / 2);
    }

    public static int getIslandZCenterCoord(String id) {
        String[] list = id.replace("(", "").replace(")", "").split(",");
        Integer z = Integer.valueOf(list[1]);
        return  (z * (SIZE+DIST)) + (SIZE / 2);
    }

    public static String getOwnerOf(int idx, int idz) {
        return IslandDataHandler.getIslandData().getIslandOwner(getIslandID(idx, idz));
    }

    public static IslandInfo getIslandInfoOf(int idx, int idz) {
        if (getOwnerOf(idx, idz) == null) {
            return null;
        }
        ArrayList<IslandInfo> islands = PlayerDataHandler.getPlayerdata(getOwnerOf(idx, idz)).getIslands();
        if (islands.size() > 0) {
            for (IslandInfo island : islands) {
                if (island.getId().equals(getIslandID(idx, idz))) {
                    return island;
                }
            }
        }
        return null;
    }

    public static boolean isBiomeOption(String biomeName) {
        biomeName = biomeName.toUpperCase();
        if (biomeName.equals("FOREST")) {
            return true;
        } else {
            if (!getBiomeTypeFrom(biomeName).equals(BiomeTypes.FOREST)) {
                return true;
            }
        }
        return false;
    }
    
    public static BiomeType getBiomeTypeFrom(String biome) {
        switch (biome) {
            case "BIRCH_FOREST":
                return BiomeTypes.BIRCH_FOREST;
            case "COLD_TAIGA":
                return BiomeTypes.COLD_TAIGA;
            case "DESERT":
                return BiomeTypes.DESERT;
            case "EXTREME_HILLS":
                return BiomeTypes.EXTREME_HILLS;
            case "FOREST":
                return BiomeTypes.FOREST;
            case "JUNGLE":
                return BiomeTypes.JUNGLE;
            case "MEGA_TAIGA":
                return BiomeTypes.MEGA_TAIGA;
            case "MUSHROOM_ISLAND":
                return BiomeTypes.MUSHROOM_ISLAND;
            case "PLAINS":
                return BiomeTypes.PLAINS;
            case "ROOFED_FOREST":
                return BiomeTypes.ROOFED_FOREST;
            case "SAVANNA":
                return BiomeTypes.SAVANNA;
            case "SWAMPLAND":
                return BiomeTypes.SWAMPLAND;
            case "TAIGA":
                return BiomeTypes.TAIGA;
            case "FLOWER_FOREST":
                return BiomeTypes.FLOWER_FOREST;
            case "ICE_PLAINS_SPIKES":
                return BiomeTypes.ICE_PLAINS_SPIKES;
            case "MEGA_SPRUCE_TAIGA":
                return BiomeTypes.MEGA_SPRUCE_TAIGA;
            case "VOID":
                return BiomeTypes.VOID;
            default:
                return BiomeTypes.FOREST;
        }
    }
}
