package neusfear.theforgecore.modules.islands.managers;

import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.islands.IslandInfo;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.property.item.FoodRestorationProperty;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class IslandEventManager {

    @Listener
    public void onDamage(DamageEntityEvent event, @First Player player) {
        if (event.getTargetEntity() instanceof Player) {
            if (IslandWorldManager.getIslandInfoOf(event.getTargetEntity().getLocation().getBlockX(), event.getTargetEntity().getLocation().getBlockZ()) == null) {
                event.setCancelled(true);
                teleportToSpawn(player);
            }
            if (testDamageFlag(event.getTargetEntity().getLocation().getBlockX(),
                    event.getTargetEntity().getLocation().getBlockZ())) {
                if (event.getTargetEntity().getLocation().getY() < 0) {
                    teleportToSpawn(player);
                }
                event.setCancelled(true);
            }
        }
    }

    public static void teleportToSpawn(@First Player player) {
        World world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);
        Warp spawn = ServerDataHandler.getServerData().getSpawn();
        player.setLocationAndRotationSafely(world.getLocation(spawn), spawn.getRotation());
    }

    public static void onHungerLoss(Event event) {
        if (event.getSource() instanceof Player) {
            Player player = (Player) event.getSource();
            if (testDamageFlag(player.getLocation().getBlockX(), player.getLocation().getBlockZ())) {
                player.foodLevel().set(player.foodLevel().get() + 1);
            }
        }
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break event, @First Player player) {
        if (!testPermsOf(player)) {
            event.setCancelled(true);
            player.sendMessage(MsgHelper.error("You don't have permission to break that here."));
        }
    }

    @Listener(order = Order.LAST)
    public void onBlockPlace(ChangeBlockEvent.Place event, @First Player player) {
        if (!testPermsOf(player)) {
            event.setCancelled(true);
            player.sendMessage(MsgHelper.error("You don't have permission to place that here."));
        }
    }

    @Listener(beforeModifications = true, order = Order.FIRST)
    public void onBlockInteract(InteractBlockEvent.Secondary event, @First Player player) {
        if (!testPermsOf(player)) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onItemInteract(InteractItemEvent.Secondary event, @First Player player) {
        if (!testPermsOf(player)) {
            if (!event.getItemStack().getProperty(FoodRestorationProperty.class).isPresent()) {
                event.setCancelled(true);
            }
        }
    }

    @Listener
    public void onEntityInteract(InteractEntityEvent.Primary event, @First Player player) {
        if (event.getTargetEntity() instanceof Player) {
            if (!testPvpFlag((int) player.getLocation().getX(), (int) player.getLocation().getZ())) {
                player.sendMessage(MsgHelper.error("PVP is not allowed here."));
                event.setCancelled(true);
            }
        }
        if (!testPermsOf(player)) {
            event.setCancelled(true);
        }
    }

    private List<EntityType> deniedTypes = Arrays.asList(EntityTypes.BLAZE, EntityTypes.CAVE_SPIDER, EntityTypes.CREEPER,
            EntityTypes.ELDER_GUARDIAN, EntityTypes.ENDER_DRAGON, EntityTypes.ENDERMAN, EntityTypes.ENDERMITE,
            EntityTypes.EVOCATION_ILLAGER, EntityTypes.GHAST, EntityTypes.GIANT, EntityTypes.GUARDIAN, EntityTypes.HUSK,
            EntityTypes.SHULKER, EntityTypes.SILVERFISH, EntityTypes.SKELETON, EntityTypes.SKELETON_HORSE, EntityTypes.SPIDER,
            EntityTypes.SLIME, EntityTypes.VEX, EntityTypes.VINDICATION_ILLAGER, EntityTypes.WITHER, EntityTypes.WITHER_SKELETON,
            EntityTypes.WITCH, EntityTypes.ZOMBIE, EntityTypes.ZOMBIE_HORSE, EntityTypes.ZOMBIE_VILLAGER);

    private boolean isMob(Entity entity) {
        for (EntityType type : deniedTypes) {
            if (type.equals(entity.getType())) {
                return true;
            }
        }
        return false;
    }

    @Listener
    public void onMobSpawning(SpawnEntityEvent event) {
        for (Iterator<Entity> iterator = event.getEntities().iterator(); iterator.hasNext(); ) {
            Entity entity = iterator.next();
            if (!isMob(entity)) {
                continue;
            }
            if (!testMobFlag(entity.getLocation().getBlockX(), entity.getLocation().getBlockZ())) {
                continue;
            }
            iterator.remove();
        }
    }

    private boolean testMobFlag(int x, int z) {
        return IslandFlagManager.getFlagOfTypeFrom(IslandWorldManager.getIslandInfoOf(x, z), "denymobs").getValue().equals("true");
    }

    private boolean testPvpFlag(int x, int z) {
        return IslandFlagManager.getFlagOfTypeFrom(IslandWorldManager.getIslandInfoOf(x, z), "pvp").getValue().equals("true");
    }

    private static boolean testDamageFlag(int x, int z) {
        return IslandFlagManager.getFlagOfTypeFrom(IslandWorldManager.getIslandInfoOf(x, z), "god").getValue().equals("true");
    }

    /*
    @Listener
    public void itemPickupEvent(ChangeInventoryEvent.Pickup event, @First Player player) {
        if (!testPermsOf(player)) {
            event.setCancelled(true);
            player.sendMessage(MsgHelper.error("You can't pickup items here."));
        }
    }

    @Listener
    public void itemDropEvent(ClickInventoryEvent.Drop event, @First Player player) {
        if (!testPermsOf(player)) {
            event.setCancelled(true);
            player.sendMessage(MsgHelper.error("You can't drop items here."));
        }
    }
    */

    private boolean testPermsOf(Player player) {
        if (!player.getWorld().getName().equals("world")) return true;

        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

        int x = (int) player.getLocation().getX();
        int z = (int) player.getLocation().getZ();

        //If there is no owner no one should be able to build, it's just void anyways.
        if (!PlayerDataHandler.hasJoined(IslandWorldManager.getOwnerOf(x, z))) {
            return false;
        }

        if (playerData != null) {
            for (IslandInfo islandInfo : playerData.getIslands()) {
                if (playerData.getUuid().equals(IslandWorldManager.getOwnerOf(x, z))) {
                    return true;
                }
            }
        }

        IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

        if (ownerIslandInfo != null && ownerIslandInfo.getMembers().contains(playerData.getUuid())) {
            return true;
        }
        if (ownerIslandInfo != null && ownerIslandInfo.getCoOwners().contains(playerData.getUuid())) {
            return true;
        }
        if (playerData.getPermissionLevel() > 8) {
            return true;
        }

        return false;
    }
}
