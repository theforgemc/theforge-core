package neusfear.theforgecore.modules.islands.managers;

import neusfear.theforgecore.modules.islands.IslandFlag;
import neusfear.theforgecore.modules.islands.IslandInfo;

import java.util.ArrayList;

public class IslandFlagManager {

    private static ArrayList<IslandFlag> defaultFlags;

    public IslandFlagManager() {
        defaultFlags = new ArrayList<>();
        defaultFlags.add(new IslandFlag("pvp", "Toggles the pvp status of the island", "false"));
        defaultFlags.add(new IslandFlag("god", "Toggles god on the island", "false"));
        defaultFlags.add(new IslandFlag( "denymobs", "Toggles the denial of mobs on an island", "false"));
    }

    public static IslandFlag getDefaultFlagOfType(String type) {
        for (IslandFlag flag : defaultFlags) {
            if (flag.getName().equals(type)) {
                return flag;
            }
        }
        return new IslandFlag("emptyflag", "dummy", "empty");
    }

    public static IslandFlag getFlagOfTypeFrom(IslandInfo islandInfo, String type) {
        if (islandInfo != null) {
            if (islandInfo.getFlags().size() > 0) {
                for (IslandFlag flag : islandInfo.getFlags()) {
                    if (flag.getName().equals(type)) {
                        return flag;
                    }
                }
            }
        }
        return new IslandFlag(type, getDefaultFlagOfType(type).getDescription(), getDefaultFlagOfType(type).getValue());
    }

    public static void replaceFlagFor(IslandInfo islandInfo, IslandFlag newFlag) {
        ArrayList<IslandFlag> islandFlags = islandInfo.getFlags();
        for (IslandFlag flag : islandFlags) {
            if (flag.getName().equals(newFlag.getName())) {
                islandFlags.remove(getFlagOfTypeFrom(islandInfo, newFlag.getName()));
                islandFlags.add(newFlag);
                islandInfo.setFlags(islandFlags);
                return;
            }
        }
        islandFlags.add(newFlag);
        islandInfo.setFlags(islandFlags);
    }

    public static String getStringOf(ArrayList<IslandFlag> flags) {
        String value = "";
        for (int i = 0; i < flags.size(); i++) {
            value = value + flags.get(i).getName() + ": " + flags.get(i).getValue() + (i==flags.size() ? "" : "\n");
        }
        return value;
    }

    public static ArrayList<IslandFlag> getIslandFlags() {
        return defaultFlags;
    }
}
