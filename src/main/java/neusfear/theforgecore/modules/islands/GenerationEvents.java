package neusfear.theforgecore.modules.islands;

import neusfear.theforgecore.helpers.LoggerHelper;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.world.chunk.ForcedChunkEvent;
import org.spongepowered.api.event.world.chunk.UnforcedChunkEvent;

public class GenerationEvents {

    /*
    @Listener
    public void onGenerate(TargetChunkEvent event) {
        World world = event.getTargetChunk().getWorld();
        World defaultWorld = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);
        if (world.equals(defaultWorld)) {
            LoggerHelper.getLogger().info(event.getTargetChunk().getPosition().toString());
        }
    }
     */

    @Listener
    public void onForceLoad(ForcedChunkEvent event) {
        LoggerHelper.getLogger().info("Force load.");
    }

    @Listener
    public void onPlayerGenerate(UnforcedChunkEvent event) {
        LoggerHelper.getLogger().info("Player load.");
    }
}
