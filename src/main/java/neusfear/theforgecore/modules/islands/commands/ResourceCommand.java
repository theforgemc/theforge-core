package neusfear.theforgecore.modules.islands.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.islands.ResourceWorldHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class ResourceCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Teleport to the resource world."))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Optional<World> resourceWorld = Sponge.getGame().getServer().getWorld(ResourceWorldHandler.worldName);
                        if (resourceWorld.isPresent()) {
                            Player player = (Player) src;
                            Location<World> location = new Location<World>(resourceWorld.get(), player.getPosition());
                            resourceWorld.get().loadChunk(location.getChunkPosition().getX(), 0, location.getChunkPosition().getZ(), true);
                            player.setLocation(location);
                            src.sendMessage(MsgHelper.info("Teleported to your relative resource world location."));
                        } else {
                            src.sendMessage(MsgHelper.error("There is no resource world loaded."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only command."));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "resource");
    }
}
