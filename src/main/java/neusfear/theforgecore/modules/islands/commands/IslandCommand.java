package neusfear.theforgecore.modules.islands.commands;

import com.flowpowered.math.vector.Vector3i;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.islands.IslandFlag;
import neusfear.theforgecore.modules.islands.IslandInfo;
import neusfear.theforgecore.modules.islands.managers.IslandFlagManager;
import neusfear.theforgecore.modules.islands.managers.IslandWorldManager;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.IslandDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.ChunkRegenerateFlags;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class IslandCommand {

    public static void registerCommand() {

        CommandSpec help = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    src.sendMessage(MsgHelper.info("List of island subcommands: &fnew, biome, home, info, list, trust, untrust, promote, demote, setname, setdesc, flag, flags"));
                    return CommandResult.success();}
                ).build();

        CommandSpec newCommand = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.string(Text.of("biome"))))
                .executor((CommandSource src, CommandContext args) -> {
                    Player player = (Player) src;
                    PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

                    //Players get 1 island
                    //Moderators get 5 islands
                    //Admins get infinite
                    if (playerData.getIslands().size() > 0) {
                        if (playerData.getPermissionLevel() < 9) {
                            src.sendMessage(MsgHelper.error("You can only have one island."));
                            return CommandResult.success();
                        } else {
                            if (playerData.getPermissionLevel() == 9 && playerData.getIslands().size() >= 5) {
                                src.sendMessage(MsgHelper.error("Moderators can only have 5 islands"));
                                return CommandResult.success();
                            }
                        }
                    }

                    //Make sure we can create the island the player wants
                    Optional<String> biome = args.<String>getOne("biome");
                    if (args.<String>getOne("biome").isPresent()) {
                        if (!IslandWorldManager.isBiomeOption(biome.get())) {
                            src.sendMessage(MsgHelper.error("That is an invalid Biome type"));
                            return CommandResult.success();
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("You need to specify a biome. View the list with: \n&f/island biomes"));
                        return CommandResult.success();
                    }

                    //Create the new island
                    World world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);
                    IslandInfo islandInfo = playerData.addIsland(biome.get().toUpperCase());

                    //Generate the island
                    Vector3i smallChunkLoc = IslandWorldManager.getIslandSmallCorner(islandInfo.getId());
                    Vector3i bigChunkLoc = IslandWorldManager.getIslandBigCorner(islandInfo.getId());
                    Location<World> sLoc = new Location<World>(world, smallChunkLoc.getX(), 0, smallChunkLoc.getZ());
                    Location<World> bLoc = new Location<World>(world, bigChunkLoc.getX(), 0, bigChunkLoc.getZ());
                    Vector3i smallChunk = sLoc.getChunkPosition();
                    Vector3i bigChunk = bLoc.getChunkPosition();

                    src.sendMessage(MsgHelper.info("Generating island &f" + islandInfo.getName() + " &7 with biome &f" + biome.get()));
                    int[] i = {0};
                    for (int[] x = {smallChunk.getX()}; x[0] <= bigChunk.getX(); x[0]++) {
                        Task.builder().execute(() -> generateRow(smallChunk, bigChunk, world, x[0])).name(islandInfo.getName() + " generation").delay(i[0], TimeUnit.SECONDS).submit(TheForgeCore.getInstance());
                        i[0]++;
                    }
                    src.sendMessage(MsgHelper.info("Done. get to it with &f/is home " + islandInfo.getName()));
                    return CommandResult.success();
                }).build();

        CommandSpec biomes = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    src.sendMessage(MsgHelper.info("Biome types: &fBIRCH_FOREST, COLD_TAIGA, DESERT, EXTREME_HILLS, " +
                            "FOREST, JUNGLE, MEGA_TAIGA, MUSHROOM_ISLAND, PLAINS, ROOFED_FOREST, SAVANNA, SWAMPLAND, " +
                            "TAIGA, FLOWER_FOREST, ICE_PLAINS_SPIKES, MEGA_SPRUCE_TAIGA"));
                    return CommandResult.success();
                }).build();

        CommandSpec info = CommandSpec.builder()
                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("islandname")))))
                .executor((CommandSource src, CommandContext args) -> {
                    Player player = (Player) src;
                    int x = (int) player.getLocation().getX();
                    int z = (int) player.getLocation().getZ();
                    PlayerData ownerData = PlayerDataHandler.getPlayerdata(IslandWorldManager.getOwnerOf(x, z));
                    if (ownerData.getUuid() == null) {
                        src.sendMessage(MsgHelper.error("No island has been generated here yet."));
                        return CommandResult.success();
                    } else {
                        PlayerData playerData = new PlayerData();

                        //Check if they're looking for someone else's playerdata
                        IslandInfo islandInfo = null;
                        if (args.getOne("player").isPresent()) {
                            String argUser = args.<String>getOne("player").get();
                            if (PlayerDataHandler.hasUserJoined(argUser)) {
                                playerData = PlayerDataHandler.getPlayerdataFromUsername(argUser);

                                //Check to see if they want to see a specific island of a player
                                if (args.getOne("islandname").isPresent()) {
                                    String islandName = args.<String>getOne("islandname").get();
                                    for (IslandInfo tIs : playerData.getIslands()) {
                                        if (tIs.getName().equalsIgnoreCase(islandName)) {
                                            islandInfo = tIs;
                                            break;
                                        }
                                    }
                                    if (islandInfo.getName() == null) {
                                        src.sendMessage(MsgHelper.error(playerData.getUsername() + " does not have an island with that name."));
                                        return CommandResult.success();
                                    }
                                } else {
                                    if (playerData.getIslands().size() > 0) {
                                        islandInfo = playerData.getIslands().get(0);
                                    } else {
                                        src.sendMessage(MsgHelper.error("That user has no islands."));
                                        return CommandResult.success();
                                    }
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("That player has never joined before."));
                            }
                        } else {
                            playerData = ownerData;
                            islandInfo = IslandWorldManager.getIslandInfoOf(x, z);
                        }

                        src.sendMessage(MsgHelper.interactablePiece("&fShowing &6Island Info&f for island: &7" + islandInfo.getName(),
                                "&6" + islandInfo.getIndex() + " &7 : &f" + islandInfo.getId()));
                        src.sendMessage(MsgHelper.formatted(" &fOwned by: &7" + playerData.getUsername()));
                        src.sendMessage(MsgHelper.formatted(" &7" + islandInfo.getDescription()));
                        src.sendMessage(MsgHelper.formatted(" &fXP: &7" + islandInfo.getExperience()));

                        String members = "";
                        String coowners = "";
                        if (islandInfo.getMembers().size() > 0) {
                            for (String member : islandInfo.getMembers()) {
                                members = members + PlayerDataHandler.getPlayerdata(member).getUsername() + ", ";
                            }
                            members = members.substring(0, (members.length() - 2)) + ".";
                        } else {
                            members = "none.";
                        }
                        if (islandInfo.getCoOwners().size() > 0) {
                            for (String coowner : islandInfo.getCoOwners()) {
                                coowners = coowners + PlayerDataHandler.getPlayerdata(coowner).getUsername() + ", ";
                            }
                            coowners = coowners.substring(0, (coowners.length() - 2)) + ".";
                        } else {
                            coowners = "none.";
                        }

                        src.sendMessage(MsgHelper.interactablePiece("&fCo-Owners: &7" + islandInfo.getCoOwners().size(), coowners));
                        src.sendMessage(MsgHelper.interactablePiece("&fMembers: &7" + islandInfo.getMembers().size(), members));
                        return CommandResult.success();
                    }
                }).build();

        CommandSpec homeList = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    Player player = (Player) src;
                    if (args.getOne("player").isPresent()) {
                        String arg1 = String.valueOf(args.<String>getOne("player").get());
                        if (PlayerDataHandler.hasUserJoined(arg1)) {
                            if (PlayerDataHandler.getPlayerdataFromUsername(arg1).getIslands().size() > 0) {
                                String homes = "";
                                for (IslandInfo island : PlayerDataHandler.getPlayerdataFromUsername(arg1).getIslands()) {
                                    homes = homes + ", " + island.getName();
                                }
                                src.sendMessage(MsgHelper.info("&7Islands of &f" + arg1 + " &7are: &f" + homes.substring(2)));
                            } else {
                                src.sendMessage(MsgHelper.error(arg1 + " has no islands"));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined."));
                        }
                    } else {
                        String homes = "";
                        for (IslandInfo island : PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getIslands()) {
                            homes = homes + ", " + island.getName();
                        }
                        src.sendMessage(MsgHelper.info("&7Islands: &f" + homes.substring(2)));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec home = CommandSpec.builder()
                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("playerOrName")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("islandname"))))                )
                .executor((CommandSource src, CommandContext args) -> {
                    Player player = (Player) src;
                    if (args.<String>getOne("playerOrName").isPresent()) {
                        String arg1 = args.<String>getOne("playerOrName").get();
                        if (args.getOne("islandname").isPresent()) {
                            if (PlayerDataHandler.hasUserJoined(arg1)) {
                                PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(arg1);
                                for (IslandInfo islandInfo : playerData.getIslands()) {
                                    if (islandInfo.getName().equalsIgnoreCase(args.<String>getOne("islandname").get())) {
                                        if (islandInfo.getMembers().contains(player.getUniqueId().toString()) ||
                                                islandInfo.getCoOwners().contains(player.getUniqueId().toString()) ||
                                                PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getPermissionLevel() >= 8) {

                                            Location<World> location = IslandWorldManager.getIslandSafeStart(islandInfo.getId());
                                            player.setLocationSafely(location);
                                            return CommandResult.success();
                                        } else {
                                            src.sendMessage(MsgHelper.error("You aren't trusted on that player's island."));
                                            return CommandResult.success();
                                        }
                                    }
                                }
                                src.sendMessage(MsgHelper.error("That player doesn't have and island with that name."));
                            } else {
                                src.sendMessage(MsgHelper.error("That player has never joined before."));
                            }
                            return CommandResult.success();
                        }
                        for (IslandInfo islandInfo : PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getIslands()) {
                            if (islandInfo.getName().equalsIgnoreCase(arg1)) {
                                Location<World> location = IslandWorldManager.getIslandSafeStart(islandInfo.getId());
                                player.setLocation(location);
                                return CommandResult.success();
                            }
                        }
                        src.sendMessage(MsgHelper.error("You don't own an island with that name."));
                    } else {
                        if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getIslands().size() > 0) {
                            String id = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getIslands().get(0).getId();
                            Location<World> location = IslandWorldManager.getIslandSafeStart(id);

                            player.setLocation(location);
                            src.sendMessage(MsgHelper.info("Teleporting to your first island."));
                        } else {
                            src.sendMessage(MsgHelper.error("You don't own any islands"));
                        }
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec trust = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.player(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (args.getOne("player").isPresent()) {
                        String argUser = args.<Player>getOne("player").get().getName();
                        if (PlayerDataHandler.hasUserJoined(argUser)) {
                            Player player = (Player) src;
                            int x = (int) player.getLocation().getX();
                            int z = (int) player.getLocation().getZ();

                            PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

                            PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                            IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                            if (ownerIslandInfo.getMembers().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid()) ||
                                    ownerIslandInfo.getCoOwners().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid())) {
                                src.sendMessage(MsgHelper.error(argUser + " is already added to this island."));
                                return CommandResult.success();
                            }

                            if (playerData.getUuid().equalsIgnoreCase(ownerPlayerData.getUuid()) || ownerIslandInfo.getCoOwners().contains(player.getUniqueId().toString())) {
                                ownerIslandInfo.addMember(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid());
                                src.sendMessage(MsgHelper.info(argUser + " added to your island."));
                                return CommandResult.success();
                            }
                            src.sendMessage(MsgHelper.error("You don't have permission to trust people on this island."));
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify a player to trust"));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec untrust = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.player(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (args.getOne("player").isPresent()) {
                        String argUser = args.<Player>getOne("player").get().getName();
                        Player player = (Player) src;
                        int x = (int) player.getLocation().getX();
                        int z = (int) player.getLocation().getZ();

                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

                        PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                        IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                        if (PlayerDataHandler.hasUserJoined(argUser)) {
                            if (ownerIslandInfo.getMembers().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid()) ||
                                    ownerIslandInfo.getCoOwners().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid())) {

                                if (playerData.getUuid().equalsIgnoreCase(ownerPlayerData.getUuid())) {
                                    ownerIslandInfo.removeMember(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid());
                                    src.sendMessage(MsgHelper.info(argUser + " removed from your island."));
                                    return CommandResult.success();
                                }
                                if (ownerIslandInfo.getCoOwners().contains(player.getUniqueId().toString())) {
                                    if (!ownerIslandInfo.getCoOwners().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid())) {
                                        ownerIslandInfo.removeMember(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid());
                                        src.sendMessage(MsgHelper.info(argUser + " removed from your island."));
                                    } else {
                                        src.sendMessage(MsgHelper.error("You can't untrust people the same island rank as you."));
                                    }
                                } else {
                                    src.sendMessage(MsgHelper.error("You don't have permission to untrust people on this island."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("That player is not added to your island."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify a player to untrust"));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec promote = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.player(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (args.getOne("player").isPresent()) {
                        String argUser = args.<Player>getOne("player").get().getName();
                        Player player = (Player) src;
                        int x = (int) player.getLocation().getX();
                        int z = (int) player.getLocation().getZ();

                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

                        PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                        IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                        if (PlayerDataHandler.hasUserJoined(argUser)) {
                            if (argUser.equalsIgnoreCase(player.getName())) {
                                src.sendMessage(MsgHelper.error("You can't do that to yourself."));
                                return CommandResult.success();
                            }
                            if (ownerIslandInfo.getMembers().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid()) ||
                                    ownerIslandInfo.getCoOwners().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid())) {
                                if (ownerIslandInfo.getCoOwners().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid())) {
                                    src.sendMessage(MsgHelper.error(argUser + " is already a Co-Owner on your island."));
                                    return CommandResult.success();
                                }

                                if (playerData.getUuid().equalsIgnoreCase(ownerPlayerData.getUuid())) {

                                    ownerIslandInfo.removeMember(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid());
                                    ownerIslandInfo.addCoOwner(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid());
                                    src.sendMessage(MsgHelper.info(argUser + " promoted to Co-Owner."));
                                } else {
                                    src.sendMessage(MsgHelper.error("You don't have permission to promote people on this island."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("That player is not added to your island."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before."));
                        }
                        return CommandResult.success();
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify a player to promote"));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec demote = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.player(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (args.getOne("player").isPresent()) {
                        String argUser = args.<Player>getOne("player").get().getName();
                        Player player = (Player) src;
                        int x = (int) player.getLocation().getX();
                        int z = (int) player.getLocation().getZ();

                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

                        PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                        IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                        if (PlayerDataHandler.hasUserJoined(argUser)) {
                            if (argUser.equalsIgnoreCase(player.getName())) {
                                src.sendMessage(MsgHelper.error("You can't do that to yourself."));
                                return CommandResult.success();
                            }
                            if (ownerIslandInfo.getMembers().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid()) ||
                                    ownerIslandInfo.getCoOwners().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid())) {

                                if (!ownerIslandInfo.getCoOwners().contains(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid())) {
                                    src.sendMessage(MsgHelper.error(argUser + " is not a Co-Owner on your island."));
                                    return CommandResult.success();
                                }

                                if (playerData.getUuid().equalsIgnoreCase(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)))) {

                                    ownerIslandInfo.removeCoOwner(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid());
                                    ownerIslandInfo.addMember(PlayerDataHandler.getPlayerdataFromUsername(argUser).getUuid());
                                    src.sendMessage(MsgHelper.info(argUser + " demoted to Member."));
                                } else {
                                    src.sendMessage(MsgHelper.error("You don't have permission to demote people on this island."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("That player is not added to your island."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify a player to demote"));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec setname = CommandSpec.builder()
                .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("name"))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (args.getOne("name").isPresent()) {
                        Player player = (Player) src;
                        String name = args.<String>getOne("name").get();
                        int x = (int) player.getLocation().getX();
                        int z = (int) player.getLocation().getZ();

                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());

                        PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                        IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                        if (playerData.getUuid().equals(ownerPlayerData.getUuid())) {
                            for (IslandInfo islandInfo : ownerPlayerData.getIslands()) {
                                if (islandInfo.getName().equalsIgnoreCase(name)) {
                                    src.sendMessage(MsgHelper.error("You already have an island with the name."));
                                    return CommandResult.success();
                                }
                            }
                            if (name.matches("^[a-zA-Z0-9]*$")) {
                                ownerIslandInfo.setName(name);
                            } else {
                                src.sendMessage(MsgHelper.error("Island names can only be Alphanumeric."));
                                return CommandResult.success();
                            }
                            src.sendMessage(MsgHelper.info("Island name updated to: &f" + name));
                        } else {
                            src.sendMessage(MsgHelper.error("Only the owner of the island can change it's name."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify the new island name."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec setdesc = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.remainingJoinedStrings(Text.of("description"))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (args.getOne("description").isPresent()) {
                        Player player = (Player) src;
                        String description = args.<String>getOne("description").get();
                        int x = (int) player.getLocation().getX();
                        int z = (int) player.getLocation().getZ();

                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                        IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                        if (playerData.getUuid().equals(ownerPlayerData.getUuid())) {
                            ownerIslandInfo.setDescription(description);
                            src.sendMessage(MsgHelper.info("Island description updated to: &f" + description));
                        } else {
                            src.sendMessage(MsgHelper.error("Only the owner of the island can change it's description."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify the new island description."));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandSpec pvpflag = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.bool(Text.of("value"))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!args.getOne("value").isPresent()) {
                        src.sendMessage(MsgHelper.error("Please specify a value"));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    int x = (int) player.getLocation().getX();
                    int z = (int) player.getLocation().getZ();

                    PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                    PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                    IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                    if (ownerIslandInfo.getCoOwners().contains(playerData.getUuid()) || ownerPlayerData.getUuid().equals(playerData.getUuid())) {
                        String value = args.<Boolean>getOne("value").get().toString();
                        IslandFlagManager.replaceFlagFor(ownerIslandInfo, IslandFlagManager.getDefaultFlagOfType("pvp").setValue(value));
                        src.sendMessage(MsgHelper.info("pvp flag updated to: " + value));
                        return CommandResult.success();
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec mobFlag = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.bool(Text.of("value"))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!args.getOne("value").isPresent()) {
                        src.sendMessage(MsgHelper.error("Please specify a value"));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    int x = (int) player.getLocation().getX();
                    int z = (int) player.getLocation().getZ();

                    PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                    PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                    IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                    if (ownerIslandInfo.getCoOwners().contains(playerData.getUuid()) || ownerPlayerData.getUuid().equals(playerData.getUuid())) {
                        String value = args.<Boolean>getOne("value").get().toString();
                        IslandFlagManager.replaceFlagFor(ownerIslandInfo, IslandFlagManager.getDefaultFlagOfType("denymobs").setValue(value));
                        src.sendMessage(MsgHelper.info("mob flag updated to: " + value));
                        return CommandResult.success();
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec godflag = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.bool(Text.of("value"))))
                .executor((CommandSource src, CommandContext args) -> {
                    Player player = (Player) src;
                    if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getPermissionLevel() != 10) {
                        src.sendMessage(MsgHelper.error("That is an admin flag"));
                        return CommandResult.success();
                    }
                    if (!args.getOne("value").isPresent()) {
                        src.sendMessage(MsgHelper.error("Please specify a value"));
                        return CommandResult.success();
                    }
                    int x = (int) player.getLocation().getX();
                    int z = (int) player.getLocation().getZ();

                    PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                    PlayerData ownerPlayerData = PlayerDataHandler.getPlayerdata(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)));
                    IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);

                    if (ownerIslandInfo.getCoOwners().contains(playerData.getUuid()) || ownerPlayerData.getUuid().equals(playerData.getUuid())) {
                        String value = args.<Boolean>getOne("value").get().toString();
                        IslandFlagManager.replaceFlagFor(ownerIslandInfo, IslandFlagManager.getDefaultFlagOfType("god").setValue(value));
                        src.sendMessage(MsgHelper.info("god flag updated to: " + value));
                        return CommandResult.success();
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec flag = CommandSpec.builder()
                .child(godflag, "god")
                .child(pvpflag, "pvp")
                .child(mobFlag, "denymobs").build();

        CommandSpec flags = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    Player player = (Player) src;
                    int x = (int) player.getLocation().getX();
                    int z = (int) player.getLocation().getZ();
                    IslandInfo ownerIslandInfo = IslandWorldManager.getIslandInfoOf(x, z);
                    if (PlayerDataHandler.hasJoined(IslandDataHandler.getIslandData().getIslandOwner(IslandWorldManager.getIslandID(x, z)))) {
                        ArrayList<IslandFlag> flagz = ownerIslandInfo.getFlags();
                        src.sendMessage(MsgHelper.info(IslandFlagManager.getStringOf(flagz)));
                        return CommandResult.success();
                    }
                    src.sendMessage(MsgHelper.error("This island has no defined flag information."));
                    return CommandResult.success();
                }).build();

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Everything to do with islands."))
                .child(newCommand, "new", "create")
                .child(home, "home", "teleport")
                .child(homeList, "homes", "list")
                .child(biomes, "biomes")
                .child(info, "info")
                .child(setname, "setname", "rename")
                .child(setdesc, "setdescription", "setdesc")
                .child(trust, "trust")
                .child(promote, "promote")
                .child(demote, "demote")
                .child(untrust, "untrust")
                .child(flag, "flag")
                .child(flags, "flags")
                .child(help, "help")
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "island", "is");
    }

    public static void generateRow(Vector3i smallChunk, Vector3i bigChunk, World world, int x) {
        for (int z = smallChunk.getZ(); z <= bigChunk.getZ(); z++) {
            world.loadChunk(x, 0, z, false);
            world.regenerateChunk(x, 0, z, ChunkRegenerateFlags.CREATE);
        }
    }
}
