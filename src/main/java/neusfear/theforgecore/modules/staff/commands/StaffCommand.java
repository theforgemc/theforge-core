package neusfear.theforgecore.modules.staff.commands;

import com.flowpowered.math.vector.Vector3d;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.handlers.BackupHandler;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import neusfear.theforgecore.modules.homes.Home;
import neusfear.theforgecore.modules.infractions.InfractionTypes;
import neusfear.theforgecore.modules.infractions.helpers.InfractionHelper;
import neusfear.theforgecore.modules.islands.managers.IslandWorldManager;
import neusfear.theforgecore.modules.permissions.Permissions;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.warps.AdminWarpHandler;
import neusfear.theforgecore.modules.warps.PlayerWarpHelper;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.effect.sound.PitchModulation;
import org.spongepowered.api.effect.sound.SoundCategories;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;

public class StaffCommand {

    public static void registerCommand() {

        CommandSpec backup = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    LoggerHelper.getLogger().info("Manually creating backup, this will cause lag.");
                    src.sendMessage(MsgHelper.error("Manually creating  backup, this freezes the main thread, so use only in emergencies"));
                    Sponge.getServer().getBroadcastChannel().send(MsgHelper.info("An admin is creating a manual backup, this will cause lag"));
                    BackupHandler.createBackup();
                    src.sendMessage(MsgHelper.info("Done."));
                    return CommandResult.success();
                }).build();

        CommandSpec setwarp = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("warpname")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("warpname").isPresent()) {
                        String warp = args.<String>getOne("warpname").get();
                        if (!AdminWarpHandler.isAdminWarpSet(warp)) {
                            Player player = (Player) src;
                            Location<World> location = player.getLocation();
                            Vector3d rotation = player.getRotation();
                            AdminWarpHandler.createNewAdminWarp(rotation, warp, location.getBlockX(), location.getBlockY(), location.getBlockZ());
                            src.sendMessage(MsgHelper.info("Warp " + warp + " has been set to your current location."));
                        } else {
                            src.sendMessage(MsgHelper.error("That warp already exists, get to it with /warp " + warp));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("You must specify a warp name"));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec delwarp = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("warpname")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("warpname").isPresent()) {
                        String warp = args.<String>getOne("warpname").get();
                        if (AdminWarpHandler.isAdminWarpSet(warp)) {
                            AdminWarpHandler.removeAdminWarp(warp);
                            src.sendMessage(MsgHelper.info("Warp " + warp + " deleted."));
                            return CommandResult.success();
                        }
                        for (Warp warpl : PlayerWarpHelper.getWarps()) {
                            if (warp.equalsIgnoreCase(warpl.getName())) {
                                PlayerDataHandler.getPlayerdata(warpl.getOwner()).removeWarp(warpl);
                                src.sendMessage(MsgHelper.info("Warp " + warp + " deleted."));
                                return CommandResult.success();
                            }
                        }
                        src.sendMessage(MsgHelper.error("The warp " + warp + " doesn't exist"));
                    } else {
                        src.sendMessage(MsgHelper.error("You must specify th warp you want to delete."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec featurewarp = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("warpname")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("warpname").isPresent()) {
                        String warp = args.<String>getOne("warpname").get();
                        ArrayList<Warp> allWarps = AdminWarpHandler.getAdminwarps();
                        allWarps.addAll(PlayerWarpHelper.getWarps());
                        for (Warp cwarp : allWarps) {
                            if (cwarp.getName().equalsIgnoreCase(warp)) {
                                ServerDataHandler.getServerData().addFeaturedWarp(cwarp);
                                src.sendMessage(MsgHelper.info("The warp " + cwarp.getName() + " was featured."));
                                return CommandResult.success();
                            }
                        }
                        src.sendMessage(MsgHelper.error("That warp is not set."));
                    } else {
                        src.sendMessage(MsgHelper.error("You must specify th warp you want to feature."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec unfeaturewarp = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("warpname")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("warpname").isPresent()) {
                        String warp = args.<String>getOne("warpname").get();
                        for (Warp cwarp : ServerDataHandler.getServerData().getFeaturedwarps()) {
                            if (cwarp.getName().equalsIgnoreCase(warp)) {
                                ServerDataHandler.getServerData().removeFeaturedWarp(cwarp);
                                src.sendMessage(MsgHelper.info("The warp " + cwarp.getName() + " was un-featured."));
                                return CommandResult.success();
                            }
                        }
                        src.sendMessage(MsgHelper.error("That warp isn't featured"));
                    } else {
                        src.sendMessage(MsgHelper.error("You must specify th warp you want to remove."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec addinfraction = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("infraction type")))),
                        GenericArguments.optionalWeak(GenericArguments.remainingJoinedStrings(Text.of("reason"))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                            if (args.getOne("player").isPresent()) {
                                if (args.getOne("infraction type").isPresent()) {
                                    if (args.getOne("reason").isPresent()) {
                                        String infPayer = args.<String>getOne("player").get();
                                        String infType = args.<String>getOne("infraction type").get();
                                        String reason = args.<String>getOne("reason").get();
                                        if (PlayerDataHandler.hasUserJoined(infPayer)) {
                                            if (!InfractionTypes.getType(infType).equalsIgnoreCase("invalid")) {
                                                Player player = (Player) src;
                                                PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(infPayer);
                                                InfractionHelper.createInfraction(playerData.getUuid(), infType, player.getUniqueId().toString(), reason);
                                                src.sendMessage(MsgHelper.info(InfractionTypes.getType(infType) + " infraction added to " + playerData.getUsername()));
                                            } else {
                                                src.sendMessage(MsgHelper.error("That is not a valid infraction type"));
                                            }
                                        } else {
                                            src.sendMessage(MsgHelper.error("That player has never joined before."));
                                        }
                                    } else {
                                        src.sendMessage(MsgHelper.error("Please give a reason for the infraction."));
                                    }
                                } else {
                                    src.sendMessage(MsgHelper.error("Please specify an infraction type."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("Please specify the player you want to give the infraction."));
                            }
                            return CommandResult.success();
                        }).build();

        CommandSpec setspawn = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    player.getWorld().getProperties().setSpawnPosition(player.getLocation().getBlockPosition());
                    ServerDataHandler.getServerData().setSpawn(new Warp(player.getRotation(), "spawn",
                            player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()));
                    src.sendMessage(MsgHelper.info("Spawn updated to your current location."));
                    return CommandResult.success();
                }).build();

        CommandSpec setfirstspawn = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    ServerDataHandler.getServerData().setFirstspawn(new Warp(player.getRotation(), "firstspawn",
                            player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()));
                    src.sendMessage(MsgHelper.info("First spawn updated to your current location."));
                    ServerDataHandler.saveServerData();
                    return CommandResult.success();
                }).build();

        CommandSpec setrank = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("rank")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getPermissionLevel() != 10) {
                        src.sendMessage(MsgHelper.error("only admins can update ranks."));
                        return CommandResult.success();
                    }
                    if (args.getOne("player").isPresent()) {
                        if (args.getOne("rank").isPresent()) {
                            PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(args.<String>getOne("player").get());
                            if (PlayerDataHandler.hasJoined(playerData.getUuid())) {
                                String rank = args.<String>getOne("rank").get().toLowerCase();
                                if (Permissions.getPermissionLevelFromRank(rank) >= 0) {
                                    playerData.setPermissionLevel(Permissions.getPermissionLevelFromRank(rank));
                                    src.sendMessage(MsgHelper.info(playerData.getUsername() + "'s Rank updated to: " + Permissions.getRankPrefix(Permissions.getPermissionLevelFromRank(rank))));
                                } else {
                                    src.sendMessage(MsgHelper.error("That player hasn't joined before."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("That player hasn't joined before"));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("Specify the rank you want the player to be set to."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Who's rank do you want to update."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec homes = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    if (args.getOne("player").isPresent()) {
                        PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(args.<String>getOne("player").get());
                        if (playerData.getHomes().size() < 1) {
                            src.sendMessage(MsgHelper.error(playerData.getUsername() + " has no homes."));
                            return CommandResult.success();
                        }
                        src.sendMessage(MsgHelper.info(playerData.getUsername() + "'s Homes:"));
                        for (Home home : playerData.getHomes()) {
                            src.sendMessage(MsgHelper.formatted(home.getName()));
                        }
                        return CommandResult.success();
                    } else {
                        src.sendMessage(MsgHelper.error("Specify the player's name."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec hometp = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("home")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    if (args.getOne("player").isPresent()) {
                        if (args.getOne("home").isPresent()) {
                            PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(args.<String>getOne("player").get());
                            if (PlayerDataHandler.hasJoined(playerData.getUuid())) {
                                String home = args.<String>getOne("home").get().toLowerCase();
                                for (Home homel : playerData.getHomes()) {
                                    if (home.equals(homel.getName())) {
                                        World world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);
                                        player.setLocation(world.getLocation(homel));
                                        src.sendMessage(MsgHelper.info("Teleporting to home: " + homel.getName() + "..."));
                                        return CommandResult.success();
                                    }
                                }
                                src.sendMessage(MsgHelper.error("The player specified has no homes with that name, list their homes with <e>/admin homes <player><e>."));
                                return CommandResult.success();
                            } else {
                                src.sendMessage(MsgHelper.error("That player hasn't joined before"));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("Which home do you want to teleport to."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("What player's home do you want to teleport to."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec delhome = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("home")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    if (args.getOne("player").isPresent()) {
                        if (args.getOne("home").isPresent()) {
                            PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(args.<String>getOne("player").get());
                            if (PlayerDataHandler.hasJoined(playerData.getUuid())) {
                                for (Home home : playerData.getHomes()) {
                                    if (args.<String>getOne("home").get().equals(home.getName())) {
                                        playerData.removeHome(home);
                                        src.sendMessage(MsgHelper.info("Home deleted."));
                                        return CommandResult.success();
                                    }
                                }
                                src.sendMessage(MsgHelper.error("The player specified has no homes with that name, list their homes with <e>/admin homes <player><e>."));
                                return CommandResult.success();
                            } else {
                                src.sendMessage(MsgHelper.error("That player hasn't joined before"));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("Which home do you want to delete."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("What player's home do you want to delete."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec islandtp = CommandSpec.builder()
                .arguments(GenericArguments.onlyOne(GenericArguments.integer(Text.of("index"))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("index").isPresent()) {
                        Player player = (Player) src;
                        player.setLocation(IslandWorldManager.getIslandSafeStart(IslandWorldManager.getCoordForIslandNumber(args.<Integer>getOne("index").get())));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec socialspy = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    if (EssentialDataHander.getEssentialDataHander().getSocialSpies().contains(player)) {
                        EssentialDataHander.getEssentialDataHander().removeSocialSpy(player);
                        src.sendMessage(MsgHelper.info("You are no longer viewing all private messages."));
                    } else {
                        EssentialDataHander.getEssentialDataHander().addSocialSpy(player);
                        src.sendMessage(MsgHelper.info("You are now viewing all private messages."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec commandspy = CommandSpec.builder()
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    if (EssentialDataHander.getEssentialDataHander().getCommandSpies().contains(player)) {
                        EssentialDataHander.getEssentialDataHander().removeCommandSpy(player);
                        src.sendMessage(MsgHelper.info("You are no longer viewing all commands."));
                    } else {
                        EssentialDataHander.getEssentialDataHander().addCommandSpy(player);
                        src.sendMessage(MsgHelper.info("You are now viewing all commands."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec broadcast = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.remainingJoinedStrings(Text.of("message")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (args.getOne("message").isPresent()) {
                        if (!(src instanceof Player)) {
                            src.sendMessage(MsgHelper.error("that is a player only command."));
                            return CommandResult.success();
                        }
                        Sponge.getServer().getBroadcastChannel().send(MsgHelper.info(args.<String>getOne("message").get()));
                        Sponge.getServer().getOnlinePlayers().forEach(player1 -> player1.playSound(SoundTypes.BLOCK_NOTE_PLING, SoundCategories.BLOCK,
                                player1.getLocation().getPosition(), 1, PitchModulation.ASHARP1));
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify the message you want to send to everyone."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec invsee = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("player").isPresent()) {
                        String playerArg = args.<String>getOne("player").get();
                        if (PlayerDataHandler.getUserFromName(playerArg).isPresent()) {
                            Player player = (Player) src;
                            User target = PlayerDataHandler.getUserFromName(playerArg).get();
                            Inventory inventory = target.isOnline() ? target.getPlayer().get().getInventory() : target.getInventory();
                            player.openInventory(inventory);
                            return CommandResult.success();
                        }
                        src.sendMessage(MsgHelper.error("That player doesn't exist."));
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify the player."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec plusone = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("player").isPresent()) {
                        String playerArg = args.<String>getOne("player").get();
                        if (PlayerDataHandler.hasUserJoined(playerArg)) {
                            PlayerDataHandler.getPlayerdataFromUsername(playerArg).addReputation();
                            src.sendMessage(MsgHelper.info("Added 1 reputation to " + playerArg));
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify the player."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec addxp = CommandSpec.builder()
                .arguments(GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.integer(Text.of("experience")))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(MsgHelper.error("that is a player only command."));
                        return CommandResult.success();
                    }
                    if (args.getOne("player").isPresent()) {
                        if (args.getOne("experience").isPresent()) {
                            String playerArg = args.<String>getOne("player").get();
                            int xp = args.<Integer>getOne("experience").get();
                            if (PlayerDataHandler.hasUserJoined(playerArg)) {
                                PlayerDataHandler.getPlayerdataFromUsername(playerArg).addExperience(Integer.valueOf(xp));
                                src.sendMessage(MsgHelper.info(xp + " XP added to " + playerArg));
                            } else {
                                src.sendMessage(MsgHelper.error("That player has never joined before"));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("Please specify tne amount of xp to give"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("Please specify the player."));
                    }
                    return CommandResult.success();
                }).build();

        CommandSpec commandSpec = CommandSpec.builder()
                .child(addinfraction, "addinfraction", "addinf")
                .child(invsee, "invsee")
                .child(socialspy, "socialsply", "ss")
                .child(commandspy, "commandspy", "cs")
                .child(homes, "homes")
                .child(hometp, "home")
                .child(delhome, "delhome", "deletehome")
                .child(setspawn, "setspawn")
                .child(setfirstspawn, "setfirstspawn")
                .child(setwarp, "setwarp")
                .child(delwarp, "delwarp")
                .child(featurewarp, "featurewarp", "fwarp")
                .child(unfeaturewarp, "unfeaturewarp", "ufwarp")
                .child(addxp, "addxp")
                .child(plusone, "+1")
                .child(broadcast, "broadcast", "announce", "bcast")
                .child(backup, "backup")
                .child(setrank, "setrank")
                .child(islandtp, "islandtp").build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), commandSpec, "staff", "admin", "mod");
    }
}
