package neusfear.theforgecore.modules.warps;

import com.flowpowered.math.vector.Vector3d;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import org.spongepowered.api.scheduler.Task;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class AdminWarpHandler {

    private static ArrayList<Warp> adminwarps = new ArrayList<>();
    private AdminWarpHandler adminWarpHandler;

    public AdminWarpHandler() {
        adminWarpHandler = this;
        loadAdminWarps();

        //Removes one min from the time a warp is being advertised.
        Task advertisedWarpsTask = Task.builder().execute(() -> tickAdvertisedWarps())
                .delay(1, TimeUnit.MINUTES).interval(1, TimeUnit.MINUTES)
                .name("TheForgeCore - Advertisement Ticks.").submit(TheForgeCore.getInstance());
    }

    private void tickAdvertisedWarps() {
        if (ServerDataHandler.getServerData().getAdvertisedwarps().size() < 1) { return; }
        ArrayList<Warp> warps = ServerDataHandler.getServerData().getAdvertisedwarps();
        ArrayList<Warp> warpsToRemove = new ArrayList<>();
        for (Warp warp : warps) {
            warp.decrimentAdvertised();
            if (warp.getAdvertised() < 1) {
                warpsToRemove.add(warp);
            }
        }
        warps.removeAll(warpsToRemove);
    }

    public static void loadAdminWarps() {
        adminwarps = ServerDataHandler.getServerData().getAdminWarps();
        updateAdminWarpsInServerData();
    }

    public static ArrayList<Warp> getAdminwarps() {
        return adminwarps;
    }

    public static Warp getAdminWarp(String name) {
        for (Warp warp: getAdminwarps()) {
            if (warp.getName().equals(name)) {
                return warp;
            }
        }
        return null;
    }

    public static void createNewAdminWarp(Vector3d rotation, String name, int x, int y, int z) {
        Warp warp = new Warp(rotation, name, x, y, z);
        addAdminWarp(warp);
    }

    public static void addAdminWarp(Warp warp) {
        adminwarps.add(warp);
        updateAdminWarpsInServerData();
    }

    public static void removeAdminWarp(String warpName) {
        for (Warp warp : getAdminwarps()) {
            if (warp.getName().equals(warpName)) {
                getAdminwarps().remove(warp);
                return;
            }
        }
        updateAdminWarpsInServerData();
    }

    public static void updateAdminWarpsInServerData() {
        ServerDataHandler.getServerData().setAdminwarps(getAdminwarps());
    }

    public static boolean isAdminWarpSet(String warpName) {
        if (getAdminWarp(warpName) != null) {
            return true;
        }
        return false;
    }

}
