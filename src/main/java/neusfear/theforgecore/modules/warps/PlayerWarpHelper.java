package neusfear.theforgecore.modules.warps;

import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;

import java.util.ArrayList;

public class PlayerWarpHelper {

    public static ArrayList<Warp> getWarps() {
        ArrayList<Warp> warps = new ArrayList<>();

        for (PlayerData playerData : PlayerDataHandler.getLoadedPlayers()) {
            warps.addAll(playerData.getWarps());
        }
        return warps;
    }

    public static Warp getWarp(String name) {
        for (Warp warp : getWarps()) {
            if (name.equals(warp.getName())) {
                return warp;
            }
        }
        return null;
    }

    public static boolean warpExists(String name) {
        return getWarp(name) != null;
    }
}
