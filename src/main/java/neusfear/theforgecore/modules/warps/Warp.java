package neusfear.theforgecore.modules.warps;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;

public class Warp extends Vector3i {

    private Vector3d rotation;
    private String name;
    private String owner;
    private int advertised;

    public Warp(Vector3d rotation, String warpName, int x, int y, int z) {
        super(x, y, z);
        setRotation(rotation);
        setName(warpName);
        setOwner("adminwarp");
        setAdvertised(0);
    }

    public Warp(Vector3d rotation, String warpName, int x, int y, int z, String ownerName) {
        super(x, y, z);
        setRotation(rotation);
        setName(warpName);
        setOwner(ownerName);
        setAdvertised(0);
    }

    public void setName(String warpName) {
        name = warpName;
    }

    public String getName() {
        return name;
    }

    public String getOwner() { return owner; }

    public void setOwner(String owner) { this.owner = owner; }

    public int getAdvertised() {
        return advertised;
    }

    public void setAdvertised(int advertised) {
        this.advertised = advertised;
    }

    public void decrimentAdvertised() {
        if (getAdvertised() > 0) { setAdvertised(getAdvertised() - 1); }
    }

    public Vector3d getRotation() {
        return rotation;
    }

    public void setRotation(Vector3d rotation) {
        this.rotation = rotation;
    }
}
