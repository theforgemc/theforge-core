package neusfear.theforgecore.modules.warps.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.warps.AdminWarpHandler;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class WarpsCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Lists the AdminWarps"))
                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player"))))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    String playerSrc = args.<String>getOne("player").orElse("adminwarplist");
                    if (src instanceof Player) {
                        if (playerSrc.equals("adminwarplist")) {
                            src.sendMessage(MsgHelper.info("List of Admin Warps:"));
                            for (Warp warp : AdminWarpHandler.getAdminwarps()) {
                                src.sendMessage(MsgHelper.formatted("&7- &f" + warp.getName()));
                            }
                        } else {
                            Optional<User> user = PlayerDataHandler.getUserFromName(playerSrc);
                            if (user.isPresent()) {
                                String uuid = user.get().getUniqueId().toString();
                                if (PlayerDataHandler.getPlayerdata(uuid).getWarps().size() > 0) {
                                    src.sendMessage(MsgHelper.info(src.getName() + "'s warps are:"));
                                    for (Warp warp : PlayerDataHandler.getPlayerdata(uuid).getWarps()) {
                                        src.sendMessage(MsgHelper.formatted("&7- &f" + warp.getName()));
                                    }
                                } else {
                                    src.sendMessage(MsgHelper.error("That player has no warps set."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("That player has never joined before."));
                            }
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "warps");
    }
}
