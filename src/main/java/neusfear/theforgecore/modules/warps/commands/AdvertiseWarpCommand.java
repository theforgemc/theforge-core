package neusfear.theforgecore.modules.warps.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class AdvertiseWarpCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Advertises a Warp"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("warpName"))),
                        GenericArguments.onlyOne(GenericArguments.integer(Text.of("minutes")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        String warpName = args.<String>getOne("warpName").get();
                        for (Warp warp : ServerDataHandler.getServerData().getAdvertisedwarps()) {
                            if (warp.getName().equals(warpName)) {
                                src.sendMessage(MsgHelper.error("That warp is already being advertised."));
                                return CommandResult.success();
                            }
                        }
                        int minutes = args.<Integer>getOne(Text.of("minutes")).get();
                        if (playerData.getMoney() >= 2*minutes) {
                            for (Warp warp : playerData.getWarps()) {
                                if (warp.getName().equals(warpName)) {
                                    playerData.removeMoney(2*minutes);
                                    warp.setAdvertised(minutes);
                                    ServerDataHandler.getServerData().addAdvertisedwarp(warp);
                                    src.sendMessage(MsgHelper.info("Congrats, you advertised the warp " + warpName + " for $" + 2*minutes + "."));
                                    return CommandResult.success();
                                }
                            }
                            src.sendMessage(MsgHelper.error("That warp doesn't exist."));
                        } else {
                            src.sendMessage(MsgHelper.error("You need $" + 2*minutes + " in your bank to advertise a warp."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "advertisewarp", "awarp", "advertwarp");
    }
}
