package neusfear.theforgecore.modules.warps.commands;

import com.flowpowered.math.vector.Vector3d;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.warps.AdminWarpHandler;
import neusfear.theforgecore.modules.warps.PlayerWarpHelper;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class SetWarpCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Sets a warp for a player"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("warpName")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        String warpName = args.<String>getOne("warpName").get();
                        if (!PlayerWarpHelper.warpExists(warpName) && !AdminWarpHandler.isAdminWarpSet(warpName)) {
                            int maxWarps = (int) Math.rint(Math.pow(((playerData.getPermissionLevel() + 1) / 2), 2));
                            if ((playerData.getWarps().size() < maxWarps)) {
                                if (playerData.getMoney() >= 1000.0) {
                                    Location<World> location = player.getLocation();
                                    Vector3d rotation = player.getRotation();
                                    playerData.addWarp(new Warp(rotation, warpName, location.getBlockX(), location.getBlockY(), location.getBlockZ(), playerData.getUuid()));
                                    playerData.removeMoney(1000.0);
                                    src.sendMessage(MsgHelper.info("Congrats, you set the warp " + warpName + " to your current location for $1000."));
                                } else {
                                    src.sendMessage(MsgHelper.error("You need $1000 in your bank to set a warp."));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("You have reached the maximum number of warps allowed for your rank."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That warp already exists, get to it with /warp " + warpName));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "setwarp", "warpset");
    }
}
