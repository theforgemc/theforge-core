package neusfear.theforgecore.modules.warps.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class AdvertisedWarpsCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Lists the player advertised warps."))
                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("arg1"))))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    String playerSrc = args.<String>getOne("arg1").orElse("public");
                    if (src instanceof Player) {
                        if (playerSrc.equals("public")) {
                            src.sendMessage(MsgHelper.info("List of Advertised Warps:"));
                            for (Warp warp : ServerDataHandler.getServerData().getAdvertisedwarps()) {
                                src.sendMessage(MsgHelper.formatted("&7- &f" + warp.getName() + "&7 : " + PlayerDataHandler.getPlayerdata(warp.getOwner()).getUsername() + "\n"));
                            }
                        } else {
                            String uuid = ((Player) src).getUniqueId().toString();
                            if (PlayerDataHandler.getPlayerdata(uuid).getWarps().size() > 0) {
                                String warplist = "";
                                for (Warp pwarp : PlayerDataHandler.getPlayerdata(uuid).getWarps()) {
                                    for (Warp awarp : ServerDataHandler.getServerData().getAdvertisedwarps()) {
                                        if (pwarp.getName().equals(awarp.getName())) {
                                            warplist = warplist + "&7- &f" + pwarp.getName() + " : " + Time.getTimeFromMin(awarp.getAdvertised()) + "\n";
                                        }
                                    }
                                }
                                if (warplist.equals("")) {
                                    src.sendMessage(MsgHelper.error("You don't have any advertised warps."));
                                } else {
                                    src.sendMessage(MsgHelper.info("Your advertised warps: \n" + warplist));
                                }
                            } else {
                                src.sendMessage(MsgHelper.error("There are not currently andy warps being advertised. Advertise yours with &f/advertisewarp <warp>"));
                            }
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "advertisedwarps", "awarps", "adwarps", "advertwarps");
    }
}
