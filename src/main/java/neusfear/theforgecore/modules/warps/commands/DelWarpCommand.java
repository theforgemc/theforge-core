package neusfear.theforgecore.modules.warps.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerData;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class DelWarpCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Sets a warp for a player"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("warpName")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        String warpName = args.<String>getOne("warpName").get();
                        for (Warp warp : playerData.getWarps()) {
                            if (warpName.equals(warp.getName())) {
                                ServerData serverData = ServerDataHandler.getServerData();
                                double refund = 500.0;
                                serverData.removeFeaturedWarp(warp);
                                for (Warp aWarp : serverData.getAdvertisedwarps()) {
                                    if (aWarp.getName().equals(warpName)) {
                                        refund += aWarp.getAdvertised();
                                        serverData.getAdvertisedwarps().remove(aWarp);
                                        break;
                                    }
                                }
                                playerData.removeWarp(warp);
                                playerData.addMoney(refund);
                                src.sendMessage(MsgHelper.info("Successfully removed the warp " + warpName + " and were refunded $" + refund));
                                return CommandResult.success();
                            }
                        }
                        src.sendMessage(MsgHelper.error("You do not own a warp called " + warpName));
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "delwarp", "deletewarp", "warpdelete");
    }
}
