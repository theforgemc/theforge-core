package neusfear.theforgecore.modules.warps.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class FeaturedWarpsCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Lists the player advertised warps."))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        src.sendMessage(MsgHelper.info("List of Featured Warps:"));
                        for (Warp warp : ServerDataHandler.getServerData().getFeaturedwarps()) {
                            src.sendMessage(MsgHelper.formatted("&7- &f" + warp.getName() + "&7 : " + PlayerDataHandler.getPlayerdata(warp.getOwner()).getUsername() + "\n"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "featuredwarps", "fwarps");
    }
}
