package neusfear.theforgecore.modules.warps.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.warps.AdminWarpHandler;
import neusfear.theforgecore.modules.warps.PlayerWarpHelper;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class WarpCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Teleports the player to a warp"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("warpName")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        String warpName = args.<String>getOne("warpName").get();
                        Player player = (Player) src;
                        World world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);
                        Warp warp = null;
                        if (AdminWarpHandler.isAdminWarpSet(warpName)) {
                            warp = AdminWarpHandler.getAdminWarp(warpName);
                        }
                        for (Warp warpl : PlayerWarpHelper.getWarps()) {
                            if (warpName.equalsIgnoreCase(warpl.getName())) {
                                warp = warpl;
                            }
                        }
                        if (warp != null) {
                            player.setLocationAndRotationSafely(world.getLocation(warp), warp.getRotation());
                            src.sendMessage(MsgHelper.info("Warping to " + warpName + "..."));
                        } else {
                            src.sendMessage(MsgHelper.error("That warp does not exist, list warps with /warps or list a player's warps with /warps <player>"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "warp", "warpto");
    }
}
