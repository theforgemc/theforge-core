package neusfear.theforgecore.modules.playtime;

import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.permissions.Permissions;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.action.InteractEvent;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.message.MessageChannelEvent;

public class AFK {

    @Listener(order = Order.LAST)
    public void onPlayerMove(final MoveEntityEvent event, @Root Player player) {
        if (isAfk(player)) {
            disableAfk(player);
        }
        PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).setAfkLevel(0);
    }

    @Listener
    public void onPlayerChat(final MessageChannelEvent.Chat event, @Root Player player) {
        if (isAfk(player)) {
            disableAfk(player);
        }
        PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).setAfkLevel(0);
    }

    @Listener
    public void onPlayerCommand(final SendCommandEvent event, @Root Player player) {
        if (!event.getCommand().toLowerCase().equals("afk")) {
            if (isAfk(player)) {
                disableAfk(player);
            }
        }
        PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).setAfkLevel(0);
    }

    @Listener(order = Order.LAST)
    public void onPlayerInteract(final InteractEvent event, @Root Player player) {
        if (isAfk(player)) {
            disableAfk(player);
        }
        PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).setAfkLevel(0);
    }

    public boolean isAfk(Player player) {
        return PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getAfkLevel() >= 5;
    }

    public void disableAfk(Player player) {
        PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).setAfkLevel(0);
        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
        Sponge.getServer().getBroadcastChannel().send(MsgHelper.formatted("&7* &f" +
                Permissions.getRankPrefix(playerData.getPermissionLevel()) + " " + playerData.getUsername() + " &7is no longer AFK"));
    }

}
