package neusfear.theforgecore.modules.playtime.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.permissions.Permissions;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class AfkCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Toggles the player's afk status."))
                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        Player player = (Player) src;
                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        int afkLevel = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getAfkLevel();
                        if (afkLevel < 5) {
                            PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).setAfkLevel(6);
                            src.sendMessage(MsgHelper.info("You are now AFK."));
                            Sponge.getServer().getBroadcastChannel().send(MsgHelper.formatted("&7* &f" +
                                    Permissions.getRankPrefix(playerData.getPermissionLevel()) + " " + playerData.getUsername() + " &7is now AFK"));
                        } else {
                            PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).setAfkLevel(0);
                            src.sendMessage(MsgHelper.info("You are no longer AFK."));
                            Sponge.getServer().getBroadcastChannel().send(MsgHelper.formatted("&7* &f" +
                                    Permissions.getRankPrefix(playerData.getPermissionLevel()) + " " + playerData.getUsername() + " &7is now AFK"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only command."));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "afk");
    }
}
