package neusfear.theforgecore.modules.homes;

import com.flowpowered.math.vector.Vector3i;

public class Home extends Vector3i {

    private String name;
    private String owner;

    public Home(String homeName, int x, int y, int z, String ownerName) {
        super(x, y, z);
        setName(homeName);
        setOwner(ownerName);
    }

    public void setName(String homeName) {
        name = homeName;
    }

    public String getName() {
        return name;
    }

    public String getOwner() { return owner; }

    public void setOwner(String owner) { this.owner = owner; }
}
