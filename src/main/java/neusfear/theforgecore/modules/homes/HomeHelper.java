package neusfear.theforgecore.modules.homes;

import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;

import java.util.ArrayList;

public class HomeHelper {

    public static ArrayList<Home> getHomes() {
        ArrayList<Home> homes = new ArrayList<>();

        for (PlayerData playerData : PlayerDataHandler.getLoadedPlayers()) {
            homes.addAll(playerData.getHomes());
        }
        return homes;
    }

    public static Home getHome(String name) {
        for (Home home : getHomes()) {
            if (name.equals(home.getName())) {
                return home;
            }
        }
        return null;
    }

    public static boolean homeExists(String name) {
        return getHome(name) != null;
    }
}
