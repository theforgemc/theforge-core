package neusfear.theforgecore.modules.homes.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.homes.Home;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class SetHomeCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Sets a home for a player"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("homeName")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        String homeName = args.<String>getOne("homeName").get();
                        int maxHomes = (int) Math.ceil(Math.pow(((playerData.getPermissionLevel() + 1) / 2), 2));
                        if ((playerData.getHomes().size() < maxHomes)) {
                            Location<World> location = player.getLocation();
                            playerData.addHome(new Home(homeName, location.getBlockX(), location.getBlockY(), location.getBlockZ(), playerData.getUuid()));
                            src.sendMessage(MsgHelper.info("Congrats, you set the home " + homeName + " to your current location."));
                        } else {
                            src.sendMessage(MsgHelper.error("You have reached the maximum number of homes allowed for your rank."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "sethome", "homeset");
    }
}
