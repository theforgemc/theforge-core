package neusfear.theforgecore.modules.homes.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.homes.Home;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class HomeCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Teleports the player to a home"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("homeName")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        String homeName = args.<String>getOne("homeName").get();
                        Player player = (Player) src;
                        for (Home home : PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getHomes()) {
                            if (homeName.equals(home.getName())) {
                                World world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);
                                player.setLocationSafely(world.getLocation(home));
                                src.sendMessage(MsgHelper.info("Teleporting to home: " + homeName + "..."));
                                return CommandResult.success();
                            }
                        }
                        src.sendMessage(MsgHelper.error("That home does not exist, list homes with /homes"));
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "home", "hometo");
    }
}
