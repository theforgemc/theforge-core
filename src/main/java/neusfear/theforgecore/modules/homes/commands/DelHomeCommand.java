package neusfear.theforgecore.modules.homes.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.homes.Home;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class DelHomeCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Sets a home for a player"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("homeName")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
                        String homeName = args.<String>getOne("homeName").get();
                        for (Home home : playerData.getHomes()) {
                            if (homeName.equals(home.getName())) {
                                playerData.removeHome(home);
                                src.sendMessage(MsgHelper.info("Successfully removed the home " + homeName));
                                return CommandResult.success();
                            }
                        }
                        src.sendMessage(MsgHelper.error("You do not own a home called " + homeName));
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "delhome", "deletehome", "homedelete");
    }
}
