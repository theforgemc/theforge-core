package neusfear.theforgecore.modules.homes.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.homes.Home;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class HomesCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Lists a player's homes"))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getHomes().size() < 1) {
                            src.sendMessage(MsgHelper.error("You have no homes set, set one with <e>/sethome<e>"));
                            return CommandResult.success();
                        }
                        src.sendMessage(MsgHelper.info(player.getName() + "'s Homes:"));
                        for (Home home : PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getHomes()) {
                            src.sendMessage(MsgHelper.formatted(home.getName()));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "homes", "homelist");
    }
}
