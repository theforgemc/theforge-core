package neusfear.theforgecore.modules.discord.chatlink;

import net.dv8tion.jda.core.EmbedBuilder;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.discord.Bot;
import neusfear.theforgecore.modules.discord.BotArgs;
import org.spongepowered.api.Sponge;

import java.awt.*;
import java.net.MalformedURLException;

public class DiscordChatLink {

    public static void sendMessage(BotArgs args) {
        args.getMessage().delete().queue();
        sendToDiscord(args.getAuthor().getName(), args.getMessage().getContentRaw(), true);
        try {
            Sponge.getGame().getServer().getBroadcastChannel().send(MsgHelper.fromDiscord(args.getAuthor().getAsTag(), args.getMessage().getContentRaw()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void positiveNegativeMessages(boolean positive, String message) {
        EmbedBuilder eb = new EmbedBuilder();
        if (positive) {
            eb.setColor(Color.GREEN);
        } else {
            eb.setColor(Color.RED);
        }
        eb.setDescription(message);
        Bot.getJda().getTextChannelById(Bot.getLinkchannel()).sendMessage(eb.build()).queue();
    }

    public static void sendToDiscord(String name, String message, boolean fromDiscord) {
        EmbedBuilder eb = new EmbedBuilder();
        if (fromDiscord) {
            eb.setColor(Color.MAGENTA);
        } else {
            eb.setColor(Color.ORANGE);
        }
        eb.setDescription(name + ": " + message);
        Bot.getJda().getTextChannelById(Bot.getLinkchannel()).sendMessage(eb.build()).queue();
    }
}
