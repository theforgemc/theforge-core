package neusfear.theforgecore.modules.discord;

public abstract class BotCommand {
    abstract public void process(BotArgs args);
}
