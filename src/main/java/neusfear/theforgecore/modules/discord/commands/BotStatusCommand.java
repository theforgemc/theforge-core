package neusfear.theforgecore.modules.discord.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;
import neusfear.theforgecore.modules.discord.BotArgs;
import neusfear.theforgecore.modules.discord.BotCommand;
import org.spongepowered.api.Sponge;

import java.awt.*;

public class BotStatusCommand extends BotCommand {
    @Override
    public void process(BotArgs args) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Server Status");
        eb.setColor(Color.ORANGE);
        eb.setDescription("Gives current information about the server");
        eb.addField(new MessageEmbed.Field("Online", "yes", true));
        eb.addField(new MessageEmbed.Field("TPS", "" + Sponge.getGame().getServer().getTicksPerSecond(), true));
        eb.addField(new MessageEmbed.Field("Players", Sponge.getGame().getServer().getOnlinePlayers().size() + "/" +
                Sponge.getGame().getServer().getMaxPlayers(), true));
        args.getChannel().sendMessage(eb.build()).queue();
    }
}
