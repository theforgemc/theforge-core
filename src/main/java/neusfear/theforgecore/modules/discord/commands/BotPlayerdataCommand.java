package neusfear.theforgecore.modules.discord.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.discord.BotArgs;
import neusfear.theforgecore.modules.discord.BotCommand;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.playerdata.helpers.LevelHelper;

import java.awt.*;

public class BotPlayerdataCommand extends BotCommand {
    @Override
    public void process(BotArgs args) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.ORANGE);
        if (args.getMessage().getContentRaw().contains(" ")) {
            String player = args.getMessage().getContentRaw().split(" ")[1];
            if (player != null && PlayerDataHandler.hasUserJoined(player)) {
                PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(player);

                eb.setTitle(playerData.getUsername() + "'s PlayerData");
                eb.addField(new MessageEmbed.Field("Level", "" + LevelHelper.getLevelForXP(playerData.getExperience()), true));
                eb.addField(new MessageEmbed.Field("Reputation", "" + playerData.getReputation(), true));
                eb.addField(new MessageEmbed.Field("Infractions", "" + playerData.getInfractions().size(), true));
                eb.addField(new MessageEmbed.Field("Joined", playerData.getFirstJoin(), true));
                eb.addField(new MessageEmbed.Field("Playtime", Time.getTimeFromMin(playerData.getOntime()), true));
                eb.addBlankField(true);
                eb.setFooter("UUID: " + playerData.getUuid(), null);

            } else {
                eb.setTitle("That player has never joined before.");
            }
        } else {
            eb.setDescription("You must specify a player.");
        }
        args.getChannel().sendMessage(eb.build()).queue();
    }
}
