package neusfear.theforgecore.modules.discord.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.Role;
import neusfear.theforgecore.modules.discord.BotArgs;
import neusfear.theforgecore.modules.discord.BotCommand;
import neusfear.theforgecore.modules.discord.handlers.StaffUUIDHandler;
import neusfear.theforgecore.modules.infractions.InfractionTypes;
import neusfear.theforgecore.modules.infractions.helpers.InfractionHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;

import java.awt.*;

public class BotAddInfractionCommand extends BotCommand {
    @Override
    public void process(BotArgs args) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.ORANGE);
        boolean error = true;
        for (Role role : args.getMember().getRoles()) {
            if (role.getName().equalsIgnoreCase("staff") && !StaffUUIDHandler.getUUIDFromDiscordID(args.getAuthor().getId()).equalsIgnoreCase("none")) {
                error = false;
                String[] arguements = args.getMessage().getContentRaw().split(" ", 4);
                if (arguements.length == 4) {
                    if (!PlayerDataHandler.hasUserJoined(arguements[1])) {
                        eb.setTitle("That player has never joined the server before");
                        eb.setDescription("Note that you are adding the infraction to a player not a discord member.");
                        break;
                    }
                    if (InfractionTypes.getType(arguements[2]).equalsIgnoreCase("invalid")) {
                        eb.setTitle("That is not a valid infraction type.");
                        break;
                    }
                    PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(arguements[1]);
                    InfractionHelper.createInfraction(playerData.getUuid(), arguements[2], StaffUUIDHandler.getUUIDFromDiscordID(args.getAuthor().getId()), arguements[3]);
                    eb.setTitle("Infraction added to " + playerData.getUsername());
                    eb.addField(new MessageEmbed.Field("Type", InfractionTypes.getType(arguements[2]), true));
                    eb.addField(new MessageEmbed.Field("Issuer", PlayerDataHandler.getPlayerdata(StaffUUIDHandler.getUUIDFromDiscordID(args.getAuthor().getId())).getUsername(), true));
                    eb.addField(new MessageEmbed.Field("Reason", arguements[3], true));

                } else {
                    eb.setTitle("Usage");
                    eb.setDescription("!addinfraction <player> <infraction type> <reason>");
                }
            }
        }
        if (error) eb.setTitle("You don't have permission to issue infractions.");
        args.getChannel().sendMessage(eb.build()).queue();
    }
}
