package neusfear.theforgecore.modules.discord.handlers;

import neusfear.theforgecore.modules.discord.BotCommandRegistry;
import neusfear.theforgecore.modules.discord.commands.BotAddInfractionCommand;
import neusfear.theforgecore.modules.discord.commands.BotPlayerdataCommand;
import neusfear.theforgecore.modules.discord.commands.BotStatusCommand;

public class BotCommandHandler {

    public BotCommandHandler() {
        BotCommandRegistry.register(new BotStatusCommand(), "status", "online", "info");
        BotCommandRegistry.register(new BotPlayerdataCommand(), "playerdata", "pd");
        BotCommandRegistry.register(new BotAddInfractionCommand(), "addinfraction", "addinf");
    }
}
