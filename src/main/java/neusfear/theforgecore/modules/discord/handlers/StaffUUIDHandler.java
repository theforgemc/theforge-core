package neusfear.theforgecore.modules.discord.handlers;

import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;

public class StaffUUIDHandler {

    public static String getUUIDFromDiscordID(String id) {
        switch (id) {
            case "251956696476286976": return PlayerDataHandler.getPlayerdataFromUsername("codes320").getUuid();
            case "332750218837622784": return PlayerDataHandler.getPlayerdataFromUsername("___________").getUuid();
            case "204020433920458752": return PlayerDataHandler.getPlayerdataFromUsername("NeusFear").getUuid();
            case "309804239733915648": return PlayerDataHandler.getPlayerdataFromUsername("PolarZer0").getUuid();
            case "106551183228805120": return PlayerDataHandler.getPlayerdataFromUsername("TonyMaster21").getUuid();
            case "332287110327435284": return PlayerDataHandler.getPlayerdataFromUsername("Aiden099").getUuid();
            case "335502341190975489": return PlayerDataHandler.getPlayerdataFromUsername("Emmallea").getUuid();
            case "83761733558075392": return PlayerDataHandler.getPlayerdataFromUsername("Jhwx").getUuid();
            case "209062024020230144": return PlayerDataHandler.getPlayerdataFromUsername("MarkElf").getUuid();
            case "98176913570279424": return PlayerDataHandler.getPlayerdataFromUsername("Plumz").getUuid();
            case "174585712488873986": return PlayerDataHandler.getPlayerdataFromUsername("Woodlandecho").getUuid();
            default: return "none";
        }
    }

}
