package neusfear.theforgecore.modules.discord;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.modules.discord.chatlink.DiscordChatLink;
import neusfear.theforgecore.modules.discord.handlers.BotCommandHandler;

import javax.security.auth.login.LoginException;

public class Bot extends ListenerAdapter {

    private static final String TOKEN = "MzQzMDU4MDU4MDg1NDAwNTc3.DxXB7g.7zUwUsz_sIzfiGaYuVJ9TCJ_HaU";
    private static final String LINKCHANNEL = "569279826658197526";
    private static JDA jda;

    public static void startBot() {
        LoggerHelper.getLogger().info("Loading Disocrd Bot.");
        try {
            JDA builder = new JDABuilder(TOKEN)
                    .addEventListener(new Bot())
                    .build();
            jda = builder;
            builder.awaitReady();
            new BotCommandHandler();
            LoggerHelper.getLogger().info("Finished loading Discord Bot.");
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static JDA getJda() {
        return jda;
    }

    public static String getLinkchannel() {
        return LINKCHANNEL;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getMessage().getContentRaw().startsWith("!")) {
            BotCommandRegistry.execute(event.getMessage().getContentRaw(),
                    new BotArgs(
                            event.getMessage(),
                            event.getAuthor(),
                            event.getGuild(),
                            event.getChannel(),
                            event.getMember()
                    ));
            return;
        }
        if (event.getChannel().getId().equals(LINKCHANNEL) && !event.getAuthor().isBot()) {
            DiscordChatLink.sendMessage(
                    new BotArgs(
                    event.getMessage(),
                    event.getAuthor(),
                    event.getGuild(),
                    event.getChannel(),
                    event.getMember()
            ));
        }
    }
}

