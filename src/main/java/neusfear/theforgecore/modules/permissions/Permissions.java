package neusfear.theforgecore.modules.permissions;

import neusfear.theforgecore.helpers.ConfigHelper;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;

public class Permissions {

    public static String getRankColor(int permissionLevel) {
        return getRankPrefix(permissionLevel).substring(3, 5);
    }

    public static String getRankPrefix(int permissionLevel) {
        CommentedConfigurationNode config = ConfigHelper.getConfig();
        switch (permissionLevel) {
            default:
                return config.getNode("permissions", "rankprefixes", "guest").getString();
            case 0:
                return config.getNode("permissions", "rankprefixes", "guest").getString();
            case 1:
                return config.getNode("permissions", "rankprefixes", "guest").getString();
            case 2:
                return config.getNode("permissions", "rankprefixes", "member").getString();
            case 3:
                return config.getNode("permissions", "rankprefixes", "noble").getString();
            case 4:
                return config.getNode("permissions", "rankprefixes", "hero").getString();
            case 5:
                return config.getNode("permissions", "rankprefixes", "royal").getString();
            case 6:
                return config.getNode("permissions", "rankprefixes", "titan").getString();
            case 7:
                return config.getNode("permissions", "rankprefixes", "legend").getString();
            case 8:
                return config.getNode("permissions", "rankprefixes", "trialmod").getString();
            case 9:
                return config.getNode("permissions", "rankprefixes", "moderator").getString();
            case 10:
                return config.getNode("permissions", "rankprefixes", "admin").getString();
        }
    }

    public static int getPermissionLevelFromRank(String name) {
        switch (name) {
            default:
                return -1;
            case "guest":
                return 0;
            case "member":
                return 2;
            case "noble":
                return 3;
            case "hero":
                return 4;
            case "royal":
                return 5;
            case "titan":
                return 6;
            case "legend":
                return 7;
            case "trialmod":
                return 8;
            case "moderator":
                return 9;
            case "admin":
                return 10;
        }
    }

    public static String getRankFromPermissionLevel(int pL) {
        switch (pL) {
            default:
                return "guest";
            case 0:
                return "guest";
            case 1:
                return "guest";
            case 2:
                return "member";
            case 3:
                return "noble";
            case 4:
                return "hero";
            case 5:
                return "royal";
            case 6:
                return "titan";
            case 7:
                return "legend";
            case 8:
                return "trialmod";
            case 9:
                return "moderator";
            case 10:
                return "admin";
        }
    }
}
