package neusfear.theforgecore.modules.permissions.commands;

import neusfear.theforgecore.helpers.ConfigHelper;
import neusfear.theforgecore.helpers.MsgHelper;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.*;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class RankCommand implements CommandCallable {

    private final Optional<Text> desc = Optional.of(Text.of("Ranks Command."));
    private final Optional<Text> help = Optional.of(Text.of("Lists/Provides information for the rank specified."));

    @Override
    public CommandResult process(CommandSource source, String arguments) throws CommandException {
        if (source instanceof Player) {
            Player player = (Player) source;
            CommandManager commandManager = Sponge.getCommandManager();

            if (arguments.equals("?")) {
                source.sendMessage(MsgHelper.info("/rank [?, list, <rank>]"));
            } else if (arguments.equals("help")) {
                commandManager.process(source, "rank ?");
            } else if (arguments.equals("list")) {
                source.sendMessage(MsgHelper.info("Ranks are: &fGuest, Member, Noble, Hero, Paladain, Royal, Legend, TrialMod, Moderator, Admin"));
            } else if (arguments.equals("guest")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "guest").getString()));
            } else if (arguments.equals("member")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "member").getString()));
            } else if (arguments.equals("noble")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "noble").getString()));
            } else if (arguments.equals("hero")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "hero").getString()));
            } else if (arguments.equals("paladin")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "paladin").getString()));
            } else if (arguments.equals("royal")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "royal").getString()));
            } else if (arguments.equals("legend")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "legend").getString()));
            } else if (arguments.equals("trialmod")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "trialmod").getString()));
            } else if (arguments.equals("moderator")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "moderator").getString()));
            } else if (arguments.equals("admin")) {
                source.sendMessage(MsgHelper.info(ConfigHelper.getConfig().getNode("rankinfo", "admin").getString()));
            } else {
                commandManager.process(source, "rank list");
            }


        } else {
            source.sendMessage(MsgHelper.error("That is a player only command."));
        }
        return CommandResult.success();
    }

    @Override
    public List<String> getSuggestions(CommandSource source, String arguments, @Nullable Location<World> targetPosition) throws CommandException {
        ArrayList<String> suggestions = new ArrayList<>(Arrays.asList("help", "?", "list", "guest", "member", "noble", "hero", "paladin", "royal", "legend", "trialmod", "moderator", "admin"));
        return suggestions;
    }

    @Override
    public boolean testPermission(CommandSource source) {
        return true;
    }

    @Override
    public Optional<Text> getShortDescription(CommandSource source) {
        return desc;
    }

    @Override
    public Optional<Text> getHelp(CommandSource source) {
        return help;
    }

    @Override
    public Text getUsage(CommandSource source) {
        return Text.EMPTY;
    }
}
