package neusfear.theforgecore.modules.infractions;

import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;

public class Infraction {

    private String type;
    private String issuer;
    private String reason;
    private String issueDate;
    private String punishmentResult;

    public Infraction(String type, String issuer, String reason) {
        this.type = InfractionTypes.getType(type);
        this.issuer = issuer;
        this.reason = reason;
        this.issueDate = Time.getNow().toString();
    }

    public String getType() {
        return type;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getReason() {
        return reason;
    }

    public String getIssueDate() {
        return issueDate;
    }

    @Override
    public String toString() {
        return getType() + " infraction issued by: " + PlayerDataHandler.getPlayerdata(getIssuer()).getUsername() +
                " on " + getIssueDate();
    }

    public String getPunishmentResult() {
        return punishmentResult;
    }

    public void setPunishmentResult(String punishmentResult) {
        this.punishmentResult = punishmentResult;
    }
}
