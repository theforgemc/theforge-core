package neusfear.theforgecore.modules.infractions;

public class InfractionTypes {
    public static final String DISRESPECT = "disrespect";
    public static final String ADVERTISEMENTS = "advertisements";
    public static final String CHAT = "chat";
    public static final String CHEATING = "cheating";
    public static final String SENSITIVITY = "sensitivity";
    public static final String SECURITY = "security";

    public static String getType(String type) {
        switch (type) {
            case "disrespect":
                return DISRESPECT;
            case "respect":
                return DISRESPECT;
            case "advertisements":
                return ADVERTISEMENTS;
            case "ads":
                return ADVERTISEMENTS;
            case "adverts":
                return ADVERTISEMENTS;
            case "advertising":
                return ADVERTISEMENTS;
            case "advertisement":
                return ADVERTISEMENTS;
            case "chat":
                return CHAT;
            case "spam":
                return CHAT;
            case "language":
                return CHAT;
            case "cheating":
                return CHEATING;
            case "mods":
                return CHEATING;
            case "hacks":
                return CHEATING;
            case "hacking":
                return CHEATING;
            case "xray":
                return CHEATING;
            case "fly":
                return CHEATING;
            case "flying":
                return CHEATING;
            case "modifications":
                return CHEATING;
            case "modding":
                return CHEATING;
            case "sensitivity":
                return SENSITIVITY;
            case "nudity":
                return SENSITIVITY;
            case "security":
                return SECURITY;
            case "ddos":
                return SECURITY;
            default:
                return "invalid";
        }
    }

    public static String getDiscription(String infractionType) {
        switch (getType(infractionType)) {
            case ADVERTISEMENTS:
                return "Any form of unauthorised advertisements of other servers or services.";
            case CHAT:
                return "You can be issued this infraction by breaking general chat rules/guidelines.";
            case CHEATING:
                return "Using any type of hacked client, x-ray, or other unauthorized mod.";
            case DISRESPECT:
                return "This infraction is reserved for players not being kind to others in any form.";
            case SECURITY:
                return "This infraction is given to those who pose a security threat to the server, including ddos.";
            case SENSITIVITY:
                return "Given to players who post or display sensitive content, keep it PG.";
        }
        return null;
    }
}
