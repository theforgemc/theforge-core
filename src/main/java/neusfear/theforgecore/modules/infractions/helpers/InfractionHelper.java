package neusfear.theforgecore.modules.infractions.helpers;

import neusfear.theforgecore.modules.infractions.Infraction;
import neusfear.theforgecore.modules.infractions.InfractionTypes;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;

import java.util.ArrayList;

public class InfractionHelper {

    public static void createInfraction(String uuid, String type, String issuer, String reason) {
        PlayerData playerData = PlayerDataHandler.getPlayerdata(uuid);
        Infraction infraction = new Infraction(InfractionTypes.getType(type), issuer, reason);
        infraction.setPunishmentResult(PunishmentHelper.issuePunishment(
                InfractionHelper.getPunishment(type, InfractionHelper.getNumOfInfractionType(type, uuid) + 1), uuid, infraction));
        playerData.addInfraction(infraction);
    }

    public static int getNumOfInfractionType(String type, String uuid) {
        if (!PlayerDataHandler.hasJoined(uuid)) {
            return -1;
        }
        ArrayList<Infraction> infractions = PlayerDataHandler.getPlayerdata(uuid).getInfractions();
        switch (InfractionTypes.getType(type)) {
            case InfractionTypes.ADVERTISEMENTS:
                return queryInfractionsNumber(infractions, InfractionTypes.ADVERTISEMENTS);
            case InfractionTypes.CHAT:
                return queryInfractionsNumber(infractions, InfractionTypes.CHAT);
            case InfractionTypes.CHEATING:
                return queryInfractionsNumber(infractions, InfractionTypes.CHEATING);
            case InfractionTypes.DISRESPECT:
                return queryInfractionsNumber(infractions, InfractionTypes.DISRESPECT);
            case InfractionTypes.SECURITY:
                return queryInfractionsNumber(infractions, InfractionTypes.SECURITY);
            case InfractionTypes.SENSITIVITY:
                return queryInfractionsNumber(infractions, InfractionTypes.SENSITIVITY);
        }
        return -1;
    }

    public static int getMaxChances(String infractionType) {
        int i = 1;
        while (!getPunishment(infractionType, i).equals("invalid")) {
            i++;
        }
        return i-1;
    }

    private static int queryInfractionsNumber(ArrayList<Infraction> infractions, String type) {
        int returnNum = 0;
        for (Infraction infraction : infractions) {
            if (infraction.getType().equals(type)) {
                returnNum++;
            }
        }
        return returnNum;
    }

    public static int getLatestInfractionNum(String uuid) {
        return PlayerDataHandler.getPlayerdata(uuid).getInfractions().size();
    }

    public static Infraction getLatestInfraction(String uuid) {
        return PlayerDataHandler.getPlayerdata(uuid).getInfractions().get(getLatestInfractionNum(uuid));
    }

    public static String getPunishment(String type, int offense) {
        switch (InfractionTypes.getType(type)) {
            case "advertisements":
                switch (offense) {
                    case 1:
                        return "title";
                    case 2:
                        return "kick";
                    case 3:
                        return "mute:1";
                    case 4:
                        return "mute:24";
                    case 5:
                        return "ban";
                    default:
                        return "invalid";
                }
            case "chat":
                switch (offense) {
                    case 1:
                        return "title";
                    case 2:
                        return "kick";
                    case 3:
                        return "mute:1";
                    case 4:
                        return "mute:4";
                    case 5:
                        return "mute:24";
                    case 6:
                        return "mute:48";
                    case 7:
                        return "tempban:6";
                    case 8:
                        return "tempban:24";
                    case 9:
                        return "tempban:48";
                    case 10:
                        return "ban";
                    default:
                        return "invalid";
                }
            case "cheating":
                switch (offense) {
                    case 1:
                        return "kick";
                    case 2:
                        return "tempban:24";
                    case 3:
                        return "tempban:48";
                    case 4:
                        return "ban";
                    default:
                        return "invalid";
                }
            case "disrespect":
                switch (offense) {
                    case 1:
                        return "title";
                    case 2:
                        return "kick";
                    case 3:
                        return "mute:1";
                    case 4:
                        return "mute:24";
                    case 5:
                        return "tempban:24";
                    case 6:
                        return "tempban:36";
                    case 7:
                        return "ban";
                    default:
                        return "invalid";
                }
            case "security":
                switch (offense) {
                    case 1:
                        return "ban";
                    default:
                        return "invalid";
                }
            case "sensitivity":
                switch (offense) {
                    case 1:
                        return "kick";
                    case 2:
                        return "tempban:24";
                    case 3:
                        return "ban";
                    default:
                        return "invalid";
                }
            default:
                return "invalid";
        }
    }
}
