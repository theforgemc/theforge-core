package neusfear.theforgecore.modules.infractions.helpers;

import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.infractions.Infraction;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.ban.BanService;
import org.spongepowered.api.text.title.Title;
import org.spongepowered.api.util.ban.Ban;
import org.spongepowered.api.util.ban.BanTypes;

import java.util.UUID;

public class PunishmentHelper {

    public static String issuePunishment(String punishment, String uuid, Infraction infraction) {
        String[] punishments = punishment.split(":");
        if (Sponge.getServer().getPlayer(UUID.fromString(uuid)).isPresent()) {
            Player player = Sponge.getServer().getPlayer(UUID.fromString(uuid)).get();

            switch (punishments[0]) {
                case "title":
                    sendWarningTitle(player);
                    return "Warning on " + infraction.getIssueDate();
                case "mute":
                    mutePlayer(uuid, Integer.parseInt(punishments[1]));
                    player.sendMessage(MsgHelper.error("You've had an infraction added to your account, resulting in a " + punishments[1] + " hour mute."));
                    return "Mute from " + infraction.getIssueDate() + " until " + Time.getFutureDate(Integer.parseInt(punishments[1]));
                case "kick":
                    player.kick(MsgHelper.formatted(
                            "&7This is the &cFINAL WARNING&7 before punishments\n" +
                            "&7You had an infraction added to your account\n" +
                            "&f/inf " + player.getName() + " " + (InfractionHelper.getLatestInfractionNum(player.getUniqueId().toString())+1) + "&7 for more info"));
                    return "Kick on " + infraction.getIssueDate();
                case "tempban":
                    String punishmentMessage = "Temporary ban from " + infraction.getIssueDate() + " until " + Time.getFutureDate(Integer.parseInt(punishments[1]));
                    player.kick(MsgHelper.formatted(
                            "&7You were &cTEMPORARILY BANNED&7 from the server\n" +
                                    "&7You had an infraction added to your account\n" +
                                    "&7Punishment info: &f" + punishmentMessage));
                    tempBanPlayer(uuid, Integer.parseInt(punishments[1]));
                    return punishmentMessage;
                case "ban":
                    player.kick(MsgHelper.formatted("&7You've been &cPERMANENTLY BANNED&7 from the server for exceeding\n" +
                            "&7the maximum &c" + infraction.getType() + "&7 infractions as of &f" + infraction.getIssueDate() + "\n" +
                            "&7Documented reason: &f" + infraction.getReason()));
                    banOnlinePlayer(player, infraction);
                    return "Permanent Ban on " + infraction.getIssueDate();
                default:
                    return "Error.";
            }
        } else {
            switch (punishments[0]) {
                case "mute":
                    mutePlayer(uuid, Integer.parseInt(punishments[1]));
                    return "Mute from " + infraction.getIssueDate() + " until " + Time.getFutureDate(Integer.parseInt(punishments[1]));
                case "tempban":
                    tempBanPlayer(uuid, Integer.parseInt(punishments[1]));
                    return "Temporary ban from " + infraction.getIssueDate() + " until " + Time.getFutureDate(Integer.parseInt(punishments[1]));
                case "ban":
                    banOfflinePlayer(uuid, infraction);
                    return "Permanent Ban on " + infraction.getIssueDate();
                default:
                    return "Error.";
            }
        }
    }

    private static void sendWarningTitle(Player player) {
        player.sendTitle(Title.builder().title(MsgHelper.formatted("&4Infraction Added"))
                .subtitle(MsgHelper.formatted("&7Use &f/inf " + player.getName() + " " +
                        InfractionHelper.getLatestInfractionNum(player.getUniqueId().toString()) + " &7for info.")).build());
    }

    private static void mutePlayer(String uuid, int hours) {
        PlayerDataHandler.getPlayerdata(uuid).setMuted(Time.getFutureDate(hours));
    }

    private static void tempBanPlayer(String uuid, int hours) {
        PlayerDataHandler.getPlayerdata(uuid).setBanStatus(Time.getFutureDate(hours));
    }

    private static void banOnlinePlayer(Player player, Infraction infraction) {
        Ban ban = Ban.builder().type(BanTypes.PROFILE).profile(player.getProfile())
                .reason(MsgHelper.formatted("&7You've been &cPERMANENTLY BANNED&7 from the server for exceeding\n" +
                        "&7the maximum &c" + infraction.getType() + "&7 infractions as of &f" + infraction.getIssueDate() + "\n" +
                        "&7Documented reason: &f" + infraction.getReason())).build();
        Sponge.getServiceManager().provide(BanService.class).get().addBan(ban);
    }

    private static void banOfflinePlayer(String uuid, Infraction infraction) {
        Ban ban = Ban.builder().type(BanTypes.PROFILE).profile(Sponge.getServer().getGameProfileManager().getDefaultCache().getById(UUID.fromString(uuid)).orElse(null))
                .reason(MsgHelper.formatted("&7You've been &cPERMANENTLY BANNED&7 from the server for exceeding\n" +
                        "&7the maximum &c" + infraction.getType() + "&7 infractions as of &f" + infraction.getIssueDate() + "\n" +
                        "&7Documented reason: &f" + infraction.getReason())).build();
        Sponge.getServiceManager().provide(BanService.class).get().addBan(ban);
    }
}
