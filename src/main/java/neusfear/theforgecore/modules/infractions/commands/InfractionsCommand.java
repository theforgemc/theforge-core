package neusfear.theforgecore.modules.infractions.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.infractions.Infraction;
import neusfear.theforgecore.modules.infractions.InfractionTypes;
import neusfear.theforgecore.modules.infractions.helpers.InfractionHelper;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class InfractionsCommand  {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command shows the playerdata of the player"))

                .arguments(
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))),
                        GenericArguments.optionalWeak(GenericArguments.onlyOne(GenericArguments.integer(Text.of("index"))))
                )

                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        if (args.<String>getOne("player").isPresent()) {
                            if (args.<Integer>getOne("index").isPresent()) {
                                String username = args.<String>getOne("player").get().trim();
                                int index = args.<Integer>getOne("index").get();
                                if (PlayerDataHandler.hasUserJoined(username)) {
                                    PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(username);

                                    if (playerData.getInfractions().size() >= index) {
                                        sendInfractionInfoMessage(src, playerData.getUuid(), index);
                                    } else {
                                        src.sendMessage(MsgHelper.error("That is not a valid infraction index."));
                                    }
                                } else {
                                    src.sendMessage(MsgHelper.error("That player has never joined before."));
                                }
                                return CommandResult.success();
                            } else {
                                String username = args.<String>getOne("player").get().trim();
                                if (PlayerDataHandler.hasUserJoined(username)) {
                                    String uuid = PlayerDataHandler.getPlayerdataFromUsername(username).getUuid();
                                    sendInfractionMenuMessage(src, uuid);
                                } else {
                                    src.sendMessage(MsgHelper.error("That player has never joined before."));
                                }
                                return CommandResult.success();
                            }
                        }
                        sendInfractionMenuMessage(src, ((Player) src).getUniqueId().toString());
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only command."));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "infractions", "infraction", "inf", "i");
    }

    private static void sendInfractionMenuMessage(CommandSource src, String uuid) {

        PlayerData playerData = PlayerDataHandler.getPlayerdata(uuid);

        int maxAd = InfractionHelper.getMaxChances(InfractionTypes.ADVERTISEMENTS);
        int maxChat = InfractionHelper.getMaxChances(InfractionTypes.CHAT);
        int maxCheat = InfractionHelper.getMaxChances(InfractionTypes.CHEATING);
        int maxDisRes = InfractionHelper.getMaxChances(InfractionTypes.DISRESPECT);
        int maxSec = InfractionHelper.getMaxChances(InfractionTypes.SECURITY);
        int maxSens = InfractionHelper.getMaxChances(InfractionTypes.SENSITIVITY);

        int adInfs = InfractionHelper.getNumOfInfractionType(InfractionTypes.ADVERTISEMENTS, uuid);
        int chatInfs = InfractionHelper.getNumOfInfractionType(InfractionTypes.CHAT, uuid);
        int cheatInfs = InfractionHelper.getNumOfInfractionType(InfractionTypes.CHEATING, uuid);
        int disResInfs = InfractionHelper.getNumOfInfractionType(InfractionTypes.DISRESPECT, uuid);
        int secInfs = InfractionHelper.getNumOfInfractionType(InfractionTypes.SECURITY, uuid);
        int sensInfs = InfractionHelper.getNumOfInfractionType(InfractionTypes.SENSITIVITY, uuid);

        String adInfractions = "";
        String chatInfractions = "";
        String cheatInfractions = "";
        String disResInfractions = "";
        String secInfractions = "";
        String sensInfractions = "";

        int i = 1;
        for (Infraction infraction : playerData.getInfractions()) {
            if (infraction.getType().equals(InfractionTypes.ADVERTISEMENTS)) { adInfractions = adInfractions + "&f#" + i + " &7" + infraction.toString() + "\n"; }
            if (infraction.getType().equals(InfractionTypes.CHAT)) { chatInfractions = chatInfractions + "&f#" + i + " &7" + infraction.toString() + "\n"; }
            if (infraction.getType().equals(InfractionTypes.CHEATING)) { cheatInfractions = cheatInfractions + "&f#" + i + " &7" + infraction.toString() + "\n"; }
            if (infraction.getType().equals(InfractionTypes.DISRESPECT)) { disResInfractions = disResInfractions + "&f#" + i + " &7" + infraction.toString() + "\n"; }
            if (infraction.getType().equals(InfractionTypes.SECURITY)) { secInfractions = secInfractions + "&f#" + i + " &7" + infraction.toString() + "\n"; }
            if (infraction.getType().equals(InfractionTypes.SENSITIVITY)) { sensInfractions = sensInfractions + "&f#" + i + " &7" + infraction.toString() + "\n"; }
            i++;
        }


        src.sendMessage(MsgHelper.info("Infractions for " + PlayerDataHandler.getPlayerdata(uuid).getUsername()));

        src.sendMessage(MsgHelper.interactable(
                MsgHelper.interactablePiece("Ads:          &7(&f" + adInfs + "&7) ",
                        InfractionTypes.getDiscription(InfractionTypes.ADVERTISEMENTS)),
                MsgHelper.interactablePiece(
                        MsgHelper.coloredProgress(10, adInfs, maxAd, "&7", "&4"), adInfractions)));

        src.sendMessage(MsgHelper.interactable(
                MsgHelper.interactablePiece("Chat:         &7(&f" + chatInfs + "&7) ",
                        InfractionTypes.getDiscription(InfractionTypes.CHAT)),
                MsgHelper.interactablePiece(
                        MsgHelper.coloredProgress(10, chatInfs, maxChat, "&7", "&4"), chatInfractions)));

        src.sendMessage(MsgHelper.interactable(
                MsgHelper.interactablePiece("Cheating:    &7(&f" + cheatInfs + "&7) ",
                        InfractionTypes.getDiscription(InfractionTypes.CHEATING)),
                MsgHelper.interactablePiece(
                        MsgHelper.coloredProgress(10, cheatInfs, maxCheat, "&7", "&4"), chatInfractions)));

        src.sendMessage(MsgHelper.interactable(
                MsgHelper.interactablePiece("Disrespect: &7(&f" + disResInfs + "&7) ",
                        InfractionTypes.getDiscription(InfractionTypes.DISRESPECT)),
                MsgHelper.interactablePiece(
                        MsgHelper.coloredProgress(10, disResInfs, maxDisRes, "&7", "&4"), disResInfractions)));

        src.sendMessage(MsgHelper.interactable(
                MsgHelper.interactablePiece("Security:    &7(&f" + secInfs + "&7) ",
                        InfractionTypes.getDiscription(InfractionTypes.SECURITY)),
                MsgHelper.interactablePiece(
                        MsgHelper.coloredProgress(10, secInfs, maxSec, "&7", "&4"), secInfractions)));

        src.sendMessage(MsgHelper.interactable(
                MsgHelper.interactablePiece("Sensitivity:  &7(&f" + sensInfs + "&7) ",
                        InfractionTypes.getDiscription(InfractionTypes.SENSITIVITY)),
                MsgHelper.interactablePiece(
                        MsgHelper.coloredProgress(10, sensInfs, maxSens, "&7", "&4"), sensInfractions)));
    }

    private static void sendInfractionInfoMessage(CommandSource src, String uuid, int index) {

        PlayerData playerData = PlayerDataHandler.getPlayerdata(uuid);
        Infraction infraction = playerData.getInfractions().get(index - 1);
        src.sendMessage(MsgHelper.info("&7Showing infraction &f#" + index + "&7 of " + playerData.getUsername()));
        src.sendMessage(MsgHelper.formatted("&7Infraction Type: &f" + infraction.getType()));
        src.sendMessage(MsgHelper.interactablePiece("&7Issuer: &f" +
                PlayerDataHandler.getPlayerdata(infraction.getIssuer()).getUsername(), infraction.getIssuer()));
        src.sendMessage(MsgHelper.formatted("&7Issue Date: &f" + infraction.getIssueDate()));
        src.sendMessage(MsgHelper.formatted("&7Issue Reason: &f" + infraction.getReason()));
        src.sendMessage(MsgHelper.formatted("&7Punishment: &f" + infraction.getPunishmentResult()));
    }
}