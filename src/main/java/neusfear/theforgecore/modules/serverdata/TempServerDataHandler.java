package neusfear.theforgecore.modules.serverdata;

public class TempServerDataHandler {

    private static TempServerDataHandler tempServerDataHandler;

    private boolean needsBackup;

    public TempServerDataHandler() {
        tempServerDataHandler = this;
    }

    public static TempServerDataHandler get() {
        return tempServerDataHandler;
    }

    public static void set(TempServerDataHandler tempServerDataHandler) {
        TempServerDataHandler.tempServerDataHandler = tempServerDataHandler;
    }

    public boolean isNeedsBackup() {
        return needsBackup;
    }

    public void setNeedsBackup(boolean needsBackup) {
        this.needsBackup = needsBackup;
    }
}
