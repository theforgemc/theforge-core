package neusfear.theforgecore.modules.serverdata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.helpers.ServerDataHelper;
import neusfear.theforgecore.modules.islands.IslandInfo;
import neusfear.theforgecore.modules.islands.managers.IslandFlagManager;
import neusfear.theforgecore.modules.islands.managers.IslandWorldManager;
import org.spongepowered.api.scheduler.Task;

import java.io.*;

public class IslandDataHandler {

    private IslandDataHandler islandDataHandler;
    private static IslandData islandData;

    public IslandDataHandler() {

        islandDataHandler = this;

        //Creating the islanddata folder if it doesn't exist
        File sdDir = new File(ServerDataHelper.getIslanddataDirectory() + File.separator);
        if (!sdDir.exists()) {
            createIslanddataDir(sdDir);
        }

        //Load the islanddata file or make a new one
        if (!loadIslandData()) {
            createNewIslandData();
        }

        //Init Flag Handler
        new IslandFlagManager();
    }

    public static void createIslanddataDir(File islandDataDir) {
        try{
            islandDataDir.mkdir();
            LoggerHelper.getLogger().info("Islanddata file created.");
        } catch(SecurityException se) {
            LoggerHelper.getLogger().warn("Security Exception:" + se);
        }
    }

    public void createNewIslandData() {
        IslandData islandData = new IslandData();
        storeIslandData(islandData);
        saveIslandData();
    }

    private static void save() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(islandData);
        try (FileWriter fileWriter = new FileWriter(ServerDataHelper.getIslanddataFile())) {
            fileWriter.write(jsonString);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveIslandData() {
        saveIslandData(false);
    }

    public static void saveIslandData(boolean useMainThread) {
        if (useMainThread) {
            save();
        } else {
            Task.builder().execute(IslandDataHandler::save).async().submit(TheForgeCore.getInstance());
        }
    }

    //Loads the islanddata and returns a boolean depending on it's success
    public static boolean loadIslandData() {
        File file = ServerDataHelper.getIslanddataFile();
        try {
            if (file.exists()) {
                LoggerHelper.getLogger().info("File exists");
                Gson gson = new Gson();
                BufferedReader br = new BufferedReader(new FileReader(file));

                IslandData islandData = gson.fromJson(br, IslandData.class);
                storeIslandData(islandData);
                return true;
            } else {
                LoggerHelper.getLogger().info("File does not exist");
                return false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void storeIslandData(IslandData sd) {
        islandData = sd;
    }

    public static IslandData getIslandData() {
        return islandData;
    }

    public static IslandInfo createNewIslandInfo(String biome) {
        IslandInfo islandInfo = new IslandInfo();

        islandInfo.setIndex(IslandDataHandler.getIslandData().getIslands().size());
        LoggerHelper.getLogger().info(islandInfo.getIndex() + "");
        islandInfo.setId(IslandWorldManager.getCoordForIslandNumber(islandInfo.getIndex()));
        islandInfo.setName("Island" + (islandInfo.getIndex() + 1));
        islandInfo.setDescription("Default Island Description");
        islandInfo.setMembers(islandInfo.getMembers());
        islandInfo.setCoOwners(islandInfo.getCoOwners());
        islandInfo.setIslandWarps(islandInfo.getIslandWarps());
        islandInfo.setFlags(islandInfo.getFlags());
        islandInfo.setExperience(0);
        islandInfo.setBiome(biome);

        return islandInfo;
    }
}
