package neusfear.theforgecore.modules.serverdata;

import neusfear.theforgecore.modules.islands.Island;
import neusfear.theforgecore.modules.islands.managers.IslandWorldManager;

import java.util.ArrayList;

public class IslandData {

    private ArrayList<Island> islands = new ArrayList<>();

    public IslandData() {
        super();
    }

    public IslandData(ArrayList<Island> islands1) {
        super();
        this.islands = islands1;
    }

    public ArrayList<Island> getIslands() {
        return islands;
    }

    public void setIslands(ArrayList<Island> islands) {
        this.islands = islands;
    }

    public void addNewIsland(String owner) {
        String id = IslandWorldManager.getCoordForIslandNumber(IslandDataHandler.getIslandData().getIslands().size());
        Island island = new Island(id, owner);
        islands.add(island);
    }

    public String getIslandOwner(String coordinate) {
        for (Island island : islands) {
            if (island.getId().equals(coordinate)) {
                return island.getOwner();
            }
        }
        return null;
    }

    //returns the island index; -1 if unclaimed.
    public int getIslandIndex(String coordinate) {
        int i = 0;
        for (Island island : islands) {
            if (island.getId().equals(coordinate)) {
                return i;
            }
            i++;
        }
        return -1;
    }
}
