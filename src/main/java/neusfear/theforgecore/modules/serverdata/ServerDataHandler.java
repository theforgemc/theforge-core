package neusfear.theforgecore.modules.serverdata;

import com.flowpowered.math.vector.Vector3d;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.helpers.ServerDataHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import neusfear.theforgecore.modules.warps.AdminWarpHandler;
import neusfear.theforgecore.modules.warps.Warp;
import org.spongepowered.api.scheduler.Task;

import java.io.*;

public class ServerDataHandler {

    private ServerDataHandler serverDataHandler;
    private static ServerData serverData;

    public ServerDataHandler() {

        serverDataHandler = this;

        //Creating the serverdata folder if it doesn't exist
        File sdDir = new File(String.valueOf(ServerDataHelper.getServerdataDirectory()) + File.separator);
        if (!sdDir.exists()) {
            createServerdataDir(sdDir);
        }

        //Load the serverdata file or make a new one
        if (!loadServerData()) {
            createNewServerData();
        }

        //Init the Warp Handler
        new AdminWarpHandler();
        //Init Essential Data Hander
        new EssentialDataHander();
        new TempServerDataHandler();
    }

    public static void createServerdataDir(File serverDataDir) {
        try{
            serverDataDir.mkdir();
            LoggerHelper.getLogger().info("Serverdata file created.");
        } catch(SecurityException se) {
            LoggerHelper.getLogger().warn("Security Exception:" + se);
        }
    }

    public void createNewServerData() {
        ServerData serverData = new ServerData();

        serverData.setUniquePlayers(0);
        serverData.setSpawn(new Warp(new Vector3d(0, 0, 0), "spawn", 0, 0, 0, "server"));
        serverData.setFirstspawn(new Warp(new Vector3d(0, 0, 0), "firstspawn", 0, 0, 0, "server"));
        serverData.setAdminwarps(AdminWarpHandler.getAdminwarps());
        serverData.setAdvertisedwarps(serverData.getAdvertisedwarps());
        serverData.setFeaturedwarps(serverData.getFeaturedwarps());

        storeServerData(serverData);
        saveServerData();
    }

    private static void save() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(serverData);
        try (FileWriter fileWriter = new FileWriter(ServerDataHelper.getServerdataFile())) {
            fileWriter.write(jsonString);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveServerData() {
        saveServerData(false);
    }

    public static void saveServerData(boolean useMainThread) {
        if (useMainThread) {
            save();
        } else {
            Task.builder().execute(ServerDataHandler::save).async().submit(TheForgeCore.getInstance());
        }
    }

    //Loads the serverdata and returns a boolean depending on it's success
    public static boolean loadServerData() {
        File file = ServerDataHelper.getServerdataFile();
        try {
            if (file.exists()) {
                Gson gson = new Gson();
                BufferedReader br = new BufferedReader(new FileReader(file));

                ServerData serverData = gson.fromJson(br, ServerData.class);
                storeServerData(serverData);
                return true;
            } else {
                return false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void storeServerData(ServerData sd) {
        serverData = sd;
    }

    public static ServerData getServerData() {
        return serverData;
    }
}
