package neusfear.theforgecore.modules.serverdata;

import neusfear.theforgecore.modules.warps.Warp;

import java.util.ArrayList;

public class ServerData {
    private int uniquePlayers;
    private Warp spawn;
    private Warp firstspawn;
    private ArrayList<Warp> adminwarps = new ArrayList<>();
    private ArrayList<Warp> featuredwarps = new ArrayList<>();
    private ArrayList<Warp> advertisedwarps = new ArrayList<>();

    public ServerData() {
        super();
    }

    public ServerData(int players, Warp ospawn, Warp firstSpawn, ArrayList<Warp> warps, ArrayList<Warp> fwarps, ArrayList<Warp> awarps) {
        super();
        uniquePlayers = players;
        spawn = ospawn;
        firstspawn = firstSpawn;
        adminwarps = warps;
        featuredwarps = fwarps;
        advertisedwarps = awarps;
    }

    public int getUniquePlayers() {
        return uniquePlayers;
    }

    public void setUniquePlayers(int uniquePlayers) {
        this.uniquePlayers = uniquePlayers;
    }

    public int incrementUniquePlayers() {
        setUniquePlayers(getUniquePlayers() + 1);
        return getUniquePlayers();
    }

    public Warp getFirstspawn() {
        return firstspawn;
    }

    public void setFirstspawn(Warp firstspawn) {
        this.firstspawn = firstspawn;
    }

    public ArrayList<Warp> getAdminWarps() {
        return adminwarps;
    }

    public void setAdminwarps(ArrayList<Warp> warps) {
        this.adminwarps = warps;
    }

    public ArrayList<Warp> getFeaturedwarps() {
        return featuredwarps;
    }

    public void setFeaturedwarps(ArrayList<Warp> featuredwarps) {
        this.featuredwarps = featuredwarps;
    }

    public void addFeaturedWarp(Warp warp) {
        this.featuredwarps.add(warp);
    }

    public void removeFeaturedWarp(Warp warp) {
        this.featuredwarps.remove(warp);
    }

    public ArrayList<Warp> getAdvertisedwarps() {
        return advertisedwarps;
    }

    public void setAdvertisedwarps(ArrayList<Warp> advertisedwarps) {
        this.advertisedwarps = advertisedwarps;
    }

    public void addAdvertisedwarp(Warp warp) {
        this.advertisedwarps.add(warp);
    }

    public void removeAdvertisedwarp(Warp warp) {
        this.advertisedwarps.remove(warp);
    }

    public Warp getSpawn() {
        return spawn;
    }

    public void setSpawn(Warp spawn) {
        this.spawn = spawn;
    }
}
