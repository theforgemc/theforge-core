package neusfear.theforgecore.modules.essential;

import neusfear.theforgecore.helpers.util.Time;
import org.spongepowered.api.entity.living.player.Player;

import java.sql.Timestamp;

public class TeleportRequest {

    private Player target;
    private Player player;
    private Timestamp time;

    public TeleportRequest(Player player, Player target) {
        this.player = player;
        this.target = target;
        this.time = Time.getNow();
    }

    public Player getTarget() {
        return target;
    }

    public Player getPlayer() {
        return player;
    }

    public Timestamp getTime() {
        return time;
    }
}
