package neusfear.theforgecore.modules.essential.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class RulesCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Accepts teleport requests."))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        src.sendMessage(MsgHelper.info("Rules:"));
                        src.sendMessage(MsgHelper.formatted("&f1. &7No advertising."));
                        src.sendMessage(MsgHelper.formatted("&f2. &7Keep chat clean (don't spam or be annoying)."));
                        src.sendMessage(MsgHelper.formatted("&f3. &7No cheating (no unauthorised client modifications)."));
                        src.sendMessage(MsgHelper.formatted("&f4. &7Be respectful to others, including staff."));
                        src.sendMessage(MsgHelper.formatted("&f5. &7Don't threaten the security of the server."));
                        src.sendMessage(MsgHelper.formatted("&f6. &7Don't be insensitive to others (keep it PG)."));
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "rules");
    }
}
