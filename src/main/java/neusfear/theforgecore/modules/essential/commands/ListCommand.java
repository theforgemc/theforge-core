package neusfear.theforgecore.modules.essential.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import java.util.Collection;

public class ListCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Lists online Players"))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Collection<Player> players = Sponge.getGame().getServer().getOnlinePlayers();
                        String playerString = " ";
                        for (Player player : players) {
                            playerString = playerString + ", " + player.getName();
                        }
                        src.sendMessage(MsgHelper.info("Online Players (&f" + players.size() + "&7) : &f" + playerString.substring(2)));
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "list");
    }
}
