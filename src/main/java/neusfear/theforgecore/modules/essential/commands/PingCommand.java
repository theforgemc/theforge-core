package neusfear.theforgecore.modules.essential.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class PingCommand {

    public static void registerCommand() {

        CommandSpec command = CommandSpec.builder()
                .description(Text.of("This command gives a useful ping message."))
                .executor((CommandSource src, CommandContext args) -> {

                    if(src instanceof Player) {
                        src.sendMessage(MsgHelper.info("Your current ping is: " + ((Player) src).getConnection().getLatency()));
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only command."));
                    }
                    return CommandResult.success();

                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();

        cmdManager.register(TheForgeCore.getInstance(), command, "ping");
    }
}
