package neusfear.theforgecore.modules.essential.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class TeleportAcceptCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Accepts teleport requests."))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        if (EssentialDataHander.getEssentialDataHander().getRequestFor(player) != null) {
                            Player sender = EssentialDataHander.getEssentialDataHander().getRequestFor(player).getPlayer();
                            if (sender.isOnline()) {
                                sender.setLocation(player.getLocation());
                                EssentialDataHander.getEssentialDataHander().removeTpaFor(player);
                                sender.sendMessage(MsgHelper.info("Teleport Accepted."));
                                src.sendMessage(MsgHelper.info("Request accepted."));
                            } else {
                                src.sendMessage(MsgHelper.error("The request you are trying to accept's sender is not online."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("You have no pending requests"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "teleportaccept", "tpaccept");
    }
}
