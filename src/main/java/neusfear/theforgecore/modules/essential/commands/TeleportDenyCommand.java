package neusfear.theforgecore.modules.essential.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class TeleportDenyCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Accepts teleport requests."))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        if (EssentialDataHander.getEssentialDataHander().getRequestFor(player) != null) {
                            EssentialDataHander.getEssentialDataHander().getRequestFor(player).getPlayer().sendMessage(MsgHelper.error("Teleport request denied."));
                            EssentialDataHander.getEssentialDataHander().removeTpaFor(player);
                            src.sendMessage(MsgHelper.info("Request denied successfully."));
                        } else {
                            src.sendMessage(MsgHelper.error("You have no pending requests"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "teleportdeny", "tpdeny", "tpd");
    }
}
