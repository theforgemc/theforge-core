package neusfear.theforgecore.modules.essential.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class TeleportRequestCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Requests a to teleport to the specified player."))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.player(Text.of("target")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        Optional<Player> argTarget = args.getOne("target");
                        if (argTarget.isPresent()) {
                            Player target = argTarget.get();
                            if (target.isOnline()) {
                                EssentialDataHander.getEssentialDataHander().setTpaFor(player, target);
                                src.sendMessage(MsgHelper.info("Teleport request sent."));
                                target.sendMessage(MsgHelper.info(player.getName() + " is requesting to teleport to you. Accept it with /tpaccept or deny it with /tpdeny"));
                            } else {
                                src.sendMessage(MsgHelper.error("That player is not online."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("Specify a present player."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "teleportrequest", "teleportask", "tpa", "tpr");
    }
}
