package neusfear.theforgecore.modules.essential.commands;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class SeenCommand {

    public static void registerCommand() {
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Accepts teleport requests."))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("player")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        String target = args.<String>getOne("player").get();
                        if (PlayerDataHandler.hasUserJoined(target)) {
                            if (PlayerDataHandler.getUserFromName(target).get().isOnline()) {
                                src.sendMessage(MsgHelper.info(target + " is online."));
                            } else {
                                src.sendMessage(MsgHelper.info(target + " has been offline for " +
                                        Time.getTimeFromMin((int)((Time.getNow().getTime() - Time.getTimestampFrom(PlayerDataHandler.getPlayerdataFromUsername(target).getSeen()).getTime())/60000)
                                        )));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("That player has never joined before"));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("That is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "seen", "lastonline", "isonline", "online");
    }
}
