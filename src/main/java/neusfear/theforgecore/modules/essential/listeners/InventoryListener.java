package neusfear.theforgecore.modules.essential.listeners;

import com.google.common.collect.Maps;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.filter.type.Exclude;
import org.spongepowered.api.event.item.inventory.InteractInventoryEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.inventory.Container;
import org.spongepowered.api.item.inventory.Inventory;

import java.util.Map;
import java.util.UUID;

public class InventoryListener {

    private static Map<UUID, Inventory> preventModify = Maps.newHashMap();

    public static void addEntry(UUID uuid, Container inventory) {
        preventModify.put(uuid, inventory);
    }

    @Listener
    @Exclude({InteractInventoryEvent.Open.class, InteractInventoryEvent.Close.class})
    public void onInventoryChange(InteractInventoryEvent event, @First Player player, @Getter("getTargetInventory") Container targetInventory) {

        if (preventModify.get(player.getUniqueId()) == targetInventory) {
            event.setCancelled(true);
            event.getCursorTransaction().setValid(false);
        }

    }

    @Listener(order = Order.POST)
    public void onInventoryClose(InteractInventoryEvent.Close event, @First Player player, @Getter("getTargetInventory") Container targetInventory) {
        if (preventModify.get(player.getUniqueId()) == targetInventory) {
            preventModify.remove(player.getUniqueId());
        }
    }

    @Listener
    public void onLogout(ClientConnectionEvent.Disconnect event, @Root Player player) {
        preventModify.remove(player.getUniqueId());
    }

}
