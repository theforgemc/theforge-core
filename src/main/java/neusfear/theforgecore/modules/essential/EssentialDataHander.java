package neusfear.theforgecore.modules.essential;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.util.Time;
import org.apache.commons.lang3.time.DateUtils;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class EssentialDataHander {

    private static EssentialDataHander essentialDataHander;
    private ArrayList<Player> socialSpy = new ArrayList<>();
    private ArrayList<Player> commandSpy = new ArrayList<>();
    private HashMap<Player, Player> replyDict = new HashMap<>();
    private ArrayList<TeleportRequest> tpaDict = new ArrayList<>();

    public EssentialDataHander() {
        essentialDataHander = this;

        Task tickTpa = Task.builder().execute(() -> tickRequests()).async()
                .delay(1, TimeUnit.MINUTES).interval(1, TimeUnit.MINUTES)
                .name("TheForgeCore - Tick TPA Requests").submit(TheForgeCore.getInstance());
    }

    public static EssentialDataHander getEssentialDataHander() {
        return essentialDataHander;
    }

    public ArrayList<Player> getSocialSpies() {
        return socialSpy;
    }

    public void addSocialSpy(Player socialSpy) {
        this.socialSpy.add(socialSpy);
    }

    public void removeSocialSpy(Player socialSpy) {
        this.socialSpy.remove(socialSpy);
    }

    public ArrayList<Player> getCommandSpies() {
        return commandSpy;
    }

    public void addCommandSpy(Player commandSpy) {
        this.commandSpy.add(commandSpy);
    }

    public void removeCommandSpy(Player commandSpy) {
        this.commandSpy.remove(commandSpy);
    }

    public Player getRecipientForSender(Player player) {
        if (replyDict.containsKey(player)) {
            return replyDict.get(player);
        }
        return null;
    }

    public void setRecentReply(Player sender, Player recipient) {
        this.replyDict.put(sender, recipient);
    }

    public TeleportRequest getRequestFor(Player player) {
        for (TeleportRequest tpr : tpaDict) {
            if (tpr.getTarget().equals(player)) {
                return tpr;
            }
        }
        return null;
    }

    public TeleportRequest getRequestFrom(Player player) {
        for (TeleportRequest tpr : tpaDict) {
            if (tpr.getPlayer().equals(player)) {
                return tpr;
            }
        }
        return null;
    }

    public void setTpaFor(Player asker, Player target) {
        if (getRequestFrom(asker) != null) {
            int index = 0;
            for (TeleportRequest tpr : tpaDict) {
                if (tpr.getPlayer().equals(asker)) {
                    tpaDict.set(index, new TeleportRequest(asker, target));
                    return;
                }
                index++;
            }
        }
        this.tpaDict.add(new TeleportRequest(asker, target));
    }

    public void removeTpaFor(Player player) {
        TeleportRequest tpreq = null;
        for (TeleportRequest tpr : tpaDict) {
            if (tpr.getTarget().equals(player)) {
                tpreq = tpr;
            }
        }
        tpaDict.remove(tpreq);
    }

    public void removeTpaFrom(Player player) {
        TeleportRequest tpreq = null;
        for (TeleportRequest tpr : tpaDict) {
            if (tpr.getPlayer().equals(player)) {
                tpreq = tpr;
            }
        }
        tpaDict.remove(tpreq);
    }

    public void tickRequests() {
        ArrayList<TeleportRequest> requests = new ArrayList<>();
        tpaDict.forEach(tpr -> {
            if (Time.getNow().after(DateUtils.addMinutes(tpr.getTime(), 2))) {
                requests.add(tpr);
            } else {
                DateUtils.addMinutes(tpr.getTime(), -1);
            }
        });
        tpaDict.removeAll(requests);
    }
}
