package neusfear.theforgecore.modules.votes;

import com.vexsoftware.votifier.sponge.event.VotifierEvent;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;

public class VoteHandler {

    @Listener
    public void voteEvent(VotifierEvent event) {
        if (PlayerDataHandler.hasUserJoined(event.getVote().getUsername())) {
            PlayerData playerData = PlayerDataHandler.getPlayerdataFromUsername(event.getVote().getUsername());
            applyConsecVote(playerData);
            playerData.addVote();
            playerData.setLastVote(Time.getNow().toString());
            VoteRewards.issueReward(playerData);
            Sponge.getServer().getBroadcastChannel().send(MsgHelper.info("&f" + event.getVote().getUsername() + " &7has now voted on &f" + event.getVote().getServiceName() + " " + playerData.getConsecutiveVotes() + " &7times in a row!"));
        }
    }

    public static void applyConsecVote(PlayerData playerData) {
        switch (testConsecVote(playerData)) {
            case -1:
                playerData.setConsecutiveVotes(1);
            case 1:
                playerData.addConsecVote();
        }
    }

    public static void tryResetConsec(PlayerData playerData) {
        if (testConsecVote(playerData) == -1) { playerData.setConsecutiveVotes(0); }
    }

    //-1 = too long, 0 = not long enough, 1 = add vote
    public static int testConsecVote(PlayerData playerData) {
        //If this is a consecutive vote (between 22 and 26 hours)
        if (Time.getTimestampFrom(playerData.getLastVote()).getTime() <= 93600000) {
            return 1;
        }
        //If they've been offline too long for a consec vote
        return -1;
    }
}
