package neusfear.theforgecore.modules.votes;

public class VoteReward {

    private double money;
    private int XP;

    public VoteReward(double money, int exp) {
        this.money = money;
        this.XP = exp;
    }

    public double getMoney() {
        return money;
    }

    public int getXP() {
        return XP;
    }
}
