package neusfear.theforgecore.modules.reports.commands;

import net.dv8tion.jda.core.entities.MessageEmbed;
import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.modules.discord.Bot;
import neusfear.theforgecore.modules.discord.helpers.BotHelper;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import java.util.Optional;

public class ReportCommand {

    public static void registerCommand() {

        //report player
        CommandSpec playerCommand = CommandSpec.builder()
                .description(Text.of("Reports a player to staff"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.player(Text.of("player"))),
                        GenericArguments.optionalWeak(GenericArguments.remainingJoinedStrings(Text.of("reason")))
                )
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        if (args.<Player>getOne("player").get().toString().equals(player.getName())) {
                            Optional<String> reason = args.<String>getOne("reason");
                            if (reason.isPresent()) {
                                MessageEmbed embed = BotHelper.makePlayerReportEmbed(src.getName(), args.<Player>getOne("player").get().getName(), reason.get());
                                Bot.getJda().getTextChannelById("537769826282635266").sendMessage(embed).queue();
                                src.sendMessage(MsgHelper.info("Your report was sent to staff, they will investigate as they see fit."));
                            } else {
                                src.sendMessage(MsgHelper.error("You must give a reason for the report, a short description of the issue helps best."));
                            }
                        } else {
                            src.sendMessage(MsgHelper.error("You can't report yourself."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        //report bug
        CommandSpec bugCommand = CommandSpec.builder()
                .description(Text.of("Reports a bug to the staff"))
                .arguments(GenericArguments.optionalWeak(GenericArguments.remainingJoinedStrings(Text.of("description"))))
                .executor((CommandSource src, CommandContext args) -> {
                    if (src instanceof Player) {
                        Optional<String> description = args.<String>getOne("description");
                        if (description.isPresent()) {
                            MessageEmbed embed = BotHelper.makeBugReportEmbed(src.getName(), description.get());
                            Bot.getJda().getTextChannelById("556558055584825365").sendMessage(embed).queue();
                            src.sendMessage(MsgHelper.info("Your report was sent to staff, thanks for bettering the server."));
                        } else {
                            src.sendMessage(MsgHelper.error("Please give a brief description of the bug."));
                        }
                    } else {
                        src.sendMessage(MsgHelper.error("This is a player only Command"));
                    }
                    return CommandResult.success();
                })
                .build();

        //Parent Command
        CommandSpec command = CommandSpec.builder()
                .description(Text.of("Report things to staff"))
                .child(bugCommand, "bug")
                .child(playerCommand, "player")
                .build();

        CommandManager cmdManager = Sponge.getCommandManager();
        cmdManager.register(TheForgeCore.getInstance(), command, "report");
    }
}
