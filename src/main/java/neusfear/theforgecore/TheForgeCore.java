package neusfear.theforgecore;

import com.google.inject.Inject;
import net.dv8tion.jda.core.entities.Game;
import neusfear.theforgecore.config.TheForgeCoreConfig;
import neusfear.theforgecore.handlers.BackupHandler;
import neusfear.theforgecore.handlers.CommandHandler;
import neusfear.theforgecore.handlers.EventHandler;
import neusfear.theforgecore.helpers.ConfigHelper;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.modules.discord.Bot;
import neusfear.theforgecore.modules.discord.chatlink.DiscordChatLink;
import neusfear.theforgecore.modules.economy.PriceReference;
import neusfear.theforgecore.modules.economy.TransactionDataHandler;
import neusfear.theforgecore.modules.islands.GenerationEvents;
import neusfear.theforgecore.modules.islands.IslandWorldModifier;
import neusfear.theforgecore.modules.islands.ResourceWorldHandler;
import neusfear.theforgecore.modules.islands.managers.IslandEventManager;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.playtime.AFK;
import neusfear.theforgecore.modules.serverdata.IslandDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.votes.VoteHandler;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameRegistryEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePostInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.world.gen.WorldGeneratorModifier;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@Plugin(
        id = "theforgecore",
        name = "TheForge Core",
        description = "The core aspects of TheForge server",
        url = "https://gitlab.com/theforgemc/theforge-core",
        authors = {
                "NeusFear"
        }
)
public class TheForgeCore {

    private static TheForgeCore instance;

    public TheForgeCore() {
        instance = this;
    }

    public static TheForgeCore getInstance() {
        return instance;
    }

    @Inject
    private Logger logger;

    private CommentedConfigurationNode config = null;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private File configFile;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private Path configPath;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configLoader;



    @Listener
    public void onPreInitialization(GamePreInitializationEvent event) {
        new LoggerHelper(logger);
        new ConfigHelper(config, configFile, configPath, configLoader);
        new TheForgeCoreConfig();

        //Start the bot and log it in, then register the listeners
        Bot.startBot();
        Bot.getJda().getPresence().setGame(Game.of(Game.GameType.DEFAULT, "Server Starting..."));
        DiscordChatLink.positiveNegativeMessages(true, "Server Starting...");
    }

    @Listener
    public void onInitialization(GameInitializationEvent event) {

        Bot.getJda().getPresence().setGame(Game.of(Game.GameType.DEFAULT, "Server Initializing..."));
        DiscordChatLink.positiveNegativeMessages(true, "Server Initializing...");

        //Registers Event handlers to avoid clutter in this main plugin class
        //This needs to happen before other things in this class are registered.
        Sponge.getEventManager().registerListeners(instance, new EventHandler());
        //Keys.IS_SNEAKING.registerEvent(Player.class, SignHelper::changeSigns);
        Sponge.getEventManager().registerListeners(instance, new GenerationEvents());
        Sponge.getEventManager().registerListeners(instance, new AFK());
        Sponge.getEventManager().registerListeners(instance, new VoteHandler());


        //Initialise Commands
        new CommandHandler();

        //Create plugin config root
        File sdDir = new File(getPlugindataDirectory() + File.separator);
        if (!sdDir.exists()) {
            createPlugindataDir(sdDir);
        }

        //Init DataHandlers
        new ServerDataHandler();
        new PlayerDataHandler();
        new IslandDataHandler();

        //Init Backup Handler
        new BackupHandler();

        //Start Backups
        Sponge.getEventManager().registerListeners(instance, new IslandEventManager());

        //Load prices
        new PriceReference();
        new TransactionDataHandler();

        //Announce that the Plugin was Started.
        LoggerHelper.getLogger().info("Plugin Initialized");
    }

    @Listener
    public void onGameRegistry(GameRegistryEvent.Register<WorldGeneratorModifier> event) {
        event.register(new IslandWorldModifier());
        LoggerHelper.getLogger().info("Registered WorldModifier: If your world is not generating the islands world generator, you need to add theforge:islands to the world.conf file of the default world in the sponge config directory.");
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        //The server has started
        LoggerHelper.getLogger().info("Loaded version: " + Reference.version);
        ResourceWorldHandler.createResourceWorld();
        Bot.getJda().getPresence().setGame(Game.of(Game.GameType.DEFAULT, "Server Online."));
        DiscordChatLink.positiveNegativeMessages(true, "Server Online.");
    }

    @Listener
    public void onPostInit(GamePostInitializationEvent event)
    {
        Keys.FOOD_LEVEL.registerEvent(Player.class, IslandEventManager::onHungerLoss);
    }

    public static Path getPlugindataDirectory() {
        String filePath = ConfigHelper.getConfigPath().toString().substring(0, (ConfigHelper.getConfigPath().toString().length() - 17));
        return Paths.get(filePath + "theforge");
    }

    public void createPlugindataDir(File pdDir) {
        try{
            pdDir.mkdir();
            LoggerHelper.getLogger().info("plugindata file created.");
        } catch(SecurityException se) {
            LoggerHelper.getLogger().warn("Security Exception:" + se);
        }
    }
}
