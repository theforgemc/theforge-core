package neusfear.theforgecore.handlers;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.ConfigHelper;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.helpers.PlayerDataHelper;
import neusfear.theforgecore.helpers.ServerDataHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.helpers.util.ZipHelper;
import neusfear.theforgecore.modules.economy.TransactionDataHandler;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.IslandDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.serverdata.TempServerDataHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.scheduler.Task;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BackupHandler {

    private int backupDelay = ConfigHelper.getConfig().getNode("data", "backupdelay").getInt();

    public BackupHandler() {

        //Creating the serverdata folder if it doesn't exist
        File bdDir = new File(String.valueOf(ServerDataHelper.getBackupDirectory()) + File.separator);
        if (!bdDir.exists()) {
            createBackupDir(bdDir);
        }

        //Auto Backup data every backupDelay minutes if it needs it
        Task backupTask = Task.builder().execute(() -> {
            if (TempServerDataHandler.get().isNeedsBackup()) {
                if (Sponge.getServer().getOnlinePlayers().size() < 1) {
                    TempServerDataHandler.get().setNeedsBackup(false);
                    LoggerHelper.getLogger().info("Creating Zip Backup");
                    createBackup();
                } else {
                    saveAllData(false);
                }
            }})
                .delay(backupDelay, TimeUnit.MINUTES).interval(backupDelay, TimeUnit.MINUTES)
                .name("TheForgeCore - Back stuff up").async().submit(TheForgeCore.getInstance());

    }

    public static void createBackup() {
        try {
            saveAllData(true);
            ZipHelper.zipDirs(ServerDataHelper.getBackupDirectory().toString(), Time.getFileTime() + ".zip", false, ServerDataHelper.getServerdataDirectory().toString(), PlayerDataHelper.getPlayerdataDirectory().toString());
            LoggerHelper.getLogger().info("Backup created: " + Time.getNow().toString() + ".zip");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createBackupDir(File backupDir) {
        LoggerHelper.getLogger().info("Creating backups directory.");
        if (!backupDir.exists()) {
            backupDir.mkdirs();
        }
    }

    private static void  saveAllData(boolean useMainThread) {
        PlayerDataHandler.saveAllPlayerdata(useMainThread);
        ServerDataHandler.saveServerData(useMainThread);
        IslandDataHandler.saveIslandData(useMainThread);
        TransactionDataHandler.saveTransactionData(useMainThread);
    }
}
