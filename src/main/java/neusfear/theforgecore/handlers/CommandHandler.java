package neusfear.theforgecore.handlers;

import neusfear.theforgecore.TheForgeCore;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.modules.chat.commands.MessageCommand;
import neusfear.theforgecore.modules.chat.commands.ReplyCommand;
import neusfear.theforgecore.modules.economy.commands.*;
import neusfear.theforgecore.modules.essential.commands.*;
import neusfear.theforgecore.modules.help.commands.HelpCommand;
import neusfear.theforgecore.modules.homes.commands.DelHomeCommand;
import neusfear.theforgecore.modules.homes.commands.HomeCommand;
import neusfear.theforgecore.modules.homes.commands.HomesCommand;
import neusfear.theforgecore.modules.homes.commands.SetHomeCommand;
import neusfear.theforgecore.modules.infractions.commands.InfractionsCommand;
import neusfear.theforgecore.modules.islands.commands.IslandCommand;
import neusfear.theforgecore.modules.islands.commands.ResourceCommand;
import neusfear.theforgecore.modules.permissions.commands.RankCommand;
import neusfear.theforgecore.modules.playerdata.commands.PlayerDataCommand;
import neusfear.theforgecore.modules.playtime.commands.AfkCommand;
import neusfear.theforgecore.modules.referrals.commands.ReferralCommand;
import neusfear.theforgecore.modules.referrals.commands.ReferralsCommand;
import neusfear.theforgecore.modules.reports.commands.ReportCommand;
import neusfear.theforgecore.modules.spawn.commands.FirstSpawnCommand;
import neusfear.theforgecore.modules.spawn.commands.SpawnCommand;
import neusfear.theforgecore.modules.staff.commands.StaffCommand;
import neusfear.theforgecore.modules.warps.commands.*;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;

public class CommandHandler {

    CommandManager cmdService = Sponge.getCommandManager();
    private static TheForgeCore plugin = TheForgeCore.getInstance();

    public CommandHandler() {
        registerCommands();
    }

    public void registerCommands() {
        LoggerHelper.getLogger().info("Registering Commands");

        //Chat Commands
        MessageCommand.registerCommand();
        ReplyCommand.registerCommand();

        //Economy Commands
        BalanceCommand.registerCommand();
        DepositCommand.registerCommand();
        PayCommand.registerCommand();
        WithdrawCommand.registerCommand();

        //Essential Commands
        TeleportRequestCommand.registerCommand();
        TeleportAcceptCommand.registerCommand();
        TeleportDenyCommand.registerCommand();
        TeleportCancelCommand.registerCommand();
        RulesCommand.registerCommand();
        SeenCommand.registerCommand();
        ListCommand.registerCommand();

        //Home Commands
        HomesCommand.registerCommand();
        HomeCommand.registerCommand();
        SetHomeCommand.registerCommand();
        DelHomeCommand.registerCommand();

        //Infraction Commands
        InfractionsCommand.registerCommand();

        //Island Commands
        IslandCommand.registerCommand();
        ResourceCommand.registerCommand();

        //PlayerData Commands
        PlayerDataCommand.registerCommand();

        //Playtime Commands
        AfkCommand.registerCommand();

        //Referrals
        ReferralCommand.registerCommand();
        ReferralsCommand.registerCommand();

        //Report Commands
        ReportCommand.registerCommand();

        //Spawn Commands
        SpawnCommand.registerCommand();
        FirstSpawnCommand.registerCommand();

        //Staff Command
        StaffCommand.registerCommand();

        //Utilities
        PingCommand.registerCommand();
        cmdService.register(plugin, new RankCommand(), "ranks", "rank");

        //Warp Commands
        WarpsCommand.registerCommand();
        WarpCommand.registerCommand();
        SetWarpCommand.registerCommand();
        DelWarpCommand.registerCommand();
        AdvertiseWarpCommand.registerCommand();
        AdvertisedWarpsCommand.registerCommand();
        FeaturedWarpsCommand.registerCommand();

        //HelpCommand MUST BE LAST
        HelpCommand.registerCommand();
    }
}
