package neusfear.theforgecore.handlers;

import com.flowpowered.math.vector.Vector3i;
import neusfear.theforgecore.helpers.LoggerHelper;
import neusfear.theforgecore.helpers.MsgHelper;
import neusfear.theforgecore.helpers.util.Time;
import neusfear.theforgecore.modules.chat.ConnectionMessages;
import neusfear.theforgecore.modules.dailyrewards.DailyRewards;
import neusfear.theforgecore.modules.discord.chatlink.DiscordChatLink;
import neusfear.theforgecore.modules.economy.helpers.SignHelper;
import neusfear.theforgecore.modules.essential.EssentialDataHander;
import neusfear.theforgecore.modules.infractions.helpers.InfractionHelper;
import neusfear.theforgecore.modules.islands.managers.IslandWorldManager;
import neusfear.theforgecore.modules.playerdata.PlayerData;
import neusfear.theforgecore.modules.playerdata.PlayerDataHandler;
import neusfear.theforgecore.modules.serverdata.ServerDataHandler;
import neusfear.theforgecore.modules.serverdata.TempServerDataHandler;
import neusfear.theforgecore.modules.votes.VoteHandler;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.world.World;

public class EventHandler {

    @Listener
    public void onJoin(ClientConnectionEvent.Join event) {
        TempServerDataHandler.get().setNeedsBackup(true);
        String uuid = event.getTargetEntity().getUniqueId().toString();
        //Broadcast the player's connection
        ConnectionMessages.playerJoin(event);
        //Message the player directly when they join
        ConnectionMessages.welcomePlayerBack(event.getTargetEntity());
        //Broadcast the player's first join
        if (!event.getTargetEntity().hasPlayedBefore()) {
            ConnectionMessages.welcomePlayer(event);

            World world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName()).orElseThrow(RuntimeException::new);
            Vector3i location = ServerDataHandler.getServerData().getFirstspawn();
            if (location.getY() > 0) {
                event.getTargetEntity().setLocation(world.getLocation(location));
            } else {
                event.getTargetEntity().setLocation(event.getTargetEntity().getWorld().getSpawnLocation());
            }
        }

        //Make sure their username is up-to-date
        PlayerData playerData = PlayerDataHandler.getPlayerdata(uuid);
        playerData.setUsername(event.getTargetEntity().getName());
        playerData.setOnline(true);
        playerData.setAfkLevel(0);
        VoteHandler.tryResetConsec(playerData);
        //If this is a consecutive join (between 22 and 26 hours)
        if (Time.getTimestampFrom(playerData.getLastJoinReward()).getTime() >= 79200000 && Time.getTimestampFrom(playerData.getLastJoinReward()).getTime() <= 93600000) {
            playerData.addConsecJoin();
        }
        //If they've been offline too long for a consec join
        if (Time.getTimestampFrom(playerData.getLastJoinReward()).getTime() > 93600000) {
            playerData.resetConsecJoins();
        }
        //Give them the stuff they deserve for joining x days in a row
        DailyRewards.issueReward(playerData);
        event.getTargetEntity().sendMessage(MsgHelper.info("&fDaily Rewards: &7you have joined &f" + playerData.getConsecJoins() + " &7days in a row. \n&f$" +
                DailyRewards.getReward(playerData.getConsecJoins()).getMoney() + " &7and &f" + DailyRewards.getReward(playerData.getConsecJoins()).getXP() + "XP&7 has been added to your playerdata"));
        PlayerDataHandler.savePlayerdata(uuid);
        DiscordChatLink.positiveNegativeMessages(true, playerData.getUsername() + " Joined the server");
    }

    @Listener
    public void onLeave(ClientConnectionEvent.Disconnect event) {
        //Broadcast the player's departure
        ConnectionMessages.playerLeave(event);

        PlayerData playerData = PlayerDataHandler.getPlayerdata(event.getTargetEntity().getUniqueId().toString());
        playerData.setOnline(false);
        playerData.setAfkLevel(0);
        playerData.setSeen(Time.getNow().toString());
        PlayerDataHandler.savePlayerdata(event.getTargetEntity().getUniqueId().toString());
        DiscordChatLink.positiveNegativeMessages(false, playerData.getUsername() + " Left the server");
    }

    @Listener
    public void onJoinAuth(ClientConnectionEvent.Auth event) {
        //If the profile has a name (should have one most of the time, but ya never know)
        String uuid = event.getProfile().getUniqueId().toString();
        if(!event.getProfile().getName().isPresent()) {
            event.setCancelled(true);
            return;
        }
        if (!PlayerDataHandler.hasJoined(uuid)) {
            PlayerDataHandler.createNewPlayerdata(uuid);
        }
        PlayerData playerData = PlayerDataHandler.getPlayerdata(event.getProfile().getUniqueId().toString());
        if (!playerData.getBanStatus().equals("false") || playerData.getBanStatus().equals("banned")) {
            if (Time.getTimestampFrom(playerData.getBanStatus()).after(Time.getNow())) {
                event.setCancelled(true);
                event.setMessage(MsgHelper.formatted("&7You are temporarily banned until: &f" + playerData.getBanStatus()));
                return;
            } else {
                playerData.setBanStatus("false");
            }
        }
    }

    @Listener
    public void onServerStop(GameStoppingServerEvent event) {
        //Just in case the player leave event doesn't get called we loop through loaded players.
        LoggerHelper.getLogger().info("Saving playerdata...");
        DiscordChatLink.positiveNegativeMessages(false, "Backing up server...");
        for (PlayerData playerData : PlayerDataHandler.getLoadedPlayers()) {
            playerData.setOnline(false);
            playerData.setAfkLevel(0);
        }
        BackupHandler.createBackup();
        DiscordChatLink.positiveNegativeMessages(false, "Server Stopped.");
    }

    @Listener
    public void onChat(MessageChannelEvent.Chat event) {
        event.setCancelled(true);

        String message = event.getRawMessage().toPlain();

        Player player = (Player) event.getSource();
        PlayerData playerData = PlayerDataHandler.getPlayerdata(player.getUniqueId().toString());
        if (!playerData.getMuted().equals("false")) {
            if (Time.getTimestampFrom(playerData.getMuted()).after(Time.getNow())) {
                player.sendMessage(MsgHelper.error("You're currently muted. &4/inf " + playerData.getUsername() + " " + InfractionHelper.getLatestInfractionNum(playerData.getUuid())));
                return;
            } else {
                playerData.setMuted("false");
            }
        }
        Sponge.getServer().getBroadcastChannel().send(MsgHelper.chat(playerData.getPermissionLevel(), playerData.getUsername(), message));
        DiscordChatLink.sendToDiscord(playerData.getUsername(), message, false);
        playerData.setAfkLevel(0);
    }

    @Listener
    public void onCommand(SendCommandEvent event) {
        if (event.getSource() instanceof Player) {
            Player source = (Player) event.getSource();
            for (Player player : EssentialDataHander.getEssentialDataHander().getCommandSpies()) {
                player.sendMessage(MsgHelper.formatted("&8[&5CS&8]: " + source.getName() + " -> &7/" + event.getCommand() + " &8" + event.getArguments()));
            }
            if (event.getCommand().equalsIgnoreCase("me") || event.getCommand().equalsIgnoreCase("tell")) {
                event.setCancelled(true);
                source.sendMessage(MsgHelper.error("That command has been disabled."));
            }
        }
    }

    @Listener
    public void onRightClickOnSign(InteractBlockEvent.Secondary event, @First Player player) {
        if (event.getTargetBlock().getLocation().isPresent() &&
        event.getTargetBlock().getLocation().get().getTileEntity().isPresent() &&
        event.getTargetBlock().getLocation().get().getTileEntity().get().getType().equals(TileEntityTypes.SIGN)) {
            if (IslandWorldManager.getIslandInfoOf(event.getTargetBlock().getLocation().get().getBlockPosition().getX(),
                    event.getTargetBlock().getLocation().get().getBlockPosition().getZ()).getId().equals("(0,0)")) {
                Sign sign = (Sign) event.getTargetBlock().getLocation().get().getTileEntity().get();
                if (sign.getSignData().lines().get(0).toPlain().equalsIgnoreCase("[shop]")) {
                    if (sign.getSignData().lines().get(2).toPlain().isEmpty()) {
                        if (PlayerDataHandler.getPlayerdata(player.getUniqueId().toString()).getPermissionLevel() >= 9) {
                            if (sign.getSignData().lines().get(1).toPlain().equalsIgnoreCase("buy")) {
                                event.setCancelled(true);
                                SignHelper.createSign(event.getTargetBlock().getLocation().get(), player, true, false);
                                return;
                            }
                            if (sign.getSignData().lines().get(1).toPlain().equalsIgnoreCase("sell")) {
                                event.setCancelled(true);
                                SignHelper.createSign(event.getTargetBlock().getLocation().get(), player, false, true);
                                return;
                            }
                            if (sign.getSignData().lines().get(1).toPlain().equalsIgnoreCase("buy/sell")) {
                                event.setCancelled(true);
                                SignHelper.createSign(event.getTargetBlock().getLocation().get(), player, true, true);
                                return;
                            }
                            event.setCancelled(true);
                            if (!sign.getSignData().lines().get(2).isEmpty() && !sign.getSignData().lines().get(3).isEmpty()) {
                                player.sendMessage(MsgHelper.error("You need to specify buy, sell, or buy/sell on the second line."));
                            }
                        }
                    }
                    if (player.gameMode().get().equals(GameModes.SURVIVAL)) {
                        event.setCancelled(true);
                        SignHelper.process(player, sign, true);
                    }
                }
            }
        }
    }

    @Listener
    public void onLeftClickOnSign(InteractBlockEvent.Primary event, @First Player player) {
        if (event.getTargetBlock().getLocation().isPresent() &&
                event.getTargetBlock().getLocation().get().getTileEntity().isPresent() &&
                event.getTargetBlock().getLocation().get().getTileEntity().get().getType().equals(TileEntityTypes.SIGN)) {
            if (player.gameMode().get().equals(GameModes.SURVIVAL)) {
                Sign sign = (Sign) event.getTargetBlock().getLocation().get().getTileEntity().get();
                if (sign.getSignData().lines().get(0).toPlain().equalsIgnoreCase("[shop]")) {
                    event.setCancelled(true);
                    SignHelper.process(player, (Sign) event.getTargetBlock().getLocation().get().getTileEntity().get(), false);
                }
            }
        }
    }

    /*
    @Listener(order = Order.LAST)
    public void onPlayerMove(final MoveEntityEvent event, @Root Player player) {
        if (IslandWorldManager.getIslandInfoOf(player.getLocation().getBlockX(), player.getLocation().getBlockZ()).getId().equals("(0,0)")) {
            if (player.get(Keys.IS_SNEAKING).orElse(false)) {
                Sign sign = (Sign) player.getTargetBlock().getLocation().get().getTileEntity().get();
                if (event.getTargetTile().getType().equals(TileEntityTypes.SIGN)) {
                    Sign sign = (Sign) event.getTargetTile();
                    if (sign.getSignData().lines().get(0).toPlain().equalsIgnoreCase("[shop]")) {
                        player.sendMessage(MsgHelper.info("ooo it's a shop!"));
                    }
                }
            }
        }
    }
     */


    /*
    @Listener
    public void onTileTarget(TargetTileEntityEvent event, @First Player player) {
        player.sendMessage(MsgHelper.info("would ya look at that tile."));
        if (event.getTargetTile().getType().equals(TileEntityTypes.SIGN)) {
            Sign sign = (Sign) event.getTargetTile();
            if (sign.getSignData().lines().get(0).toPlain().equalsIgnoreCase("[shop]")) {
                player.sendMessage(MsgHelper.info("ooo it's a shop!"));
                if (player.get(Keys.IS_SNEAKING).orElse(false)) {
                    List<Text> lines = Arrays.asList(Text.of("[shop]"), Text.of("sell"));
                    BlockState blockState = BlockState.builder().blockType(sign.getBlock().getType())
                            .add(Keys.SIGN_LINES, lines).build();
                    player.sendBlockChange(event.getTargetTile().getLocation().getBlockX(),
                            event.getTargetTile().getLocation().getBlockY(),
                            event.getTargetTile().getLocation().getBlockZ(),
                            blockState);
                }
            }
        }
    }
     */
}
