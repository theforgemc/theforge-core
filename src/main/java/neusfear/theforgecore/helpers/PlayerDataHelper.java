package neusfear.theforgecore.helpers;

import neusfear.theforgecore.TheForgeCore;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PlayerDataHelper {

    public static Path getPlayerdataDirectory() {
        String filePath = TheForgeCore.getPlugindataDirectory().toString();
        return Paths.get(filePath + File.separator + "playerdata");
    }

    public static File getPlayerdataFile(String uuid) {
        File pdFile = new File(String.valueOf(PlayerDataHelper.getPlayerdataDirectory()) + File.separator + uuid + ".json");
        return pdFile;
    }

}
