package neusfear.theforgecore.helpers;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class ConfigHelper {

    private static CommentedConfigurationNode config = null;
    private static File configFile;
    private static Path configPath;
    private static ConfigurationLoader<CommentedConfigurationNode> configLoader;

    public ConfigHelper(CommentedConfigurationNode config, File configFile, Path configPath, ConfigurationLoader<CommentedConfigurationNode> configLoader){
        ConfigHelper.config = config;
        ConfigHelper.configFile = configFile;
        ConfigHelper.configPath = configPath;
        ConfigHelper.configLoader = configLoader;
    }

    public static CommentedConfigurationNode getConfig(){
        try {

            if (!ConfigHelper.getConfigFile().exists()) {
                return config;
            }

            config = ConfigHelper.getConfigLoader().load();
        } catch (IOException exception) {
            LoggerHelper.getLogger().error("Couldn't create default configuration file!" + exception);
        }
        return config;
    }

    public static File getConfigFile() {
        return configFile;
    }

    public static Path getConfigPath() {
        return configPath;
    }

    public static ConfigurationLoader<CommentedConfigurationNode> getConfigLoader() {
        return configLoader;
    }
}
