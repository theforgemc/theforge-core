package neusfear.theforgecore.helpers.util;

import java.math.BigDecimal;

public class MathHelper {

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    public static int countDecimalDigits(Double d) {
        BigDecimal deci = BigDecimal.valueOf(d);
        int decimalPlaces = 0;
        String text = deci.toPlainString();

        String[] integerDecimalSplit = text.split("\\.");
        if (integerDecimalSplit.length > 1) {
            decimalPlaces = integerDecimalSplit[1].length();
        }
        return decimalPlaces;
    }
}
