package neusfear.theforgecore.helpers;

import neusfear.theforgecore.modules.permissions.Permissions;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.net.MalformedURLException;
import java.net.URL;

public class MsgHelper {

    private static String name = ConfigHelper.getConfig().getNode("messages", "meta", "name").getString();
    private static String normalColor = ConfigHelper.getConfig().getNode("messages", "meta", "normalColor").getString();
    private static String emphasisColor = ConfigHelper.getConfig().getNode("messages", "meta", "emphasisColor").getString();
    private static String mainColor = ConfigHelper.getConfig().getNode("messages", "meta", "mainColor").getString();
    private static String errorColor = ConfigHelper.getConfig().getNode("messages", "meta", "errorColor").getString();

    private static String prefix = emphasisColor + "[" + mainColor + name + emphasisColor + "]" + normalColor;

    public static Text interactablePiece(String message, String hover, String action) {
        return formatted(message).toBuilder()
                .onHover(TextActions.showText(Text.of(TextSerializers.FORMATTING_CODE.deserialize(hover))))
                .onClick(TextActions.runCommand(action))
                .onShiftClick(TextActions.insertText(action)).build();
    }

    public static Text interactablePiece(String message, String hover) {
        return formatted(message).toBuilder()
                .onHover(TextActions.showText(Text.of(TextSerializers.FORMATTING_CODE.deserialize(hover)))).build();
    }

    public static Text interactablePieceLink(String message, String hover, URL link) {
        return formatted(message).toBuilder()
                .onHover(TextActions.showText(Text.of(TextSerializers.FORMATTING_CODE.deserialize(hover))))
                .onClick(TextActions.openUrl(link)).build();
    }

    public static Text interactable(Text... imPieces) {
        return Text.of(imPieces);
    }

    public static Text formatted(String text) {
        return formatted(text, normalColor);
    }

    private static Text formatted(String text, String baseColor) {

        if (!text.contains("<e>")) {
            return Text.of(TextSerializers.FORMATTING_CODE.deserialize(text));
        }
        //Apply emphasis colors markup
        String[] boldTexts = text.split("<e>");
        String empasizedTexts = "&7";
        for (int i = 0; i < boldTexts.length; i++) {
            if (i % 2 == 0) {
                empasizedTexts = empasizedTexts + boldTexts[i] + baseColor;
            } else {
                empasizedTexts = empasizedTexts + boldTexts[i] + emphasisColor;
            }
        }

        //Give it that color boi
        return Text.of(TextSerializers.FORMATTING_CODE.deserialize(empasizedTexts));
    }

    public static Text noPerm() {
        return formatted(prefix + errorColor + " You don't have permission to use that command.", errorColor);
    }

    public static Text error(String message) {
        return formatted(prefix + errorColor + " " + message, errorColor);
    }

    public static Text info(String message) {
        return formatted(prefix + normalColor + " " + message, normalColor);
    }

    public static Text chat(int permissionLevel, String name, String message) {
        return interactable(
                interactablePiece(Permissions.getRankPrefix(permissionLevel), "&fClick&f to view &6rank info", "/rank " + Permissions.getRankFromPermissionLevel(permissionLevel)),
                interactablePiece(Permissions.getRankColor(permissionLevel) + " " + name, "&fClick &7to view &6PlayerData", "/playerdata " + name),
                formatted("&7: &f" + message, mainColor));
    }

    public static Text fromDiscord(String name, String message) throws MalformedURLException {
        return interactable(
                interactablePieceLink("&7[&5D&7] &f" + name, "Message sent from our discord (click to join)", new URL("https://discord.gg/PxQjwh9")),
                formatted("&7: &f" + message, mainColor));
    }

    public static String coloredProgress(int length, int completed, int maxValue, String blankColor, String filledColor) {
        int filled = (int) ((((double)completed / (double)maxValue) * (double)length)+0.5);
        String line = filledColor + StringUtils.repeat("-", length);
        return new StringBuilder(line).insert(filled + 2, blankColor).toString();
    }
}
