package neusfear.theforgecore.helpers;

import org.slf4j.Logger;

public class LoggerHelper {

    private static Logger logger;

    public LoggerHelper(Logger logger) {
        LoggerHelper.logger = logger;
    }

    public static Logger getLogger() {
        return logger;
    }
}
