package neusfear.theforgecore.helpers;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ServerDataHelper {

    public static Path getBackupDirectory() {
        String filePath = ConfigHelper.getConfigPath().toString().substring(0, (ConfigHelper.getConfigPath().toString().length() - 17));
        return Paths.get(filePath + "theforge" + File.separator + "backups");
    }

    public static Path getServerdataDirectory() {
        String filePath = ConfigHelper.getConfigPath().toString().substring(0, (ConfigHelper.getConfigPath().toString().length() - 17));
        return Paths.get(filePath + "theforge" + File.separator + "serverdata");
    }

    public static File getServerdataFile() {
        File pdFile = new File(String.valueOf(ServerDataHelper.getServerdataDirectory()) + File.separator + "data.json");
        return pdFile;
    }

    public static Path getIslanddataDirectory() {
        return getServerdataDirectory();
    }

    public static File getIslanddataFile() {
        File pdFile = new File(String.valueOf(ServerDataHelper.getServerdataDirectory()) + File.separator + "islands.json");
        return pdFile;
    }

    public static Path getTransactionDataDirectory() {
        return getServerdataDirectory();
    }

    public static File getTransactionDataFile() {
        File pdFile = new File(String.valueOf(ServerDataHelper.getServerdataDirectory()) + File.separator + "transactions.json");
        return pdFile;
    }
}
