package neusfear.theforgecore.config;

import neusfear.theforgecore.helpers.ConfigHelper;
import neusfear.theforgecore.helpers.LoggerHelper;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;

import java.io.IOException;

public class TheForgeCoreConfig {

    private CommentedConfigurationNode config;

    public TheForgeCoreConfig() {
        LoggerHelper.getLogger().info("Pre-Initialization process of TheForge Core has begun.");
        try {

            if (!ConfigHelper.getConfigFile().exists()) {

                ConfigHelper.getConfigFile().createNewFile();
                config = ConfigHelper.getConfigLoader().load();

                //Setting this to 0 as the config will populate from defaults below, depending on latest version.
                config.getNode("main", "version").setValue(0);

                //Save the default node
                ConfigHelper.getConfigLoader().save(config);
                LoggerHelper.getLogger().info("Created default configuration file with pre-version checks.");

            }

            config = ConfigHelper.getConfigLoader().load();

        } catch (IOException exception) {
            LoggerHelper.getLogger().error("Couldn't create default configuration file!");
        }

        // Testing to make sure the plugin's config is up to date
        int version = config.getNode("main", "version").getInt();
        LoggerHelper.getLogger().info("Current config file version is " + version + ".");

        switch (version) {

            case 0: {
                LoggerHelper.getLogger().info("Found outdated config file, populating with configuration version 1 nodes.");

                config.getNode("messages", "meta", "name").setValue("Pleodia");
                config.getNode("messages", "meta", "normalColor").setValue("&7");
                config.getNode("messages", "meta", "emphasisColor").setValue("&f");
                config.getNode("messages", "meta", "mainColor").setValue("&6");
                config.getNode("messages", "meta", "errorColor").setValue("&c");

                config.getNode("messages", "welcomeback").setValue("Welcome back {PLAYER} to &6Pleodia &fSurvival!");
                config.getNode("messages", "join").setValue("&7[&2+&7] &f{PLAYER} joined the game.");
                config.getNode("messages", "leave").setValue("&7[&4-&7] &f{PLAYER} left the game.");
                config.getNode("messages", "welcome").setValue("Welcome {PLAYER} to &6Pleodia &fSurvival!");

                config.getNode("islands", "size").setValue(513);
                config.getNode("islands", "distance").setValue(129);

                config.getNode("data", "backupdelay").setValue(5);

                config.getNode("permissions", "rankprefixes", "guest").setValue("&f[&7Guest&f]&7");
                config.getNode("permissions", "rankprefixes", "member").setValue("&f[&7Member&f]&7");
                config.getNode("permissions", "rankprefixes", "noble").setValue("&f[&bNoble&f]&b");
                config.getNode("permissions", "rankprefixes", "hero").setValue("&f[&2Hero&f]&2");
                config.getNode("permissions", "rankprefixes", "paladin").setValue("&f[&5Royal&f]&5");
                config.getNode("permissions", "rankprefixes", "royal").setValue("&f[&cTitan&f]&c");
                config.getNode("permissions", "rankprefixes", "legend").setValue("&f[&9Legend&f]&9");
                config.getNode("permissions", "rankprefixes", "trialmod").setValue("&f[&eStaff&f]&e");
                config.getNode("permissions", "rankprefixes", "moderator").setValue("&f[&6Staff&f]&6");
                config.getNode("permissions", "rankprefixes", "admin").setValue("&f[&6Staff&f]&6");

                config.getNode("rankinfo", "guest").setValue("The default rank for players who just joined.");
                config.getNode("rankinfo", "member").setValue("The Rank once the player has joined our discord.");
                config.getNode("rankinfo", "noble").setValue("[insert perks] : link to buy");
                config.getNode("rankinfo", "hero").setValue("[insert perks] : link to buy");
                config.getNode("rankinfo", "royal").setValue("[insert perks] : link to buy");
                config.getNode("rankinfo", "titan").setValue("[insert perks] : link to buy");
                config.getNode("rankinfo", "legend").setValue("[insert perks] : link to buy");
                config.getNode("rankinfo", "trialmod").setValue("A Moderator in training : link to apply");
                config.getNode("rankinfo", "staff").setValue("Monitors chat and keeps the server chat under control.");
                config.getNode("rankinfo", "admin").setValue("Have console access and manage the server if anything breaks.");

                config.getNode("main", "version").setValue(1);

                LoggerHelper.getLogger().info("Version 1 nodes applied, we will do another check.");
            }

            default: {
                config.getNode("main", "version").setValue(1);
                LoggerHelper.getLogger().info("Configuration file is current.");
            }
        }

        try {
            // Save any changes
            ConfigHelper.getConfigLoader().save(config);
        } catch (IOException e) {
            LoggerHelper.getLogger().error("Failed to save config file!", e);
        }
    }
}
